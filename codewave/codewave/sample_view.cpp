//
//  sample_view.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/18/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "sample_view.h"
#include "n_ranges_for.h"
#include "range_cont.h"
#include <imgui/imgui_internal.h>

using namespace ec;
using namespace std;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void Sample_view::insert_head(std::shared_ptr<Sample_head> const &h) {
    if (auto i = std::find(_heads.begin(), _heads.end(), h);
        i == _heads.end()) {
        _heads.insert(_heads.end(), h);
    }
}

void Sample_view::remove_head(std::shared_ptr<Sample_head> const &h) {
    if (auto i = std::find(_heads.begin(), _heads.end(), h);
        i != _heads.end()) {
        _heads.erase(i);
    }
}

void Sample_view::remove_all_heads() {
    _heads.clear();
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void Sample_view::insert_selection(Range<size_t> r, int tag,
                                   Sample_view::Selection_op sel_op) {
    auto i = _selections.find(tag);
    if (i == end(_selections)) {
        i = _selections.insert(end(_selections), make_pair(tag, Selection()));
    }
    insert_range(i->second, r);
    if (sel_op == select_overwrite_all) {
        for_each(begin(_selections), end(_selections), [&](auto p) {
            if (p.first != tag) {
                remove_range(p.second, r);
            }
        });
    }
}

void Sample_view::remove_selection(Range<size_t> r, int tag) {
    if (auto i = _selections.find(tag); i != end(_selections)) {
        remove_range(i->second, r);
    }
}

void Sample_view::remove_all_selections(int tag) {
    if (auto i = _selections.find(tag); i != end(_selections)) {
        i->second.clear();
    }
}

Sample_view::Selection_iter
Sample_view::selection_begin(int tag) {
    auto i = _selections.find(tag);
    if (i == end(_selections)) {
        i = _selections.insert(end(_selections), make_pair(tag, Selection()));
    }
    return i->second.begin();
}

Sample_view::Selection_iter
Sample_view::selection_end(int tag) {
    auto i = _selections.find(tag);
    if (i == end(_selections)) {
        i = _selections.insert(end(_selections), make_pair(tag, Selection()));
    }
    return i->second.end();
}

Sample_view::Selection_const_iter
Sample_view::selection_begin(int tag) const {
    auto i = _selections.find(tag);
    if (i == end(_selections)) {
        i = _selections.insert(end(_selections), make_pair(tag, Selection()));
    }
    return i->second.begin();
}

Sample_view::Selection_const_iter
Sample_view::selection_end(int tag) const {
    auto i = _selections.find(tag);
    if (i == end(_selections)) {
        i = _selections.insert(end(_selections), make_pair(tag, Selection()));
    }
    return i->second.end();
}

size_t Sample_view::selection_size(int tag) const {
    if(auto i = _selections.find(tag); i != end(_selections)) {
        return i->second.size();
    } else {
        return 0;
    }
}
