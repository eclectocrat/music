//
//  ranges_interface.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 6/19/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "ranges_interface.h"
#include "lua_script_util.h"
#include "lua_sample_util.h"
#include <limits>

#include "RangeSet.h"
#include "RangeSet.hpp"

using namespace ec;
using namespace std;
using namespace mpark;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// name may refer to:
///     interval: {Real, Real}(where t:Table, t[1] <= t[2], extent > 0)
///     intervals: Intervals(where extent > 0)
///     range: {Pos, Pos}(where r:Range, r[1] <= r[2])
///     ranges: Ranges
///
//namespace ec::lua {
//Range_set_t as_ranges(lua_State *ls, size_t extent, const char *name) {
//    const auto type = inquire_range(ls, name);
//    if (type == discrete_range) {
//        // range :: r: {Pos, Pos} where r[1] <= r[2]
//        return { get_discrete_range(ls, name) };
//    } else if (type == continuous_range) {
//        // interval: {Real, Real}(where t:Table, t[1] <= t[2], extent > 0)
//        expects(extent > 0);
//        const auto cr = get_continuous_range(ls, name);
//        return {Range_t{Pos(begin(cr) * extent),
//                        Pos(end(cr) * extent)}};
//    } else {
//        lua_getfield(ls, -1, name);
//        auto r = reinterpret_cast<Ranges_t*>(luaL_checkudata(ls, 1, RANGES_TYPE));
//        lua_pop(ls, 1);
//        if (r != nullptr) {
//            // ranges: Ranges
//            return *r;
//        } else {
//            // intervals: Intervals(where extent > 0)
//            expects(extent > 0);
//            const auto intervals = checked_get<Intervals_t *>(ls, -1);
//            Range_set_t out;
//            transform(begin(*intervals), end(*intervals),
//                      back_inserter(out), [=](auto &cr) {
//                          return Range_t{size_t(begin(cr)) / extent,
//                                         size_t(end(cr)) / extent};
//                      });
//            return out;
//        }
//    }
//}
//
///// name may refer to:
/////     interval: {Real, Real}(where t:Table, t[1] <= t[2])
/////     intervals: Intervals -> Intervals
/////     range: {Pos, Pos}(where r:Range, r[1] <= r[2], extent > 0)
/////     ranges: Ranges(where extent > 0)
/////
//Intervals_t as_intervals(lua_State *ls, size_t extent,
//                                  const char *name) {
//    const auto type = inquire_range(ls, name);
//    if (type == not_a_range) {
//        lua_getfield(ls, -1, name);
//        auto r = reinterpret_cast<Ranges_t*>(luaL_checkudata(ls, 1, RANGES_TYPE));
//        lua_pop(ls, 1);
//        if (r != nullptr) {
//            // ranges: Ranges(where extent > 0) -> Intervals
//            expects(extent > 0);
//            Intervals_t out;
//            transform(begin(*r), end(*r), back_inserter(out),
//                      [=](auto &dr) {
//                          return Interval_t{double(begin(dr)) / extent,
//                                            double(end(dr)) / extent};
//                      });
//            return out;
//        } else {
//            // intervals: Intervals -> Intervals
//            const auto intervals = checked_get<Intervals_t *>(ls, -1);
//            return *intervals;
//        }
//    } else if (type == continuous_range) {
//        // interval: {Real, Real}(where t:Table, t[1] <= t[2]) -> Intervals
//        return { get_continuous_range(ls, name) };
//    } else {
//        // range: {Pos, Pos}(where r:Range, r[1] <= r[2], extent > 0) -> Intervals
//        expects(extent > 0);
//        const auto range = get_discrete_range(ls, name);
//        return { double(begin(range)) / extent, double(end(range)) / extent };
//    }
//}
//} // end namespace
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int Ranges_create(lua_State* ls) {
    // lua::create_object<Ranges_t>(ls, RANGES_TYPE);
    return 1;
}

int Ranges_destroy(lua_State *ls) {
    // lua::destroy_object<Ranges_t>(ls, RANGES_TYPE);
    return 0;
}

int Ranges_clone(lua_State *ls) {
    // lua::clone_object(ls, RANGES_TYPE, lua::checked_get<Ranges_t*>(ls, 1));
    return 1;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
using Range_param = variant<Range<size_t>, Range<double>>;

template <typename F>
void Ranges_perform(Range_set_t& self, Range_param const &range, F f) {
    // we are a discrete ranges container, convert param to discrete_range.
    Sample_range discrete_range;
    if (range.index() == 1) {
        const auto extent = size(self);
        const auto continuous_range = get<1>(range);
        discrete_range = Sample_range(begin(continuous_range) * extent,
                                      end(continuous_range) * extent);
    } else {
        discrete_range = get<0>(range);
    }
    // continue with discrete elements.
    f(self, discrete_range);
}

template <typename F> int Ranges_perform(lua_State *ls, F f) {
    auto self = lua::checked_get<Ranges_t *>(ls, 1);
    auto range = lua::get_range(ls, "range");
    Ranges_perform(*self, range, f);
    return 0;
}

int Ranges_insert(lua_State* ls) {
    return Ranges_perform(ls, [](auto& c, auto r) { insert_range(c, r); });
}

int Ranges_remove(lua_State* ls) {
    return Ranges_perform(ls, [](auto& c, auto r) { remove_range(c, r); });
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int Ranges_to_lua_tables(lua_State* ls) {
    auto self = lua::checked_get<Ranges_t *>(ls, 1);
    lua_createtable(ls, 0, 0);
    int i=1;
    for_each(begin(*self), end(*self), [&](auto const &r) {
        lua::to_range(ls, r);
        lua_rawseti(ls, -2, i++);
    });
    return 1;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// Crop out all ranges outside the clamp.
int Ranges_crop(lua_State* ls) {
    auto self = lua::checked_get<Ranges_t *>(ls, 1);
    auto to_range = lua::get_range(ls, "to");
    Ranges_perform(*self, to_range, [](auto & list, auto const& range) {
        using Range_t = decay_t<decltype(range)>;
        using Index_t = typename Range_t::Index;
        remove_range(list, Range_t(numeric_limits<Index_t>::min(), begin(range)));
        remove_range(list, Range_t(end(range), numeric_limits<Index_t>::max()));
    });
    return 0;
}

/// Multiply each range in a selection by a constant.
///
///     r.scale(by | to)
///     r | scaled{by | to: Real}
///
/// @param by: Double; scale the ranges by the given value.
/// @param to: Double; scale the range so the end lies at the given value. If
///                     both `by` and `to` are given, `to` overwrites `by`.
int Ranges_scale(lua_State* ls) {
    auto self = lua::checked_get<Ranges_t *>(ls, 1);
    lua_getfield(ls, -1, "by");
    double scale=.0;
    if (lua_isnumber(ls, -1)) {
        scale = lua_tonumber(ls, -1);
    }
    lua_pop(ls, 1);
    lua_getfield(ls, -1, "to");
    if (lua_isnumber(ls, -1)) {
        scale = lua_tonumber(ls, -1) / double(ranges_end(*self)); // scale to 0.f <= x <= 1.f
    }
    lua_pop(ls, 1);
    transform(begin(*self), end(*self), begin(*self), [&](auto r) {
        return Range_t{size_t(double(begin(r)) * scale),
                       size_t(double(end(r)) * scale)};
    });
    return 0;
}

/// Repeats a selection contents "times" times, padding the end with "space" or
/// zero.
void Ranges_repeat(Ranges_t* self, int times, size_t space) {
    
    const auto source_begin = begin(*self);
    const auto source_end = end(*self);
    for(int i=0; i<times; i++) {
        const auto offset = ranges_end(*self) + space;
        transform(source_begin, source_end, back_inserter(*self), [&](auto r) {
            return Range_t(begin(r) + offset, end(r) + offset);
        });
    }
}

int Ranges_repeat(lua_State* ls) {
    auto self = lua::checked_get<Ranges_t *>(ls, 1);
    lua_getfield(ls, -1, "times");
    int times=0;
    if (lua_isinteger(ls, -1)) { times = static_cast<int>(lua_tointeger(ls, -1)); }
    lua_pop(ls, 1);
    lua_getfield(ls, -1, "space");
    double space = 0.;
    if (lua_isnumber(ls, -1)) { space = lua_tonumber(ls, -1); }
    lua_pop(ls, 1);
    Ranges_repeat(self, times, space);
    return 0;
}

int Ranges_reverse(lua_State* ls) {
    auto self = lua::checked_get<Ranges_t *>(ls, 1);
    const auto off = ranges_begin(*self);
    const auto len = ranges_size(*self);
    transform(begin(*self), end(*self), begin(*self), [=](auto const &r) {
        return Range_t(len - begin(r) + off, len - end(r) + off);
    });
    self->sort(
        [](auto const &a, auto const &b) { return begin(a) < begin(b); });
    return 0;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static const luaL_Reg Ranges_funcs[] = {{"new", Ranges_create}, {0, 0}};
static const luaL_Reg Ranges_meths[] = {{"__gc", Ranges_destroy},
                                           {"make_copy", Ranges_clone},
                                           {"insert", Ranges_insert},
                                           {"remove", Ranges_remove},
                                           {"get_all", Ranges_to_lua_tables},
                                           {"crop", Ranges_crop},
                                           {"scale", Ranges_scale},
                                           {"repeat", Ranges_repeat},
                                           {"reverse", Ranges_reverse},
                                           {0, 0}};

/*
(a, b) :: Product-type
{a, b} :: Sum-type
{a} :: Optional-type

mutable Range :: (begin:Pos, end:Pos) where r:Range, r.begin <= r.end

Range :: public mutable Range

mutable Ranges ::
    make_copy   :: Ranges -> Ranges
    insert      :: Ranges -> insert:Ranges -> Void
    remove      :: Ranges -> remove:Ranges -> Void
    as_tables   :: Ranges -> [Range]
    crop        :: Ranges -> bounds:Range -> Voids
    scale       :: Ranges -> by:(0 < Real) | to:(0 < Real) -> Void
    repeat      :: Ranges -> times:Int -> {space:Pos} -> Void
    reverse     :: Ranges -> Void
 
Ranges :: private mutable Ranges ::
    removed     :: Ranges -> remove:Ranges -> Ranges
    inserted    :: Ranges -> insert:Ranges -> Ranges
    front       :: Ranges -> until:Pos -> Ranges
    back        :: Ranges -> from:Pos -> Ranges
    reversed    :: Ranges -> Ranges
    repeated    :: Ranges -> times:Int -> Ranges
    +           :: Ranges -> Ranges -> Ranges
    +           :: Ranges -> Int -> Ranges
    -           :: Ranges -> Ranges -> Ranges
    -           :: Ranges -> Int -> Ranges
    *           :: Ranges -> (0 < Real) -> Ranges
    /           :: Ranges -> (0 < Real) -> Ranges
    union       :: Ranges -> Ranges -> Ranges
    intersection:: Ranges -> Ranges -> Ranges
 
    Lua eg:
        r = Ranges{0, 100}
        if immutable then
            r = r << removed{10, 20} | front{r.size/2}
                    | inserted{Ranges{5, 10} * 2} | repeated{2}
        else
            r:remove{10, 20}
            r:remove_back{r.size/2}
            r:insert{Ranges{5, 10} * 2}
            r:repeat{2}
        end
        are_equal = r == Ranges{0, 100} -- true
*/

void ec::register_ranges_model(lua_State *lua) {
    ec::lua::register_class(lua, "Ranges", RANGES_TYPE, Ranges_meths,
                            Ranges_funcs);
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int Intervals_create(lua_State* ls) {
    lua::create_object<Intervals_t>(ls, INTERVALS_TYPE);
    return 1;
}

int Intervals_destroy(lua_State *ls) {
    lua::destroy_object<Intervals_t>(ls, INTERVALS_TYPE);
    return 0;
}

int Intervals_clone(lua_State *ls) {
    lua::clone_object(ls, INTERVALS_TYPE, lua::checked_get<Intervals_t *>(ls, 1));
    return 1;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////s
template <typename F> int Intervals_perform(lua_State *ls, F f) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    auto interval = lua::get_continuous_range(ls, "interval");
    f(*self, interval);
    return 0;
}

int Intervals_insert(lua_State* ls) {
    return Intervals_perform(ls, [](auto& c, auto r) { insert_range(c, r); });
}

int Intervals_remove(lua_State* ls) {
    return Intervals_perform(ls, [](auto& c, auto r) { remove_range(c, r); });
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int Intervals_to_lua_tables(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    lua_createtable(ls, 0, 0);
    int i=1;
    for_each(begin(*self), end(*self), [&](auto const &r) {
        lua::to_range(ls, r);
        lua_rawseti(ls, -2, i++);
    });
    return 1;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// Crop out all ranges outside the clamp.
int Intervals_crop(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    auto range = lua::get_continuous_range(ls, "to");
    remove_range(*self,
                 Interval_t(numeric_limits<double>::min(), begin(range)));
    remove_range(*self,
                 Interval_t(end(range), numeric_limits<double>::max()));
    return 0;
}

/// Multiply each range in a selection by a constant.
///
///     r.scale(by | to)
///     r | scaled{by | to: Real}
///
/// @param by: Double; scale the ranges by the given value.
/// @param to: Double; scale the range so the end lies at the given value. If
///                     both `by` and `to` are given, `to` overwrites `by`.
int Intervals_scale(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    lua_getfield(ls, -1, "by");
    double scale=.0;
    if (lua_isnumber(ls, -1)) {
        scale = lua_tonumber(ls, -1);
    }
    lua_pop(ls, 1);
    lua_getfield(ls, -1, "to");
    if (lua_isnumber(ls, -1)) {
        scale = lua_tonumber(ls, -1) / intervals_end(*self); // scale to 0.f <= x <= 1.f
    }
    lua_pop(ls, 1);
    transform(begin(*self), end(*self), begin(*self), [&](auto i) {
        return Interval_t{begin(i) * scale, end(i) * scale};
    });
    return 0;
}

/// Repeats a selection contents "times" times, padding the end with "space" or
/// zero.
int Intervals_repeat(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    lua_getfield(ls, -1, "times");
    long long times=0;
    if (lua_isinteger(ls, -1)) { times = lua_tointeger(ls, -1); }
    lua_pop(ls, 1);
    lua_getfield(ls, -1, "space");
    double space = 0.;
    if (lua_isnumber(ls, -1)) { space = lua_tonumber(ls, -1); }
    lua_pop(ls, 1);
    const auto source_begin = begin(*self);
    const auto source_end = end(*self);
    for(int i=0; i<times; i++) {
        const auto offset = intervals_end(*self) + space;
        transform(source_begin, source_end, back_inserter(*self),
                  [&](auto i) {
                      return Interval_t(begin(i) + offset, end(i) + offset);
                  });
    }
    return 0;
}

int Intervals_reverse(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    const auto off = intervals_begin(*self);
    const auto len = intervals_size(*self);
    transform(begin(*self), end(*self), begin(*self), [=](auto const &i) {
        return Interval_t(len - begin(i) + off, len - end(i) + off);
    });
    self->sort(
        [](auto const &a, auto const &b) { return begin(a) < begin(b); });
    return 0;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static const luaL_Reg Intervals_funcs[] = {{"new", Intervals_create}, {0, 0}};
static const luaL_Reg Intervals_meths[] = {{"__gc", Intervals_destroy},
                                           {"make_copy", Intervals_clone},
                                           {"insert", Intervals_insert},
                                           {"remove", Intervals_remove},
                                           {"get_all", Intervals_to_lua_tables},
                                           {"crop", Intervals_crop},
                                           {"scale", Intervals_scale},
                                           {"repeat", Intervals_repeat},
                                           {"reverse", Intervals_reverse},
                                           {0, 0}};

void ec::register_intervals_model(lua_State *lua) {
    ec::lua::register_class(lua, "Intervals", INTERVALS_TYPE, Intervals_meths,
                            Intervals_funcs);
}
