//
//  console_window.h
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/3/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef console_window_h
#define console_window_h

#include <array>
#include <vector>
#include <string>
#include <ctype.h>
#include <imgui/imgui.h>

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Console_window {
    std::array<char, 256>       _input_buf;
    std::vector<std::string>    _items;
    bool                        _scroll_to_bottom;
    std::vector<std::string>    _history;
    int                         _history_pos;    // -1: new line, 0.._history.size()-1 browsing history.

    virtual void execute(std::string const&) = 0;
    
    static bool is_whitespace(char ch) {
        return std::isspace(static_cast<unsigned char>(ch)) or ch == '\0';
    }

public:
    Console_window() {
        clear_log();
        _input_buf.fill(0);
        _history_pos = -1;
    }
    virtual ~Console_window() {
        clear_log();
    }
    
    void clear_log() {
        _items.clear();
        _scroll_to_bottom = true;
    }
    
    void append_log(std::string const& str) {
        _items.push_back(str);
        _scroll_to_bottom = true;
    }

    void append_log(const char* fmt, ...) IM_PRINTFARGS(2) {
        std::array<char, 1024> buf;
        va_list args;
        va_start(args, fmt);
        vsnprintf(buf.data(), buf.size(), fmt, args);
        buf[buf.size()-1] = 0;
        va_end(args);
        _items.push_back(std::string(buf.data()));
        _scroll_to_bottom = true;
    }

    void draw(const char* title, bool* p_open) {
        ImGui::SetNextWindowSize(ImVec2(520, 600), ImGuiSetCond_FirstUseEver);
        if (not ImGui::Begin(title, p_open, ImGuiWindowFlags_ShowBorders)) {
            ImGui::End();
            return;
        }
        ImGui::TextWrapped("Enter 'HELP' for help, press TAB to use text completion.");
        ImGui::Separator();

        ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, 0));
        static ImGuiTextFilter filter;
        filter.Draw("Filter (\"incl,-excl\") (\"error\")", 180);
        ImGui::PopStyleVar();
        ImGui::Separator();

        ImGui::BeginChild("ScrollingRegion",
                          ImVec2(0, -ImGui::GetItemsLineHeightWithSpacing()),
                          false, ImGuiWindowFlags_HorizontalScrollbar);
        if (ImGui::BeginPopupContextWindow()) {
            if (ImGui::Selectable("Clear")) {
                clear_log();
            }
            ImGui::EndPopup();
        }

        // Display every line as a separate entry so we can change their color or add custom widgets. If you only want raw text you can use ImGui::TextUnformatted(log.begin(), log.end());
        // NB - if you have thousands of entries this approach may be too inefficient and may require user-side clipping to only process visible items.
        // You can seek and display only the lines that are visible using the ImGuiListClipper helper, if your elements are evenly spaced and you have cheap random access to the elements.
        // To use the clipper we could replace the 'for (int i = 0; i < Items.Size; i++)' loop with:
        //     ImGuiListClipper clipper(Items.Size);
        //     while (clipper.Step())
        //         for (int i = clipper.DisplayStart; i < clipper.DisplayEnd; i++)
        // However take note that you can not use this code as is if a filter is active because it breaks the 'cheap random-access' property. We would need random-access on the post-filtered list.
        // A typical application wanting coarse clipping and filtering may want to pre-compute an array of indices that passed the filtering test, recomputing this array when user changes the filter,
        // and appending newly elements as they are inserted. This is left as a task to the user until we can manage to improve this example code!
        // If your items are of variable size you may want to implement code similar to what ImGuiListClipper does. Or split your data into fixed height items to allow random-seeking into your list.
        
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1)); // Tighten spacing
        for (auto item: _items) {
            if (not filter.PassFilter(item.c_str())) {
                continue;
            }
            ImVec4 col = ImVec4(1.f, 1.f, 1.f, 1.f); // A better implementation may store a type per-item. For the sample let's just parse the text.
            if (item.find("[error]") != std::string::npos) {
                col = ImColor(1.f, 0.4f, 0.4f ,1.f);
            } else if (item.substr(0, 2) == "# ") {
                col = ImColor(1.0f,0.78f,0.58f,1.0f);
            }
            ImGui::PushStyleColor(ImGuiCol_Text, col);
            ImGui::TextUnformatted(item.c_str());
            ImGui::PopStyleColor();
        }
        if (_scroll_to_bottom) {
            ImGui::SetScrollHere();
        }
        _scroll_to_bottom = false;
        ImGui::PopStyleVar();
        ImGui::EndChild();
        ImGui::Separator();

        // Command-line
        if (ImGui::InputText("Input", _input_buf.data(), _input_buf.size(),
                             ImGuiInputTextFlags_EnterReturnsTrue |
                                 ImGuiInputTextFlags_CallbackCompletion |
                                 ImGuiInputTextFlags_CallbackHistory,
                             &TextEditCallbackStub, (void *)this)) {
            // remove trailing whitespace.
            auto trimmed_input = std::string(
                std::begin(_input_buf),
                std::find_if(std::rbegin(_input_buf), std::rend(_input_buf), [](auto c) {
                    return not is_whitespace(c);
                }).base());
            if (not trimmed_input.empty()) {
                exec_command(trimmed_input);
            }
            _input_buf.fill(0);
        }
        // keep auto focus on the input box
        if (ImGui::IsItemHovered() or
            (ImGui::IsRootWindowOrAnyChildFocused() and
             not ImGui::IsAnyItemActive() and not ImGui::IsMouseClicked(0))) {
            ImGui::SetKeyboardFocusHere(-1);
        }
        ImGui::End();
    }

    void exec_command(std::string const& command_line) {
        append_log("# %s\n", command_line.c_str());
        _history_pos = -1;
        _history.push_back(command_line);
        execute(command_line);
    }

    static int TextEditCallbackStub(ImGuiTextEditCallbackData* data) {
        auto console = reinterpret_cast<Console_window*>(data->UserData);
        return console->text_edit_callback(data);
    }

    int text_edit_callback(ImGuiTextEditCallbackData * data) {
        switch (data->EventFlag) {
            case ImGuiInputTextFlags_CallbackCompletion: {
                // Example of TEXT COMPLETION

                // Locate beginning of current word
                const char* word_end = data->Buf + data->CursorPos;
                const char* word_start = word_end;
                while (word_start > data->Buf) {
                    const char c = word_start[-1];
                    if (c == ' ' or c == '\t' or c == ',' or c == ';') {
                        break;
                    }
                    word_start--;
                }
                // Build a list of candidates
                std::vector<std::string> candidates;
                
                // TODO:
                //for (int i = 0; i < _commands.size(); i++) {
                //    if (Strnicmp(_commands[i], word_start, (int)(word_end-word_start)) == 0) {
                //        candidates.push_back(_commands[i]);
                //    }
                //}
                if (candidates.empty()) {
                    append_log("No match for \"%.*s\"!\n", (int)(word_end-word_start), word_start);
                } else if (candidates.size() == 1) {
                    // Single match. Delete the beginning of the word and replace it entirely so we've got nice casing
                    data->DeleteChars((int)(word_start-data->Buf), (int)(word_end-word_start));
                    data->InsertChars(data->CursorPos, candidates[0].c_str());
                    data->InsertChars(data->CursorPos, " ");
                } else {
                    // Multiple matches. Complete as much as we can, so inputing "C" will complete to "CL" and display "CLEAR" and "CLASSIFY"
                    int match_len = (int)(word_end - word_start);
                    for (;;) {
                        int c = 0;
                        bool all_candidates_matches = true;
                        for (int i = 0; i < candidates.size() and all_candidates_matches; i++) {
                            if (i == 0) {
                                c = candidates[i][match_len];
                            } else if (c == 0 or c != candidates[i][match_len]) {
                                all_candidates_matches = false;
                            }
                        }
                        if (not all_candidates_matches) {
                            break;
                        }
                        match_len++;
                    }
                    if (match_len > 0) {
                        data->DeleteChars((int)(word_start - data->Buf), (int)(word_end-word_start));
                        data->InsertChars(data->CursorPos, candidates[0].c_str(), candidates[0].c_str() + match_len);
                    }
                    // List matches
                    append_log("Possible matches:\n");
                    for (auto str: candidates) {
                        append_log("- %s\n", str.c_str());
                    }
                }
                break;
            }
            case ImGuiInputTextFlags_CallbackHistory: {
                // Example of HISTORY
                const int prev_history_pos = _history_pos;
                if (data->EventKey == ImGuiKey_UpArrow) {
                    if (_history_pos == -1) {
                        _history_pos = _history.size() - 1;
                    } else if (_history_pos > 0) {
                        _history_pos--;
                    }
                } else if (data->EventKey == ImGuiKey_DownArrow) {
                    if (_history_pos != -1) {
                        if (++_history_pos >= _history.size()) {
                            _history_pos = -1;
                        }
                    }
                }
                // A better implementation would preserve the data on the current
                // input line along with cursor position.
                if (prev_history_pos != _history_pos) {
                    data->CursorPos = data->SelectionStart =
                        data->SelectionEnd = data->BufTextLen = (int)snprintf(
                            data->Buf, (size_t)data->BufSize, "%s",
                            (_history_pos >= 0) ? _history[_history_pos].c_str()
                                                : "");
                    data->BufDirty = true;
                }
            }
        }
        return 0;
    }
};

} // namespace
#endif /* console_window_h */
