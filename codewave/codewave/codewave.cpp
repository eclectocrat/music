//
//  codewave.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "ranges_interface.h"
#include "lua_script_util.h"
#include "lua_sample_util.h"
#include <limits>

using namespace ec;
using namespace std;

int Intervals_create(lua_State* ls) {
    lua::create_object<Intervals_t>(ls, INTERVALS_TYPE);
    return 1;
}

int Intervals_destroy(lua_State *ls) {
    lua::destroy_object<Intervals_t>(ls, INTERVALS_TYPE);
    return 0;
}

int Intervals_clone(lua_State *ls) {
    lua::clone_object(ls, INTERVALS_TYPE, lua::checked_get<Intervals_t *>(ls, 1));
    return 1;
}

template <typename F> int Intervals_perform(lua_State *ls, F f) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    auto interval = lua::get_continuous_range(ls, "interval");
    f(*self, interval);
    return 0;
}

int Intervals_insert(lua_State* ls) {
    return Intervals_perform(ls, [](auto& c, auto r) { insert_range(c, r); });
}

int Intervals_remove(lua_State* ls) {
    return Intervals_perform(ls, [](auto& c, auto r) { remove_range(c, r); });
}

int Intervals_to_lua_tables(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    lua_createtable(ls, 0, 0);
    int i=1;
    for_each(begin(*self), end(*self), [&](auto const &r) {
        lua::to_range(ls, r);
        lua_rawseti(ls, -2, i++);
    });
    return 1;
}

/// Crop out all ranges outside the clamp.
int Intervals_crop(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    auto range = lua::get_continuous_range(ls, "to");
    remove_range(*self,
                 Interval_t(numeric_limits<double>::min(), begin(range)));
    remove_range(*self,
                 Interval_t(end(range), numeric_limits<double>::max()));
    return 0;
}

/// Multiply each range in a selection by a constant.
///
///     r.scale(by | to)
///     r | scaled{by | to: Real}
///
/// @param by: Double; scale the ranges by the given value.
/// @param to: Double; scale the range so the end lies at the given value. If
///                     both `by` and `to` are given, `to` overwrites `by`.
int Intervals_scale(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    lua_getfield(ls, -1, "by");
    double scale=.0;
    if (lua_isnumber(ls, -1)) {
        scale = lua_tonumber(ls, -1);
    }
    lua_pop(ls, 1);
    lua_getfield(ls, -1, "to");
    if (lua_isnumber(ls, -1)) {
        scale = lua_tonumber(ls, -1) / intervals_end(*self); // scale to 0.f <= x <= 1.f
    }
    lua_pop(ls, 1);
    transform(begin(*self), end(*self), begin(*self), [&](auto i) {
        return Interval_t{begin(i) * scale, end(i) * scale};
    });
    return 0;
}

/// Repeats a selection contents "times" times, padding the end with "space" or
/// zero.
int Intervals_repeat(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    lua_getfield(ls, -1, "times");
    long long times=0;
    if (lua_isinteger(ls, -1)) { times = lua_tointeger(ls, -1); }
    lua_pop(ls, 1);
    lua_getfield(ls, -1, "space");
    double space = 0.;
    if (lua_isnumber(ls, -1)) { space = lua_tonumber(ls, -1); }
    lua_pop(ls, 1);
    const auto source_begin = begin(*self);
    const auto source_end = end(*self);
    for(int i=0; i<times; i++) {
        const auto offset = intervals_end(*self) + space;
        transform(source_begin, source_end, back_inserter(*self),
                  [&](auto i) {
                      return Interval_t(begin(i) + offset, end(i) + offset);
                  });
    }
    return 0;
}

int Intervals_reverse(lua_State* ls) {
    auto self = lua::checked_get<Intervals_t *>(ls, 1);
    const auto off = intervals_begin(*self);
    const auto len = intervals_size(*self);
    transform(begin(*self), end(*self), begin(*self), [=](auto const &i) {
        return Interval_t(len - begin(i) + off, len - end(i) + off);
    });
    self->sort(
        [](auto const &a, auto const &b) { return begin(a) < begin(b); });
    return 0;
}
s
static const luaL_Reg Intervals_funcs[] = {{"new", Intervals_create}, {0, 0}};
static const luaL_Reg Intervals_meths[] = {{"__gc", Intervals_destroy},
                                           {"make_copy", Intervals_clone},
                                           {"insert", Intervals_insert},
                                           {"remove", Intervals_remove},
                                           {"get_all", Intervals_to_lua_tables},
                                           {"crop", Intervals_crop},
                                           {"scale", Intervals_scale},
                                           {"repeat", Intervals_repeat},
                                           {"reverse", Intervals_reverse},
                                           {0, 0}};

void ec::register_intervals_model(lua_State *lua) {
    ec::lua::register_class(lua, "Intervals", INTERVALS_TYPE, Intervals_meths,
                            Intervals_funcs);
}

