//
//  sample_view.hpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/18/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef sample_view_h
#define sample_view_h

#include "sample.h"
#include "sample_head.h"
#include "rect.h"
#include "range_cont.h"
#include <imgui/imgui.h>
#include <map>
#include <list>
#include <vector>

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Sample_view {
public:
    using Selection = std::list<Range<size_t>>;
    using Selection_iter = Selection::iterator;
    using Selection_const_iter = Selection::iterator;

    Sample_view() = default;
    Sample_view(Sample_view const&) = default;
    Sample_view& operator=(Sample_view const&) = default;
    Sample_view(Sample_view &&) = default;
    Sample_view& operator=(Sample_view &&) = default;
    virtual ~Sample_view() = default;
    
/// Views can display one Sample.
    
    virtual void set_sample(std::shared_ptr<Sample> const &s) { _sample = s; }
    virtual std::shared_ptr<Sample> const &sample() { return _sample; }
    virtual std::shared_ptr<const Sample> sample() const { return _sample; }
    
/// Views can display any number of Sample_heads.
    
    virtual void insert_head(std::shared_ptr<Sample_head> const &);
    virtual void remove_head(std::shared_ptr<Sample_head> const &);
    virtual void remove_all_heads();

/// View's can have highlights that span over the time domain. We can edit and
/// read the topology of those highlights whenever we please. View's can display
/// highlights in any implementation defined manner.

    enum Selection_op { select_normal, select_overwrite_all };
    enum Selection_type { selection=0, disabled /* ... Use your own tags!... */ };
    virtual void insert_selection(Range<size_t>, int=0, Selection_op=select_normal);
    virtual void remove_selection(Range<size_t>, int=0);
    virtual void remove_all_selections(int=0);
    virtual Selection_iter          selection_begin     (int=0);
    virtual Selection_iter          selection_end       (int=0);
    virtual Selection_const_iter    selection_begin     (int=0) const;
    virtual Selection_const_iter    selection_end       (int=0) const;
    virtual size_t                  selection_size      (int=0) const;

protected:
    using Head_vec = std::vector<std::shared_ptr<Sample_head>>;
    using Selection_map = std::map<int, Selection>;
    Head_vec& heads() { return _heads; }
    Selection_map& selection_map() { return _selections; }
    
private:
    std::shared_ptr<Sample> _sample;
    Head_vec _heads;
    mutable Selection_map _selections;
};

} // END namespace
#endif /* sample_view_h */
