//
//  lua_sample_util.h
//  codewave
//
//  Created by Jeremy Jurksztowicz on 6/19/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef lua_sample_util_h
#define lua_sample_util_h

#include "std_variant.hpp"
#include "sample_interface.h"

namespace ec::lua {

template<typename ...Types>
using variant = mpark::variant<Types...>;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline size_t get_pos(lua_State *ls, const char *name, Sample *sample) {
    lua_getfield(ls, -1, name);
    size_t pos = 0;
    if (lua_isinteger(ls, -1)) {
        if (lua_isinteger(ls, -1)) {
            pos = lua_tointeger(ls, -1);
        } else if (lua_isnumber(ls, -1)) {
            pos = lua_tonumber(ls, -1) * double(sample->frame_count());
        }
    }
    lua_pop(ls, 1);
    return pos;
}

inline Sample_range get_discrete_range(lua_State *ls, const char *name,
                                       Sample const*sample = nullptr) {
    lua_getfield(ls, -1, name);
    Sample_range range;
    if (lua_istable(ls, -1)) {
        auto get_range_pos = [&](int n) {
            size_t pos = 0;
            lua_rawgeti(ls, -1, n);
            if (lua_isinteger(ls, -1)) {
                pos = lua_tointeger(ls, -1);
            } else if (lua_isnumber(ls, -1)) {
                pos = lua_tonumber(ls, -1) *
                      (sample ? double(sample->frame_count()) : 1.f);
            }
            lua_pop(ls, 1);
            return pos;
        };
        range = Sample_range(get_range_pos(1), get_range_pos(2));
    }
    lua_pop(ls, 1);
    return range;
}

inline Range<double> get_continuous_range(lua_State *ls, const char *name) {
    lua_getfield(ls, -1, name);
    Range<double> range;
    if (lua_istable(ls, -1)) {
        auto get_range_pos = [&](int n) {
            double pos = 0.f;
            lua_rawgeti(ls, -1, n);
            if (lua_isnumber(ls, -1)) {
                pos = lua_tonumber(ls, -1);
            }
            lua_pop(ls, 1);
            return pos;
        };
        range = Range<double>(get_range_pos(1), get_range_pos(2));
    }
    lua_pop(ls, 1);
    return range;
}

enum Range_inquiry { not_a_range, discrete_range, continuous_range };
inline Range_inquiry inquire_range(lua_State *ls, const char *name) {
    Range_inquiry result = not_a_range; // <empty stack>
    lua_getfield(ls, -1, name);         // {}
    if (lua_istable(ls, -1)) {
        lua_rawgeti(ls, -1, 1);          // {}, ?
        if (lua_isinteger(ls, -1)) {     // {}, int
            lua_pop(ls, 1);              // {}
            lua_rawgeti(ls, -1, 2);      // {}, ?
            if (lua_isinteger(ls, -1)) { // {}, int
                lua_pop(ls, 1);          // {}
                result = discrete_range;
            }
        } else if (lua_isnumber(ls, -1)) { // {}, double
            const auto first = lua_tonumber(ls, -1);
            lua_pop(ls, 1); // {}
            if (first >= 0. and first <= 1. + FLT_EPSILON) {
                lua_rawgeti(ls, -1, 2);     // {}, ?
                if (lua_isnumber(ls, -1)) { // {}, double
                    const auto second = lua_tonumber(ls, -1);
                    if (second >= 0. and second <= 1. + FLT_EPSILON) {
                        result = continuous_range;
                    }
                }
                lua_pop(ls, 1); // {}
            }
        } else {
            lua_pop(ls, 1); // {}
        }
    }
    lua_pop(ls, 1); // <empty stack>
    return result;
}

inline variant<Sample_range, Range<double>>
get_range(lua_State *ls, const char *name, Sample const*sample = nullptr) {
    const auto state = inquire_range(ls, name);
    if (state == not_a_range) {
        throw luaL_error(ls, "Not a range!");
    } else if (sample != nullptr or discrete_range) {
        return get_discrete_range(ls, name, sample);
    } else {
        expects(state == continuous_range);
        return get_continuous_range(ls, name);
    }
}

template <typename T> void to_range(lua_State *, Range<T>);
template <> inline void to_range<size_t>(lua_State *ls, Range<size_t> r) {
    lua_createtable(ls, 2, 0);
    lua_pushinteger(ls, begin(r));
    lua_rawseti(ls, -2, 1);
    lua_pushinteger(ls, end(r));
    lua_rawseti(ls, -2, 2);
}
template <> inline void to_range<double>(lua_State *ls, Range<double> r) {
    lua_createtable(ls, 2, 0);
    lua_pushnumber(ls, begin(r));
    lua_rawseti(ls, -2, 1);
    lua_pushnumber(ls, end(r));
    lua_rawseti(ls, -2, 2);
}

inline void dump_range(lua_State *ls, const char *name) {
    check_is_table(ls, -1);
    if (name != nullptr) {
        lua_getfield(ls, -1, name);
        check_is_table(ls, -1);
    }
    lua_rawgeti(ls, -1, 1);
    lua_rawgeti(ls, -2, 2);
    std::cout << "Range{" << lua_tonumber(ls, -2) << ", "
              << lua_tonumber(ls, -1) << "}";
    lua_pop(ls, 2 + (name != nullptr ? 1 : 0));
}

inline void update_ref(lua_State *ls, int &ref) {
    if (ref != LUA_REFNIL) {
        luaL_unref(ls, LUA_REGISTRYINDEX, ref);
    }
    ref = luaL_ref(ls, LUA_REGISTRYINDEX);
}

} // namespace lua
#endif /* lua_sample_util_h */
