//
// Copyright (c) 2017 Jeremy Jurksztowicz
//
//     Permission is hereby granted, free of charge, to any person obtaining a
//     copy of this software and associated documentation files (the
//     "Software"), to deal in the Software without restriction, including
//     without limitation the rights to use, copy, modify, merge, publish,
//     distribute, sublicense, and/or sell copies of the Software, and to permit
//     persons to whom the Software is furnished to do so, subject to the
//     following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <Accelerate/Accelerate.h>
#include <GL/gl3w.h>
#include <SDL2/SDL.h>
#include <RtAudio.h>
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>
#include <imgui/imgui_impl_sdl_gl3.h>
#include <vector>
#include <list>
#include <memory>
#include <readerwriterqueue/readerwriterqueue.h>
#include <experimental/optional>

#define BOOST_NO_AUTO_PTR
#include <boost/filesystem.hpp>

// embedded
#include "rtmidi/RtMidi.h"
#include "cxxopts.hpp"

#include "constexpr_algo.h"
#include "imgui-tools/program.h"
#include "lua/lua.hpp"
#include "lua_script_util.h"
#include "windows/console_window.h"
#include "strong_type.h"
#include "pointer.h"
#include "sample.h"
#include "sample_view.h"
#include "sample_interface.h"
#include "ranges_interface.h"
#include "wave_audio.h"

// #include "graph_editor.h"
// #include <iostream>
// #include <iomanip>

using namespace std;
using namespace ec;
using namespace ec::audio;
using std::experimental::optional;

#define CATCH_AND_PRINT \
    catch (RtMidiError &error) { \
        error.printMessage(); \
    } catch (error_base const& err) { \
        cerr << err.what() << endl; \
    } catch (std::exception const& err) { \
        cerr << err.what() << endl; \
    } catch (...) { \
        cerr << "Unknown exception, aborting..." << endl; \
    }
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Scripting repl
//
struct Lua_repl: Console_window {
    lua_State * lua;
    
    Lua_repl(): Console_window() {
        lua = luaL_newstate();
        luaL_openlibs(lua);
    }
    
    // implemented _after_ the lua object model is defined below
    void register_object_model();
    
private:
    void execute(string const& str) override {
        if (auto err = luaL_dostring(lua, str.c_str()); err != 0) {
            append_log(print_lua_error(lua));
        }
    }
    
    string print_lua_error(lua_State * ls) {
        if (lua_isstring(ls, -1)) {
            const string err_str(lua_tostring(ls, -1));
            cerr << err_str << endl;
            lua_pop(ls, 1);
            return err_str;
        } else {
            cerr << "UNKNOWN ERROR!" << endl;
            return "UNKNOWN ERROR!";
        }
    }
};

shared_ptr<Lua_repl> repl;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Midi
//
struct Midi_msg {
    using Byte = unsigned char;
    Byte channel = 0;
    Byte type    = 0;
    Byte byte1   = 0;
    Byte byte2   = 0;
};

using Midi_msgs_queue = moodycamel::ReaderWriterQueue<Midi_msg>;
shared_ptr<RtMidiIn> midi_in;
shared_ptr<RtMidiOut> midi_out;
int midi_handler_ref = LUA_REFNIL;
Midi_msgs_queue midi_in_queue;

void midi_in_f(double time_stamp, std::vector<unsigned char> *message, void *) {
    try {
        expects(message);
        
        // DEBUG
        // cout << hex << (int)(message->at(0)) << " | " << flush;
        
        if (message->at(0) == 0xF8) {
            if (not midi_in_queue.try_enqueue({0, 0xF8, 0, 0})) {
                throw runtime_error("MIDI in queue overflow!");
            }
        } else {
            const Midi_msg::Byte chan = message->at(0) & 0x0F;
            const Midi_msg::Byte type = (message->at(0) & 0xF0) >> 4;
            const Midi_msg::Byte byte = message->size() > 1 ? message->at(1) : 0;
            const Midi_msg::Byte byte2 = message->size() > 2 ? message->at(2) : 0;
            
            if (not midi_in_queue.try_enqueue({chan, type, byte, byte2})) {
                throw runtime_error("MIDI in queue overflow!");
            }
        }
    } CATCH_AND_PRINT;
}

int MIDI_set_input(lua_State* ls) {
    if (midi_handler_ref != LUA_REFNIL) {
        luaL_unref(ls, LUA_REGISTRYINDEX, midi_handler_ref);
        midi_handler_ref = LUA_REFNIL;
    }
    if (lua_type(ls, -1) == LUA_TFUNCTION) {
        midi_handler_ref = luaL_ref(ls, LUA_REGISTRYINDEX);
    }
    return 0;
}

int MIDI_send(lua_State* ls) {
    luaL_checktype(ls, -1, LUA_TTABLE);
    lua_len(ls, -1);
    vector<unsigned char> buffer(lua_tointeger(ls, -1));
    lua_pop(ls, 1);
    int index=0;
    do {
        lua_geti(ls, -1, index+1);
        buffer[index] = lua_tointeger(ls, -1);
        lua_pop(ls, 1);
    } while(++index < buffer.size());
    midi_out->sendMessage(&buffer);
    return 0;
}

static const luaL_Reg MIDI_funcs[] = {
    {"set_receive", MIDI_set_input},
    {"send", MIDI_send},
    {0, 0}
};
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Audio
//
Wave_audio_driver audio_io;

using Audio = Wave_audio_driver;
int audio_head_was_added_ref = LUA_REFNIL;
int audio_head_was_removed_ref = LUA_REFNIL;
int audio_slice_will_process_ref = LUA_REFNIL;
int audio_slice_did_process_ref = LUA_REFNIL;

int Audio_play(lua_State *ls) {
    auto head = lua::param<Wave_head *>(ls, "head", 1);
    const int r = luaL_ref(ls, LUA_REGISTRYINDEX);
    audio_io.send_message(
        {&audio_io,
         shared_ptr<Sample_head>(
             head, [=](auto) { luaL_unref(ls, LUA_REGISTRYINDEX, r); }),
         Audio::Message::add});
    return 0;
}

int Audio_pause(lua_State *ls) {
    auto head = lua::param<Wave_head *>(ls, "head", 1);
    const int r = luaL_ref(ls, LUA_REGISTRYINDEX);
    audio_io.send_message(
        {&audio_io,
         shared_ptr<Sample_head>(
             head, [=](auto) { luaL_unref(ls, LUA_REGISTRYINDEX, r); }),
         Audio::Message::remove});
    return 0;
}

inline void update_ref(lua_State *ls, int &ref) {
    luaL_checktype(ls, -1, LUA_TFUNCTION);
    if (ref != LUA_REFNIL) {
        luaL_unref(ls, LUA_REGISTRYINDEX, ref);
    }
    ref = luaL_ref(ls, LUA_REGISTRYINDEX);
}

int Audio_set_will_process(lua_State *ls) {
    update_ref(ls, audio_slice_will_process_ref);
    return 0;
}

int Audio_set_did_process(lua_State *ls) {
    update_ref(ls, audio_slice_did_process_ref);
    return 0;
}

int Audio_set_head_was_added(lua_State *ls) {
    update_ref(ls, audio_head_was_added_ref);
    return 0;
}

int Audio_set_head_was_removed(lua_State *ls) {
    update_ref(ls, audio_head_was_removed_ref);
    return 0;
}

static const luaL_Reg Audio_funcs[] = {
    {"play", Audio_play},
    {"pause", Audio_pause},
    {"set_pre_clock_tick", Audio_set_will_process},
    {"set_post_clock_tick", Audio_set_did_process},
    {"set_head_was_removed", Audio_set_head_was_removed},
    {"set_head_was_added", Audio_set_head_was_added},
    {0, 0}};
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static const char *print_fun_source =
    "function print(str, ...) _print_impl(string.format(str, ...)) end";

int _print_impl(lua_State *ls) {
    if (lua_isstring(ls, -1)) {
        const auto str = string(lua_tostring(ls, -1));
        repl->append_log(str);
        cout << str << endl;
    }
    return 0;
}

// Filesystem
int Files_current_path(lua_State *ls) {
    lua_pushstring(ls, boost::filesystem::current_path().native().c_str());
    return 1;
}

int Files_set_current_path(lua_State *ls) {
    using namespace boost;
    auto path = lua::param<string>(ls, "path");
    system::error_code err;
    filesystem::current_path(path, err);
    if (err) {
        luaL_error(ls, "error setting current path: %s", err.message().c_str());
    }
    return 0;
}

int Files_get_file_selection(lua_State *ls) {
    
    return 0;
}

static const luaL_Reg Filesystem_funcs[] = {
    {"current_path", Files_current_path},
    {"set_current_path", Files_set_current_path},
    {0, 0}};

// Window
int window_size_did_change = LUA_REFNIL;

void update_window_size(Rect<float> const& r) {
    if (window_size_did_change != LUA_REFNIL) {
        expects(repl->lua != nullptr);
        lua_rawgeti(repl->lua, LUA_REGISTRYINDEX, window_size_did_change);
        lua_createtable(repl->lua, 0, 4);
        lua_pushnumber(repl->lua, r.origin.x);
        lua_setfield(repl->lua, -2, "x");
        lua_pushnumber(repl->lua, r.origin.y);
        lua_setfield(repl->lua, -2, "y");
        lua_pushnumber(repl->lua, r.size.w);
        lua_setfield(repl->lua, -2, "w");
        lua_pushnumber(repl->lua, r.size.h);
        lua_setfield(repl->lua, -2, "h");
        if (lua_pcall(repl->lua, 1, 0, 0)) {
            cerr << "Error: " << lua_tostring(repl->lua, -1) << endl;
            lua_pop(repl->lua, 1);
        }
    }
}

int Window_size_did_change(lua_State *ls) {
    update_ref(ls, window_size_did_change);
    return 0;
}

static const luaL_Reg Window_funcs[] = {
    {"set_size_did_change", Window_size_did_change},
    {0, 0}};

int Time_now(lua_State *ls) {
    lua_pushinteger(ls, SDL_GetTicks());
    return 1;
}

static const luaL_Reg Time_funcs[] = {
    {"now", Time_now},
    {0, 0}};

/*
static const char *idle_source = "\
    idle_counter = 0 \
    function idle() \
        idle_counter = idle_counter + 1 \
    end";
*/

void Lua_repl::register_object_model() {
    register_wave_model(lua);
    register_wave_view_model(lua);
    register_wave_head_model(lua);
    // register_ranges_model(lua);

    luaL_newlib(lua, Filesystem_funcs);
    lua_setglobal(lua, "Files");

    luaL_newlib(lua, Audio_funcs);
    lua_setglobal(lua, "Audio");

    luaL_newlib(lua, MIDI_funcs);
    lua_setglobal(lua, "MIDI");
    
    luaL_newlib(lua, Window_funcs);
    lua_setglobal(lua, "Window");
    
    luaL_newlib(lua, Time_funcs);
    lua_setglobal(lua, "Time");

    lua_pushcfunction(lua, _print_impl);
    lua_setglobal(lua, "_print_impl");
    expects(luaL_dostring(lua, print_fun_source) == 0);
    
    // TODO: fix
    // expects(luaL_dostring(lua, idle_source) == 0);
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
bool lua_audio_head_callback(Wave_head &head, int callback) {
    expects(callback != LUA_REFNIL);
    expects(repl->lua != nullptr);
    lua_rawgeti(repl->lua, LUA_REGISTRYINDEX, callback);
    head.push_weak_self(repl->lua);
    if (not lua_isnil(repl->lua, -1)) {
        if (lua_pcall(repl->lua, 1, 0, 0)) {
            cerr << "Error: " << lua_tostring(repl->lua, -1) << endl;
            lua_pop(repl->lua, 1);
            return false;
        }
        return true;
    } else {
        lua_pop(repl->lua, 1); // get rid of nil
        return false;
    }
}

bool lua_audio_clock_callback(int callback, double stream_time) {
    expects(callback != LUA_REFNIL);
    expects(repl->lua != nullptr);
    lua_rawgeti(repl->lua, LUA_REGISTRYINDEX, callback);
    lua_pushnumber(repl->lua, stream_time);
    if (lua_pcall(repl->lua, 1, 0, 0)) {
        cerr << "Error: " << lua_tostring(repl->lua, -1) << endl;
        lua_pop(repl->lua, 1);
        return false;
    }
    return true;
}

void process_audio_queue() {
    Audio::Message msg;
    while (audio_io.try_recieve_message(msg)) {
        switch(msg.meta) {
        case Wave_audio_driver::Message::will_begin_frame_slice: {
            if (audio_slice_will_process_ref != LUA_REFNIL) {
                lua_audio_clock_callback(audio_slice_will_process_ref,
                                         msg.time);
            }
        } break;
        case Wave_audio_driver::Message::did_end_frame_slice: {
            if (audio_slice_did_process_ref != LUA_REFNIL) {
                lua_audio_clock_callback(audio_slice_did_process_ref,
                                         msg.time);
            }
        } break;
        case Wave_audio_driver::Message::played: {
            // A head made progress:
        } break;
        case Wave_audio_driver::Message::remove: {
            if (msg.head != nullptr and
                audio_head_was_removed_ref != LUA_REFNIL) {
                auto h = reinterpret_cast<Wave_head*>(msg.head.get());
                lua_audio_head_callback(*h, audio_head_was_removed_ref);
            }
        } break;
        case Wave_audio_driver::Message::add: {
            if (msg.head != nullptr and
                audio_head_was_added_ref != LUA_REFNIL) {
                auto h = reinterpret_cast<Wave_head*>(msg.head.get());
                lua_audio_head_callback(*h, audio_head_was_added_ref);
            }
        } break;
        default: break;
        }
    }
}

void process_midi_queue() {
    Midi_msg midi_msg;
    while (midi_in_queue.try_dequeue(midi_msg)) {
        if(LUA_REFNIL != midi_handler_ref) {
            lua_gc(repl->lua, LUA_GCSTOP, 0);
            lua_rawgeti(repl->lua, LUA_REGISTRYINDEX, midi_handler_ref);
            lua_pushinteger(repl->lua, midi_msg.channel);
            lua_pushinteger(repl->lua, midi_msg.type);
            lua_pushinteger(repl->lua, midi_msg.byte1);
            lua_pushinteger(repl->lua, midi_msg.byte2);
            if(lua_pcall(repl->lua, 4, 0, 0)) {
                cerr << "Error: " << lua_tostring(repl->lua, -1) << endl;
                lua_pop(repl->lua, 1);
            }
            lua_gc(repl->lua, LUA_GCRESTART, 0);
        }
    }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// Hook this into whatever windowing system you are using to handle
/// mouse/touch/cursor/whatever movement, begin event (touch, click, etc.),
/// and end event.
void wave_view_cursor_handler(Wave_view *view, int ref, bool left, bool right,
                              Point<size_t> p) {
    if (ref == LUA_REFNIL) {
        return;
    }
    lua_rawgeti(repl->lua, LUA_REGISTRYINDEX, ref);
    luaL_checktype(repl->lua, -1, LUA_TFUNCTION);
    view->push_weak_self(repl->lua);
    expects(lua_istable(repl->lua, -1));
    lua_pushboolean(repl->lua, left);
    lua_pushboolean(repl->lua, right);
    lua_pushinteger(repl->lua, p.x);
    lua_pushinteger(repl->lua, p.y);
    if (lua_pcall(repl->lua, 5, 0, 0)) {
        cerr << "Cursor error: " << lua_tostring(repl->lua, -1) << endl;
        lua_pop(repl->lua, 1);
    }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
optional<boost::filesystem::path>
path_for_program(boost::filesystem::path program_path) {
    using namespace boost::filesystem;
    if (exists(program_path)) {
        if (is_directory(program_path)) {
            // look inside for executable file
        }
        return program_path;
    } else if (program_path.extension() != "lua" and
               exists(program_path.replace_extension("lua"))) {
        return program_path;
    } else {
        return {};
    }
}

optional<boost::filesystem::path>
find_path_for_program_name(string program_name) {
    using namespace boost::filesystem;
    const path script_path("./script/");
    if (auto path = path_for_program(program_name); path) {
        return path;
    } else if (auto path = path_for_program(script_path / program_name); path) {
        return path;
    }
    return {};
}

void run_program(string program) {
    const auto program_path = find_path_for_program_name(program);
    if (not program_path) {
        cerr << "Unable to find `" << program << "`" << endl;
        return;
    }
    if (luaL_dofile(repl->lua, program_path->string().c_str()) != 0) {
        cerr << lua_tostring(repl->lua, -1) << endl;
        lua_pop(repl->lua, 1);
    }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
extern list<Wave*> all_waves;
extern list<Wave_view*> all_wave_views;

int main(int argc, char ** argv) {
    try {
        cxxopts::Options options(
            "codewave", "A fully programmable Audio and MIDI environment.");
        options.add_options("execute")
            ("r,run", "Run the given codewave program", cxxopts::value<string>())
            ("t,test", "Run built in test suite. May help diagnose errors.");
        
        const auto args = options.parse(argc, argv);

        setlocale(LC_ALL, "");
    
    //////////////////////////
    /// Setup memory model ///
    //////////////////////////
        Sample_buffer::shared_allocator_ref = make_shared<Malloc<float>>();
    
    ////////////////////////////
    /// Setup scripting repl ///
    ////////////////////////////
        repl = make_shared<Lua_repl>();
        repl->register_object_model();
    
    //////////////////
    /// Setup MIDI ///
    //////////////////
        midi_in = make_shared<RtMidiIn>(RtMidi::UNSPECIFIED, "codewave");
        midi_in->setCallback(midi_in_f);
        midi_in->openVirtualPort("codewave");
        midi_in->ignoreTypes(false, false, false);
        midi_out = make_shared<RtMidiOut>();
        midi_out->openVirtualPort();
    
    ////////////////////////////////
    /// Load core lua base files ///
    ////////////////////////////////
        const auto path = boost::filesystem::path("./script/core/");
        boost::filesystem::directory_iterator end_itr;
        vector<pair<boost::filesystem::path, string>> raw_paths;
        for (boost::filesystem::directory_iterator itr(path); itr != end_itr;
             ++itr) {
            if (is_regular_file(itr->path())) {
                raw_paths.push_back(
                    make_pair(itr->path(), itr->path().filename().string()));
            }
        }
        sort(begin(raw_paths), end(raw_paths),
             [](auto const &a, auto const &b) {
                 return tolower(a.second.front()) > tolower(b.second.front());
             });
        if (auto prop_iter =
                find_if(begin(raw_paths), end(raw_paths),
                     [](const auto &p) { return p.second == "property.lua"; });
            prop_iter != end(raw_paths)) {
            const auto copy = *prop_iter;
            raw_paths.erase(prop_iter);
            raw_paths.insert(end(raw_paths), copy);
        }
        for_each(rbegin(raw_paths), rend(raw_paths), [&](auto const& p) {
            cout << "Loading " << p.second << "..." << flush;
            if (luaL_dofile(repl->lua, p.first.string().c_str()) != 0) {
                cout << endl;
                cerr << lua_tostring(repl->lua, -1) << endl;
                lua_pop(repl->lua, 1);
            }
            cout << " OK!" << endl;
        });
    
    ///////////////////
    /// Setup audio ///
    ///////////////////
        audio_io.init();
        
    ///////////////////////////////////////////////
    /// Optionally load an app file immediately ///
    ///////////////////////////////////////////////
        if(args.count("run") > 0) {
            const auto program = args["run"].as<string>();
            cout << "Running program \"" << program << "\"..." << endl;
            run_program(program);
        }

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// ImGui program:
///     Below here we have implementation specific to a windowing/presentation
///     system. Here we handle the actual runloop of the app. If this were a
///     console app we'd use curses or printf in a loop below. If this were a
///     UIKit app, the following would be handled by the iOS main queue.
///
///     Try and keep the code above ^ this break as platform and presentation
///     agnostic as possible.
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
        Wave_view * last_cursor_window = nullptr;
        bool show_console = true;
        Imgui_program prog;
        prog.key_down = [](auto key_code) {};
        prog.key_up = [](auto key_code) {};
        
        prog.window_state = [](Size<float> const& size) {
            update_window_size(size);
        };
        prog.mouse_moved = [&](bool left, bool right, auto p) {
            if (last_cursor_window != nullptr and
                last_cursor_window->keep_cursor_while_down) {
                wave_view_cursor_handler(last_cursor_window,
                                         last_cursor_window->cursor_moved_ref,
                                         left, right, p);
            } else if (auto i = find_if(
                           begin(all_wave_views), end(all_wave_views),
                           [=](Wave_view *view) {
                               return view->last_drawn_frame.contains_point(p);
                           });
                       i != end(all_wave_views)) {
                wave_view_cursor_handler(*i, (*i)->cursor_moved_ref, left,
                                         right, p);
            }
        };
        prog.mouse_down = [&](bool left, bool right, auto p) {
            if (auto i =
                    find_if(begin(all_wave_views), end(all_wave_views),
                            [=](Wave_view *view) {
                                return view->last_drawn_frame.contains_point(p);
                            });
                i != end(all_wave_views)) {
                wave_view_cursor_handler(*i, (*i)->cursor_down_ref, left, right,
                                         p);
            }
        };
        prog.mouse_up = [&](bool left, bool right, auto p) {
            if (last_cursor_window != nullptr and
                last_cursor_window->keep_cursor_while_down) {
                wave_view_cursor_handler(last_cursor_window,
                                         last_cursor_window->cursor_up_ref,
                                         left, right, p);
            } else if (auto i = find_if(
                           begin(all_wave_views), end(all_wave_views),
                           [=](Wave_view *view) {
                               return view->last_drawn_frame.contains_point(p);
                           });
                       i != end(all_wave_views)) {
                wave_view_cursor_handler(*i, (*i)->cursor_up_ref, left, right,
                                         p);
            }
            last_cursor_window = nullptr;
        };
        prog.run_windows = [&] {
            process_audio_queue();
            process_midi_queue();
        
            // wave views:
            int i=0;
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
            for(auto wave_view_ptr: all_wave_views) {
                const auto wave_frame = wave_view_ptr->frame();
                ImGui::SetNextWindowSize(wave_frame.size, ImGuiSetCond_Always);
                ImGui::SetNextWindowPos(wave_frame.origin, ImGuiSetCond_Always);
                ImGui::Begin(
                    to_string(i++).c_str(), &wave_view_ptr->show,
                    ImGuiWindowFlags_ShowBorders | ImGuiWindowFlags_NoTitleBar |
                        ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoMove |
                        ImGuiWindowFlags_NoResize);
                wave_view_ptr->run();
                ImGui::End();
            }
            ImGui::PopStyleVar();
            
            // console:
            repl->draw("lua 5.3", &show_console);
            
            // ImGui::ShowTestWindow();
            // ShowExampleAppCustomNodeGraph(&show_console);
        };
        prog.idle = [&](const size_t ms_to_spare) {
        /*
            lua_getglobal(repl->lua, "idle");
            if (lua_type(repl->lua, -1) == LUA_TFUNCTION) {
                lua_pushinteger(repl->lua, ms_to_spare);
                if(lua_pcall(repl->lua, 1, 0, 0)) {
                    cerr << "idle error: " << lua_tostring(repl->lua, -1) << endl;
                    lua_pop(repl->lua, 1);
                }
            }
            lua_pop(repl->lua, 1);
        */
        };
        prog.cleanup = [&] {
            midi_in->closePort();
            midi_out->closePort();
            audio_io.deinit();
        };
    
        return args.count("test") > 0
            ? imgui_test_program_main(prog, argc, argv)
            : imgui_program_main(prog, argc, argv);
    
    } CATCH_AND_PRINT;
    
    return -1;
}
