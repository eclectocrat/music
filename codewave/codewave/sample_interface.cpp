//
//  sample_interface.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/18/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "sample_interface.h"
#include "lua_script_util.h"
#include "braille_canvas.h"
#include "n_ranges_for.h"
#include "lua_sample_util.h"
#include "ranges_interface.h"

using namespace ec;
using namespace std;
using namespace mpark;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
namespace ec {
namespace lua {

Rect<float> get_rect(lua_State* ls, const char *name) {
    lua::check_is_table(ls, -1);
    lua_getfield(ls, -1, name);
    lua::check_is_table(ls, -1);
    lua_len(ls, -1);
    const auto len = lua_tointeger(ls, -1);
    lua_pop(ls, 1);
    if(len < 4) {
        lua_pop(ls, 1);
        return Rect<float>();
    } else {
        lua_rawgeti(ls, -1, 1);
        const auto x = lua_tonumber(ls, -1);
        lua_pop(ls, 1);
        lua_rawgeti(ls, -1, 2);
        const auto y = lua_tonumber(ls, -1);
        lua_pop(ls, 1);
        lua_rawgeti(ls, -1, 3);
        const auto w = lua_tonumber(ls, -1);
        lua_pop(ls, 1);
        lua_rawgeti(ls, -1, 4);
        const auto h = lua_tonumber(ls, -1);
        lua_pop(ls, 2); // + pop get_field(name)
        return Rect<float>(x, y, w, h);
    }
}

} // namespace lua
} // namespace ec

void Weak_self::make_weak_self(lua_State* ls) {
    lua_createtable(ls, 0, 1);          // self, {}
    lua_pushstring(ls, "v");            // self, {}, "v"
    lua_setfield(ls, -2, "__mode");     // self, {__mode="v"}
    lua_pushnil(ls);                    // self, {__mode="v"}, nil
    lua_copy(ls, -3, -1);               // self, {__mode="v"}, self
    lua_setfield(ls, -2, "self");       // self, {__mode="v", self=self}
    lua::update_ref(ls, weak_self_ref); // self
}

void Weak_self::free_weak_self(lua_State* ls) {
    if (weak_self_ref != LUA_REFNIL) {
        luaL_unref(ls, LUA_REGISTRYINDEX, weak_self_ref);
    }
}

void Weak_self::push_weak_self(lua_State *ls) {
    expects(weak_self_ref != LUA_REFNIL);
    lua_rawgeti(ls, LUA_REGISTRYINDEX, weak_self_ref);  // {}|nil
    luaL_checktype(ls, -1, LUA_TTABLE);                 // {}
    lua_getfield(ls, -1, "self");                       // {}, self|nil
    lua_remove(ls, -2);                                 // self|nil
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
list<Wave *> all_waves;

static const char *WAVE_TYPE = "ec.Wave";

int Wave_create(lua_State *ls) {
    all_waves.push_back(lua::create_object<Wave>(ls, WAVE_TYPE));
    return 1;
}

int Wave_destroy(lua_State *ls) {
    auto wave = lua::destroy_object<Wave>(ls, WAVE_TYPE);
    if (not wave) {
        lua_settop(ls, 0);
    } else if (auto i = find(begin(all_waves), end(all_waves), wave);
               i != end(all_waves)) {
        all_waves.erase(i);
    }
    return 0;
}

int Wave_clone(lua_State *ls) {
    auto clone =
        lua::clone_object(ls, WAVE_TYPE, lua::checked_get<Wave*>(ls, 1));
    all_waves.push_back(clone);
    return 1;
}

int Wave_open_stream(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    self->open_stream(lua::param<string>(ls, "file_path"));
    return 0;
}

int Wave_map_stream_samples(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    if (self->empty()) {
        luaL_error(ls,
                   "[Wave] unable to map stream samples, no clips are loaded!");
    } else {
        for_each(self->clips().begin(), self->clips().end(), [&](auto &c) {
            if (not c.is_stream_open()) {
                luaL_error(
                    ls,
                    "[Wave] unable to map stream samples, no stream is open!");
            }
            // NOTE: assumes that all clips in the wave are sourced from the
            // same stream and haven't been shuffled. If _stream_range and
            // _file_stream are out of sync or heterogenous across clips, then
            // bizarro land will manifest on the tip of your cat's tail.
            c.map_stream_frames(c.range());
        });
    }
    return 0;
}

int Wave_unmap_samples(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    for_each(self->clips().begin(), self->clips().end(),
             [](auto &c) { c.unmap_frames(); });
    return 0;
}

int Wave_frame_count(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    lua_pushinteger(ls, int(self->frame_count()));
    return 1;
}

int Wave_channel_count(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    lua_pushinteger(ls, int(self->channel_count()));
    return 1;
}

int Wave_copy_mapped_samples(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    auto chan = lua::param<int>(ls, "channel");
    const auto ranges = lua::as_ranges(ls, self->frame_count(), "ranges");
    lua_createtable(ls, int(size(ranges)), 0);
    int i = 1;
    for_each(begin(ranges), end(ranges), [&](auto & range) {
        self->for_range(range, [](auto &c) { return c.range(); },
                        [&](auto &c) -> auto & { return c.samples()[chan]; },
                        [&](float f) {
                            lua_pushnumber(ls, f);
                            lua_rawseti(ls, -2, i++);
                        });
    });
    return 1;
}

int Wave_write_mapped_samples(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    auto chan = lua::param<int>(ls, "channel");
    const auto begin = lua::get_pos(ls, "to", self);
    lua_getfield(ls, -1, "buffer");
    luaL_checktype(ls, -1, LUA_TTABLE);
    lua_len(ls, -1);
    const size_t buffer_len = lua_tonumber(ls, -1);
    lua_pop(ls, 1);
    int i = 1;
    self->for_range(Sample_range(begin, begin + buffer_len),
                    [](auto &c) { return c.range(); },
                    [&](auto &c) -> auto & { return c.samples()[chan]; },
                    [&](float &f) {
                        lua_rawgeti(ls, -1, i++);
                        f = lua_tonumber(ls, -1);
                        lua_pop(ls, 1);
                    });
    return 0;
}

int Wave_remove(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    const auto ranges = lua::as_ranges(ls, self->frame_count(), "ranges");
    for_each(rbegin(ranges), rend(ranges), [&](auto & range) {
        self->erase(range);
    });
    return 0;
}

int Wave_take(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    auto from = lua::param<Wave *>(ls, "from");
    const auto ranges = lua::as_ranges(ls, self->frame_count(), "ranges");
    const auto to = lua::get_pos(ls, "to", self);
    for_each(rbegin(ranges), rend(ranges), [&](auto & range) {
        self->take(range, *from, size_t(to));
    });
    return 0;
}

int Wave_copy(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    auto from = lua::param<Wave *>(ls, "from");
    const auto ranges = lua::as_ranges(ls, self->frame_count(), "ranges");
    const auto to = lua::get_pos(ls, "to", self);
    for_each(rbegin(ranges), rend(ranges), [&](auto & range) {
        self->copy(range, *from, size_t(to));
    });
    return 0;
}

int Wave_print(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    Braille_canvas canvas({100, 44});
    const auto src = self->clips().front().rms();
    const auto src_len = src[0].size();
    const auto pts_per_channel = canvas.point_size().h / self->channel_count();
    for_all_n_ranges(
        N_ranges<size_t>(src_len, canvas.point_size().w),
        [&](auto i, auto range) {
            const auto rms = min(1.f, *max_element(begin(src[0]) + begin(range),
                                                   begin(src[0]) + end(range)));
            const auto rms_h = rms * pts_per_channel / 2;
            const auto center = pts_per_channel / 2;
            canvas.set_vline(i, center, center - rms_h);
            canvas.set_vline(i, center, center + rms_h + 1);
            if (src.size() > 1) {
                const auto rms_r =
                    min(1.f, *max_element(begin(src[1]) + begin(range),
                                          begin(src[1]) + end(range)));
                const auto rms_r_h = rms_r * pts_per_channel / 2;
                const auto center_r = center + pts_per_channel;
                canvas.set_vline(i, center_r, center_r - rms_r_h);
                canvas.set_vline(i, center_r, center_r + rms_r_h + 1);
            }
        });
    canvas.draw(wcout);
    wcout.flush();
    return 0;
}

static const luaL_Reg Wave_funcs[] = {{"new", Wave_create}, {0, 0}};
static const luaL_Reg Wave_meths[] = {
    {"__gc", Wave_destroy},
    {"print", Wave_print},
    {"make_copy", Wave_clone},
    {"open_stream", Wave_open_stream},
    {"get_frame_count", Wave_frame_count},
    {"get_channel_count", Wave_channel_count},
    {"map_stream_frames", Wave_map_stream_samples},
    {"unmap_frames", Wave_unmap_samples},
    {"copy_frames", Wave_copy_mapped_samples},
    {"write", Wave_write_mapped_samples},
    {"remove", Wave_remove},
    {"take", Wave_take},
    {"copy_from", Wave_copy},
    {0, 0}};

/*
Wave(W) ::
    ; Low level mutating interface
    make_copy           :: Wave -> Wave
    open_stream         :: Wave -> String -> Void
    let frame_count     :: Int
    let channel_count   :: Int (where 0 <= channel_count <= 2)
    map_stream_frames   :: Wave -> Void
    unmap_frames        :: Wave -> Void
    copy_frames         :: Wave -> [Real]
    write               :: Wave -> [Real] -> channel:Int -> Pos -> Void
    remove              :: Wave -> Ranges -> Void
    take                :: Wave -> from:Wave -> Ranges -> Void
    copy_from           :: Wave -> from:Wave -> Ranges -> Void
 
    remove_front        :: Wave -> Pos -> Void
    remove_back         :: Wave -> Pos -> Void
    crop                :: Wave -> Range -> Void
 
    ; Functional-immutable interface
    removed     :: Wave -> remove:Ranges -> Wave
    inserted    :: Wave -> insert:Wave -> at:Pos -> Wave
    reversed    :: Wave -> Wave
    front       :: Wave -> until:Pos -> Wave
    back        :: Wave -> from:Pos -> Wave
    subranges   :: Wave -> subranges:Ranges -> Wave
    processed   :: Wave -> process:Process -> Wave
*/

void ec::register_wave_model(lua_State *lua) {
    ec::lua::register_class(lua, "Wave", WAVE_TYPE, Wave_meths, Wave_funcs);
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
list<Wave_view *> all_wave_views;

static const char *WAVE_VIEW_TYPE = "ec.Wave_view";

int Wave_view_create(lua_State *ls) {
    auto self = lua::create_object<Wave_view>(ls, WAVE_VIEW_TYPE);
    all_wave_views.insert(end(all_wave_views), self);
    self->make_weak_self(ls);
    return 1;
}

int Wave_view_destroy(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    if (auto i = find(begin(all_wave_views), end(all_wave_views), self);
        i != end(all_wave_views)) {
        all_wave_views.erase(i);
    }
    expects(self->weak_self_ref != LUA_REFNIL);
    self->free_weak_self(ls);
    return 0;
}

int Wave_view_set_wave(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    auto wave = lua::param<Wave *>(ls, "wave");
    lua_getfield(ls, -1, "wave"); // ref table, not userdata
    const int r = luaL_ref(ls, LUA_REGISTRYINDEX);
    self->set_sample(shared_ptr<Sample>(
        wave, [=](auto) { luaL_unref(ls, LUA_REGISTRYINDEX, r); }));
    return 0;
}

int Wave_view_set_show(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    auto show = lua::param<int>(ls, "show");
    self->show = show != 0;
    return 0;
}

int Wave_view_add_head(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    auto head = lua::param<Wave_head *>(ls, "head");
    lua_getfield(ls, -1, "head"); // ref table, not userdata
    const int r = luaL_ref(ls, LUA_REGISTRYINDEX);
    self->insert_head(shared_ptr<Sample_head>(
        head, [=](auto) { luaL_unref(ls, LUA_REGISTRYINDEX, r); }));
    return 0;
}

int Wave_view_remove_head(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    auto head = lua::param<Wave_head *>(ls, "head");
    self->remove_head(shared_ptr<Sample_head>(head, [](auto) {}));
    return 0;
}

int Wave_view_set_cursor_down_handler(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    lua_getfield(ls, -1, "cursor_down");
    luaL_checktype(ls, -1, LUA_TFUNCTION);
    lua::update_ref(ls, self->cursor_down_ref);
    return 0;
}

int Wave_view_set_cursor_up_handler(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    lua_getfield(ls, -1, "cursor_up");
    luaL_checktype(ls, -1, LUA_TFUNCTION);
    lua::update_ref(ls, self->cursor_up_ref);
    return 0;
}

int Wave_view_set_cursor_moved_handler(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    lua_getfield(ls, -1, "cursor_moved");
    luaL_checktype(ls, -1, LUA_TFUNCTION);
    lua::update_ref(ls, self->cursor_moved_ref);
    return 0;
}

int Wave_view_set_frame(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    self->set_frame(lua::get_rect(ls, "rect"));
    return 0;
}

///
/// inserts a range, or ranges into a selection.
///     @param range/s {Range, Ranges, Interval} range or ranges to insert
///     @pre self->sample() != nullptr
///     @post contains(self->selection(index), range/s) == true
///
int Wave_view_insert_selection(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    auto ranges = lua::as_ranges(ls, self->sample()->frame_count(), "ranges");
    int index=0;
    lua_getfield(ls, -1, "index");
    if (lua_isinteger(ls, -1)) {
        index = static_cast<int>(lua_tointeger(ls, -1));
    }
    lua_pop(ls, 1);
    // cout << "Selecting (" << get<0>(range).begin() << ", " << get<0>(range).end() << "]" << endl;
    for (auto range: ranges) {
        self->insert_selection(range, index);
    }
    return 0;
}

int Wave_view_remove_selection(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    auto range = lua::get_range(ls, "range", self->sample().get());
    int index=0;
    lua_getfield(ls, 1, "index");
    if (lua_isinteger(ls, -1)) {
        index = static_cast<int>(lua_tointeger(ls, -1));
    }
    lua_pop(ls, 1);
    self->remove_selection(get<0>(range), index);
    return 0;
}

int Wave_view_selection(lua_State *ls) {
    auto self = lua::checked_get<Wave_view *>(ls, 1);
    int index=0;
    lua_getfield(ls, 1, "index");
    if (lua_isinteger(ls, -1)) {
        index = static_cast<int>(lua_tointeger(ls, -1));
    }
    lua_pop(ls, 1);
    auto l = list<Sample_range>();
    copy(self->selection_begin(index), self->selection_end(index),
         back_inserter(l));
    lua::create_object<Ranges_t>(ls, RANGES_TYPE);
    auto ranges = reinterpret_cast<Ranges_t*>(lua_touserdata(ls, -1));
    *ranges = l;
    return 1;
}

static const luaL_Reg Wave_view_funcs[] = {{"new", Wave_view_create}, {0, 0}};
static const luaL_Reg Wave_view_meths[] = {
    {"__gc", Wave_view_destroy},
    
    // Head interface
    {"set_wave",    Wave_view_set_wave},
    {"add_head",    Wave_view_add_head},
    {"remove_head", Wave_view_remove_head},
    
    // Highlight interface
    {"select",      Wave_view_insert_selection},
    {"deselect",    Wave_view_remove_selection},
    {"selection",   Wave_view_selection},
    
    // UI hooks interface
    {"set_shown", Wave_view_set_show},
    {"set_cursor_down", Wave_view_set_cursor_down_handler},
    {"set_cursor_up", Wave_view_set_cursor_up_handler},
    {"set_cursor_moved", Wave_view_set_cursor_moved_handler},
    {"set_frame", Wave_view_set_frame},
    
    {0, 0}};

/*
Wave_view(V) ::
    var wave        : Wave
    var heads       : [Wave_head]
    var selection   : [Ranges]
    var shown       : Bool
    var shown_range : Ranges ; TODO
    cursor_down     : id:UUID -> x:Int -> y:Int -> Void
    cursor_up       : id:UUID -> x:Int -> y:Int -> Void
    cursor_moved    : id:UUID -> x:Int -> y:Int -> Void
*/

void ec::register_wave_view_model(lua_State *lua) {
    ec::lua::register_class(lua, "Wave_view", WAVE_VIEW_TYPE, Wave_view_meths,
                            Wave_view_funcs);
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
list<Wave_head *> all_wave_heads;

static const char *WAVE_HEAD_TYPE = "ec.Wve_head_type";

int Wave_head_create(lua_State *ls) {
    auto self = lua::create_object<Wave_head>(ls, WAVE_HEAD_TYPE);
    all_wave_heads.insert(end(all_wave_heads), self);
    self->make_weak_self(ls);
    return 1;
}

int Wave_head_destroy(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    if (auto i = find(begin(all_wave_heads), end(all_wave_heads), self);
        i != end(all_wave_heads)) {
        all_wave_heads.erase(i);
    }
    expects(self->weak_self_ref != LUA_REFNIL);
    self->free_weak_self(ls);
    return 0;
}

int Wave_head_attach_to_wave(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    auto wave = lua::param<Wave *>(ls, "wave");
    const auto range = lua::get_discrete_range(ls, "range", wave);
    if (range.end() > wave->frame_count()) {
        luaL_error(ls, "Range out of bounds!");
    }
    lua_getfield(ls, -1, "wave");
    const int r = luaL_ref(ls, LUA_REGISTRYINDEX);
    self->attach(shared_ptr<Sample>(
                     wave, [=](auto) { luaL_unref(ls, LUA_REGISTRYINDEX, r); }),
                 range.empty() ? wave->frame_count() : range, 1.0, 0);
    return 0;
}

int Wave_head_set_dir(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    auto dir = lua::param<int>(ls, "dir");
    self->set_dir(dir < 0 ? Sample_head::Direction::backward
                          : Sample_head::Direction::forward);
    return 0;
}

int Wave_head_dir(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    lua_pushinteger(ls,
                    self->dir() == Sample_head::Direction::forward ? 1 : -1);
    return 1;
}

int Wave_head_set_pos(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    auto pos = lua::param<int>(ls, "pos");
    self->set_pos(size_t(pos));
    return 0;
}

int Wave_head_pos(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    lua_pushinteger(ls, int(self->pos()));
    return 1;
}

int Wave_head_set_normal_pos(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    auto normal_pos = lua::param<float>(ls, "normal");
    if (not self->attached()) {
        luaL_error(ls, "No Wave attached to head!");
    } else {
        self->set_pos(size_t(normal_pos * float(self->range_size()) +
                             self->range_begin()));
    }
    return 0;
}

int Wave_head_normal_pos(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    lua_pushinteger(ls, self->attached()
                            ? int(float(self->pos()) /
                                  float(self->attached()->frame_count()))
                            : 0);
    return 1;
}

int Wave_head_set_speed(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    auto speed = lua::param<float>(ls, "speed");
    self->set_speed(double(speed));
    return 0;
}

int Wave_head_speed(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    lua_pushinteger(ls, self->speed());
    return 1;
}

int Wave_head_set_amp(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    auto amp = lua::param<float>(ls, "amp");
    self->set_amp(double(amp));
    return 0;
}

int Wave_head_amp(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    lua_pushinteger(ls, self->amp());
    return 1;
}

int Wave_head_set_loop(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    auto loop = lua::param<bool>(ls, "loop");
    self->set_loop(loop);
    return 0;
}

int Wave_head_loop(lua_State *ls) {
    auto self = lua::checked_get<Wave_head *>(ls, 1);
    lua_pushboolean(ls, self->loop());
    return 1;
}

static const luaL_Reg Wave_head_funcs[] = {{"new", Wave_head_create}, {0, 0}};
static const luaL_Reg Wave_head_meths[] = {
    {"__gc", Wave_head_destroy},
    {"attach", Wave_head_attach_to_wave},
    {"set_dir", Wave_head_set_dir},
    {"get_dir", Wave_head_dir},
    {"set_pos", Wave_head_set_pos},
    {"get_pos", Wave_head_pos},
    {"get_normal", Wave_head_normal_pos},
    {"set_normal", Wave_head_set_normal_pos},
    {"set_speed", Wave_head_set_speed},
    {"get_speed", Wave_head_speed},
    {"set_amp", Wave_head_set_amp},
    {"get_amp", Wave_head_amp},
    {"set_loop", Wave_head_set_loop},
    {"get_loop", Wave_head_loop},
    {0, 0}};

/*
Wave_head(H) ::
    var wave        : Wave
    var dir         : Forward or Backward
    var pos         : Int (where pos < wave.frame_count)
    var normal      : Real (where 0 <= normal <= 1)
    var speed       : Real (where min_speed <= speed <= max_speed)
    let max_speed   : Real (where max_speed <- wave.frame_count)
    let min_speed   : Real (where min_speed <- wave.frame_count)
    var amp         : Real
    var loop        : Bool
*/

void ec::register_wave_head_model(lua_State *lua) {
    ec::lua::register_class(lua, "Wave_head", WAVE_HEAD_TYPE, Wave_head_meths,
                            Wave_head_funcs);
}
