//
//  imgui_sample_view.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 6/13/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "imgui_sample_view.h"
#include "n_ranges_for.h"
#include <imgui/imgui_internal.h>

using namespace ec;
using namespace std;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void imgui_Sample_view::run() {
    using namespace ImGui;
    auto s = sample();
    ImGuiWindow *window = GetCurrentWindow();
    last_drawn_frame = {{size_t(window->Pos.x), size_t(window->Pos.y)},
                        {size_t(window->Size.x), size_t(window->Size.y)}};
    if (window->SkipItems or not s) {
        return;
    }
    const ImGuiStyle &style = GImGui->Style;
    const auto frame_rect = frame();
    ItemSize(frame_rect.size, style.FramePadding.y);
    if (not ItemAdd(ImVec4(frame_rect), nullptr)) {
        return;
    }
    if (not s->empty()) {
        int hovered = -1;
        if (IsHovered(ImVec4(frame_rect), 0)) {
            const auto d =
                ImClamp((GImGui->IO.MousePos.x - frame_rect.bottom_left().x) /
                            frame_rect.size.w,
                        0.0f, 0.9999f);
            hovered = s->frame_count() * d;
        }
        draw(frame_rect, hovered);
    }
}

void imgui_Sample_view::draw(Rect<float> const &frame,
                             const size_t cursor_frame) {
    auto s = sample();
    expects(s != nullptr);
    auto draw_list = ImGui::GetWindowDrawList();
    if (s->clips().empty()) {
        draw_empty(frame, wave_color, draw_list);
    } else {
        auto &c = s->clips().front();
        if (c.rms().empty()) {
            c.render_stream_rms(frame.size.w);
            expects(not c.rms().empty());
        }
        draw_rms(c.rms(), frame, wave_color, draw_list);
        auto x = float(cursor_frame) / float(s->frame_count()) * frame.size.w;
        draw_list->AddLine({frame.min_x() + x, frame.min_y()},
                           {frame.min_x() + x, frame.max_y()}, cursor_color);
        float head_y = 10.f;
        for (auto head : heads()) {
            auto f = [&](size_t n) {
                return n / float(s->frame_count()) * frame.size.w;
            };
            x = f(head->pos());
            const auto range_begin = f(head->range_begin());
            const auto range_end = f(head->range_end());
            draw_list->AddLine({frame.min_x() + x, frame.min_y()},
                               {frame.min_x() + x, frame.max_y()},
                               cursor_color);
            draw_list->AddLine(
                {frame.min_x() + x, frame.min_y() + head_y},
                {frame.min_x() + range_end, frame.min_y() + head_y},
                cursor_color);
            draw_list->AddLine(
                {frame.min_x() + range_begin, frame.min_y() + head_y},
                {frame.min_x() + x, frame.min_y() + head_y},
                cursor_track_dark_color);
            head_y += 7.5f;
        }
    }
}

void imgui_Sample_view::draw_selections(Rect<float> const &frame) {
    for (auto selection: selection_map()) {
        
    }
}

void imgui_Sample_view::draw_rms(vector<Sample_buffer> const &rms,
                                 Rect<float> const &r, const ImColor color,
                                 ImDrawList *draw_list) {
    expects(not rms.empty());
    const float height = r.size.h / rms.size();

    for (size_t chan = 0; chan < rms.size(); ++chan) {
        for_all_n_ranges(
            N_ranges<size_t>(rms[0].size(), r.size.w), [&](auto p, auto rng) {
                if (rng.empty()) {
                    rng = Range(rng.begin()-1, rng.begin());
                }
                for (size_t i = rng.begin(); i < rng.end(); ++i) {
                    const auto len = height * rms[chan][i];
                    const auto center =
                        r.origin.y + (height * chan) + height / 2;
                    draw_list->AddLine(
                        Point<float>(r.origin.x + p, center - len),
                        Point<float>(r.origin.x + p, center + len), color);
                }
            });
    }
}

void imgui_Sample_view::draw_empty(Rect<float> const &rect, const ImColor color,
                                   ImDrawList *draw_list) {
    const auto r = rect.inset({4, 20}).translated({0, 4});
    draw_list->AddRect(r.bottom_left(), r.top_right(), color);
    draw_list->AddLine(r.top_left(), r.bottom_right(), color);
    draw_list->AddLine(r.bottom_left(), r.top_right(), color);
}
