//
//  sample_interface.hpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/18/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef sample_interface_h
#define sample_interface_h

#include "lua/lua.hpp"
#include "sample.h"
#include "imgui_sample_view.h"
#include "sample_head.h"
#include "geometry.h"
#include "std_variant.hpp"
#include <tuple>

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
struct Weak_self {
    int weak_self_ref = LUA_REFNIL;
    void make_weak_self(lua_State*);
    void free_weak_self(lua_State*);
    void push_weak_self(lua_State*); // push self table or nil onto stack
};

struct Wave: Sample {};

struct Wave_view: imgui_Sample_view, Weak_self {
    bool show = true;
    int cursor_down_ref = LUA_REFNIL;
    int cursor_up_ref = LUA_REFNIL;
    int cursor_moved_ref = LUA_REFNIL;
    bool keep_cursor_while_down = false;
    
    void set_frame(Rect<float> const& r) { _frame = r; }
    Rect<float> frame() override { return _frame; }

private:
    Rect<float> _frame = {100, 100, 250, 75};
};

struct Wave_head: Sample_head, Weak_self {};

// model entries
void register_wave_model(lua_State*);
void register_wave_view_model(lua_State*);
void register_wave_head_model(lua_State*);

} // END namespace
#endif /* sample_interface_h */
