
-- MIDI events ----------------------------------------------------------------
-------------------------------------------------------------------------------

math.randomseed(0)

c4 = 24

note_pool = gen_array(12, function (n) 
	return c4 + math.random(24)
end)

clock_division = 6

probability = .3

playing = nil

gate = 64

current_gate = 0

channel = 2

MIDI.receive_clock = function (note, velocity)
	clock_ticks = (clock_ticks or 0) + 1
	-- DEBUG
	-- if clock_ticks % 24 == 0 then print("TICK") end
	if playing then
		current_gate = current_gate - 1
		if current_gate < 1 then
			MIDI.send_note_off(channel, playing)
			playing = nil
		end
	end
	if clock_ticks % clock_division == 0 then
		if math.random() < probability then
			-- add slide here
			if playing then
				MIDI.send_note_off(channel, playing)
				playing = nil
			end
			playing = note_pool[math.random(#note_pool)]
			current_gate = gate
			MIDI.send_note_on(channel, playing)
		end
	end
end