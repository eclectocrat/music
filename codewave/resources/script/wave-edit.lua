	
-- App objects ----------------------------------------------------------------
-------------------------------------------------------------------------------

wave = Wave.load("audio/drone.aif")
head = Wave_head.new()

overview = Wave_view.new()
overview.wave = wave

subview = Wave_view.new()
subview.wave = wave

-- UI events ------------------------------------------------------------------
-------------------------------------------------------------------------------

overview.cursor_down = function(self, left, right, x, y)
	print('down')
	if left then
		self.cursor_down_time = Time.now()
		self.range_start = x
	end
end

overview.cursor_moved = function(self, left, right, x, y)
	assert(x)
	assert(y)
	print('moved')
	if not self.range_start then 
		return 
	end
	zoomed = Ranges{math.min(self.range_start, x), math.max(self.range_start, x)}
	self:select{range = zoomed}
	-- subview:set_wave{wave = wave >> selection}
	-- subview_range = range
end

overview.cursor_up = function(self, left, right, x, y)
	print('up')
	self.range_start = nil

	-- reset selection with a click anywhere in detail view
	local temporally_near = Time.now() - self.cursor_down_time < 300
	local spacially_near = math.abs(range_start - x) < 4
	if temporally_near and spacially_near then
		print("deselect all")
		zoomed = {.0, 1.0}
		-- subview:select{}
	end
end

function delete()
	table.insert(history, wave)
	wave = wave >> removed{range=sel}
	overview.wave, subview.wave = wave, wave
end

function crop()
	table.insert(history, wave)
	wave = wave >> subrange{range=sel}
	overview.wave, subview.wave = wave, wave
end

-- MIDI events ----------------------------------------------------------------
-------------------------------------------------------------------------------

instr_channel = 1

function filter_channel(func)
	return function (chan, byte1, byte2)
		if chan == instr_channel then func(byte1, byte2) end
	end
end

MIDI.receive_clock = function (note, velocity)
	clock_ticks = (clock_ticks or 0) + 1
	if clock_ticks % 24 == 0 then
		print("TICK")
	end
end

-- Window events --------------------------------------------------------------
-------------------------------------------------------------------------------

function inset(rect, w, h)
	return {rect[1] + w, rect[2] + h, rect[3] - w*2, rect[4] - h*2}
end

Window.size_did_change = function(size) 
	overview:set_frame{rect = inset({0, 0, size.w, size.h*.25}, 5, 5)}
	subview:set_frame{rect = inset({0, size.h*.25, size.w, size.h*.75}, 5, 5)}
end

-- Audio events ---------------------------------------------------------------
-------------------------------------------------------------------------------

Audio.pre_clock_tick = function(stream_time) end

-- show heads on both views
Audio.head_was_added = function(head)
	view:add_head{head=head}
	overview:add_head{head=head}
end

Audio.head_was_removed = function(head)
	view:remove_head{head=head}
	overview:add_head{head=head}
end
