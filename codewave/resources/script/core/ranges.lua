	
-------------------------------------------------------------------------------

getmetatable(Ranges).__call = function(_, parms)
	local r = Ranges.new()
	if #parms == 0 then
		return r
	elseif #parms == 1 then
		if type(parms[1]) ~= 'table' or #parms[1] < 2 then
		end
		r:insert{range=parms[1]}
	else
		if type(parms[1]) == 'number' and type(parms[2]) == 'number' then
			r:insert{range=parms}
			return r
		else
			for range in values(parms) do
				r:insert{range=range}
			end
			return r
		end
	end
end

-------------------------------------------------------------------------------

function Ranges.remove_front(self, parms)
	self:remove{range={0, parms.to}}
end

function Ranges.remove_back(self, parms)
	self:remove{range={parms.from, self.frame_count}}
end

function Ranges.subranges(self, parms)
	self:set_range(parms)
end

-------------------------------------------------------------------------------

removed_ranges 		= lazy_composable_method(Ranges, "remove")
inserted_ranges 	= lazy_composable_method(Ranges, "insert")
reversed_ranges 	= lazy_composable_method(Ranges, "reverse")
front_ranges 		= lazy_composable_method(Ranges, "remove_back")
back_ranges 		= lazy_composable_method(Ranges, "remove_front")
subranges_ranges 	= lazy_composable_method(Ranges, "subranges")
