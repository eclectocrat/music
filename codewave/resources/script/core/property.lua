
-------------------------------------------------------------------------------

Property = {
	define_interfaces = function (obj, props)
		local mt = getmetatable(obj)
		mt.__index = function (t, k)
			return props[k] 
				and props[k].get(t) 
				or (rawget(mt, k) or rawget(t, k))
		end
		mt.__newindex = function (t, k, v)
			if props[k] 
				then props[k].set(t, v)
				else rawset(t, k, v)
		 	end
		end
	end,

	interface = function (name) 
		local getter = 'get_' .. name
		local setter = 'set_' .. name
		return {
			get = function(t) 
				if not t[getter] then 
					error("Can't read write-only value `" .. name .. "`.")
				end
				return t[getter](t) 
			end,
			set = function(t, v) 
				if not t[setter] then 
					error("Can't set read-only value `" .. name .. "`.")
				end
				t[setter](t, {[name]=v}) 
			end,
		}
	end,

	array_interface = function (name)
		local getter = 'get_' .. name
		local setter = 'set_' .. name
		local counter = 'count_' .. name
		return {
			-- getter returns a modified array to access each array element
			-- individually through a metatable operator overloads.
			get = function(t) 
				if not t[getter] then 
					error("Can't read write-only value `" .. name .. "`.")
				end
				local array_accessor = {}
				setmetatable(array_accessor, {
					__index = function (_, k)
						return t[getter](t, k)
					end,
					__newindex = function(_, k, v)
						t[setter](t, k, v)
					end,
					__len = function (_)
						return t[counter](t)
					end,
				})
				return array_accessors
			end,
			-- setter allows you to set the entire array at once.
			set = function(t, v) 
				if not t[setter] then 
					error("Can't set read-only value `" .. name .. "`.")
				end
				t[setter](t, {[name]=v}) 
			end,
		}
	end,
}

-------------------------------------------------------------------------------

-- Properties for Wave
Property.define_interfaces(Wave, {
	frame_count 	= Property.interface('frame_count'),
	channel_count 	= Property.interface('channel_count'),
})


-- Properties for Wave_head
Property.define_interfaces(Wave_head, {
	amp		= Property.interface('amp'),
	dir 	= Property.interface('dir'),
	pos 	= Property.interface('pos'),
	normal 	= Property.interface('normal'),
	speed	= Property.interface('speed'),
	loop	= Property.interface('loop'),
})


-- Properties for Wave_view
Property.define_interfaces(Wave_view, {
	wave 		= Property.interface('wave'),
	cursor_down = Property.interface('cursor_down'),
	cursor_up 	= Property.interface('cursor_up'),
	cursor_moved= Property.interface('cursor_moved'),
})


-- Ranges
Property.define_interfaces(Ranges, {
	all = Property.interface('all'),
})


-- Properties for core services: Audio, MIDI
function setter(t, k, v)
	local direct = rawget(t, 'set_' .. k)
	if direct ~= nil 
		then direct(t, v) 
		else rawset(t, k, v)
	end
end

setmetatable(MIDI, { __newindex = setter })
setmetatable(Audio, { __newindex = setter })
setmetatable(Window, { __newindex = setter })
