
function n_ranges_transform_location(
    input_size, output_size, inputs_per_output, remainder_ratio, n)

    assert(n <= output_size)
    
    if remainder_ratio.first == 0 then
        return n * inputs_per_output
    elseif n == output_size then
        return input_size
    else
        return n * inputs_per_output +
               (n * remainder_ratio.num / remainder_ratio.den) +
               ((n * remainder_ratio.num % remainder_ratio.den < remainder_ratio.num) 
               		and 0 
               		or 1)
    end
end