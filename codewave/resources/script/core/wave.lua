	
-------------------------------------------------------------------------------

function Wave.load(file)
	local w = Wave.new()
	w:open_stream{file_path=file}
	w:map_stream_frames{}
	return w
end

-------------------------------------------------------------------------------

function Wave.assert_wave(self, w)
	assert(getmetatable(w) == getmetatable(Wave))
end

function Wave.assert_loc(self, n)
	assert(type(n) == 'number')
	assert(n >= 0 and n <= self.frame_count)
end

function Wave.assert_range(self, r)
	assert(type(r) == 'table')
	assert(#r >= 2)
	assert(r[1] >= 0 and r[1] <= self.frame_count)
	assert(r[2] >= 0 and r[2] <= self.frame_count)
end

-------------------------------------------------------------------------------

function Wave.remove_front(self, parms)
	self:assert_loc(parms.to)
	self:remove{range={0, parms.to}}
end

function Wave.remove_back(self, parms)
	self:assert_loc(parms.from)
	self:remove{range={parms.from, self.frame_count}}
end

function Wave.subrange(self, parms)
	self:assert_range(parms.range)
	self:set_range(parms)
end

-------------------------------------------------------------------------------

getmetatable(Wave).__shr = function(left, right)
	return right(left)
end

-------------------------------------------------------------------------------

removed_wave 	= lazy_composable_method(Wave, "remove")
inserted_wave 	= lazy_composable_method(Wave, "insert")
reversed_wave 	= lazy_composable_method(Wave, "reverse")
front_wave 		= lazy_composable_method(Wave, "remove_back")
back_wave 		= lazy_composable_method(Wave, "remove_front")
subranges_wave 	= lazy_composable_method(Wave, "subranges")

fade_in		= lazy_composable_method(Wave, "fade_in")
fade_out	= lazy_composable_method(Wave, "fade_out")
apply		= lazy_composable_method(Wave, "apply_fx")

-- TODO: Put somewhere more appropriate
-------------------------------------------------------------------------------
-- polymorphic functions:

removed 	= lazy_composable_polymorphic{removed_wave, 	removed_ranges}
inserted 	= lazy_composable_polymorphic{inserted_wave, 	inserted_ranges}
reversed 	= lazy_composable_polymorphic{reversed_wave, 	reversed_ranges}
front 		= lazy_composable_polymorphic{front_wave, 		front_ranges}
back 		= lazy_composable_polymorphic{back_wave, 		back_ranges}
subranges 	= lazy_composable_polymorphic{subranges_wave, 	subranges_ranges}
