
-------------------------------------------------------------------------------

function for_each(array, fun)
	for i=1,#(array or {}) do
		local obj = array[i]
		fun(obj)
	end
end

function gen_array(n, gen)
	local t = {}
	for i=1,n do table.insert(t, gen(n)) end
	return t
end

function find_index(t, pred)
	for i=1,#t do 
		if pred(t[i]) then return i end 
	end
	return #t+1
end

function matching(t, pred)
	local r = {}
	for i=1,#t do 
		if pred(t[i]) then table.insert(r, t[i]) end 
	end
	return r
end

function removed_matching(t, pred)
	local kept, not_kept = {}, {}
	for i=1,#t do 
		table.insert(pred(t[i]) and not_kept or kept, t[i]) 
	end
	return not_kept, kept
end

function values(t)
	local i = 0
	return function () 
		i = i + 1
		return t[i]
	end
end