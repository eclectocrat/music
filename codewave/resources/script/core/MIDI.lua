
-------------------------------------------------------------------------------

MIDI.codes = {
	NOTEON 				= 0x90 >> 4,
	NOTEOFF 			= 0x80 >> 4,
	POLYAFTERTOUCH 		= 0xA0 >> 4,
	CC 					= 0xB0 >> 4,
	CHANNELAFTERTOUCH 	= 0xD0 >> 4,
	SONGPOSPOINTER		= 0xF2,
	MIDITIMINGCLOCK 	= 0xF8,
	MIDISTART 			= 0xFA,
	MIDICONTINUE 		= 0xFB,
	MIDISTOP 			= 0xFC,
}

MIDI.receive = function (channel, msg_type, byte1, byte2)
	if msg_type == MIDI.codes.NOTEON then
		if type(MIDI.receive_note_on) == 'function' then
			MIDI.receive_note_on(channel, byte1, byte2)
		end
	elseif msg_type == MIDI.codes.NOTEOFF then
		if type(MIDI.receive_note_off) == 'function' then
			MIDI.receive_note_off(channel, byte1, byte2)
		end
	elseif msg_type == MIDI.codes.CHANNELAFTERTOUCH then
		if type(MIDI.receive_channel_aftertouch) == 'function' then
			MIDI.receive_channel_aftertouch(channel, byte1)
		end
	elseif msg_type == MIDI.codes.CC then
		if type(MIDI.receive_cc) == 'function' then
			MIDI.receive_cc(channel, byte1, byte2)
		end
	elseif msg_type == MIDI.codes.MIDITIMINGCLOCK then
		if type(MIDI.receive_clock) == 'function' then
			MIDI.receive_clock(channel, byte1, byte2)
		end
	end
end

MIDI.send_note_on = function (channel, note, velocity)
	assert(channel)
	assert(note)
	velocity = velocity or 127
	MIDI.send{ 0x90 | channel, note, velocity }
end

MIDI.send_note_off = function (channel, note)
	assert(channel)
	assert(note)
	MIDI.send{ 0x80 | channel, note }
end