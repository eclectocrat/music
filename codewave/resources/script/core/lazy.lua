
-------------------------------------------------------------------------------

function lazy_composable_function(func, name)
	local callable = {name=name}
	setmetatable(callable, {
		__call = function (_, parms)
			local self = {
				name=name,
				func=func,
				parms=parms,
			}
			setmetatable(self, {
				__bor = function(left, right)
					left.lazy = left.lazy or {}
					table.insert(left.lazy, right)
					return left				
				end,
				__call = function(_, input)
					copy = input:make_copy()
					assert(copy)
					self.func(copy, self.parms)
					for_each(self.lazy, function(f) 
						f.func(copy, f.parms) 
					end)
					return copy
				end,
			})
			return self
		end
	})
	return callable
end

-------------------------------------------------------------------------------

function lazy_composable_method(Class, name)
	local f = lazy_composable_function(Class[name], name)
	f.Class = getmetatable(Class) -- weak ref
	return f
end

-------------------------------------------------------------------------------

function lazy_composable_polymorphic(types)
	assert(#types > 0)
	local name = types[1].name
	local mt_map = {}
	for f in values(types) do
		if not f.Class then
			error("Can't compose non-lazy_composable object!")			
		end
		mt_map[f.Class] = f
	end
	return function(params)
		local self = {
			func = function(object, parms) 
				mt_map[getmetatable(object)](object, parms)
			end,
			parms = parms,
		}
		setmetatable(self, {
			__bor = function(left, right)
				left.lazy = left.lazy or {}
				table.insert(left.lazy, right)
				return left				
			end,
			__call = function(_, input)
				copy = input:make_copy()
				assert(copy)
				self.func(copy, self.parms)
				for_each(self.lazy, function(f) 
					f.func(copy, f.parms) 
				end)
				return copy
			end,
		})
		return self
	end
end