
-- Application ----------------------------------------------------------------
-------------------------------------------------------------------------------

math.randomseed(0)

w0 = Wave.load("audio/biggie.aif")
v0 = Wave_view.new()
v0:set_wave{wave=w0}

local sample_rate = 44100
local grains = 1024
free_heads = gen_array(grains, function (n) 
	local h = Wave_head.new()
	local b = math.random() * 0.8 + 0.1
	h:attach{
		wave =w0, 
		range={b, b + math.random() / 10.0}
	}
	return h
end)
busy_heads = {}

-- Instrument -----------------------------------------------------------------
-------------------------------------------------------------------------------

function set_note (wave, note)
	wave.base_speed = note / 63.0 + (math.random()/5.0) - .2
	wave.speed = wave.base_speed
	wave.note = note
	return wave
end

function head(parms)
	if #free_heads == 0 then return nil end
	local h = table.remove(free_heads)
	set_note(h, parms.note or 63)
	h.amp 		= parms.amp 	or 1.0
	h.normal 	= parms.normal 	or 0.0
	h.dir 		= parms.dir 	or 1
	h.loop 		= parms.loop 	or false
	return h
end

function filter_channel(func)
	return function (chan, byte1, byte2)
		if chan == instr_channel then func(byte1, byte2) end
	end
end

function random_dir()
	return math.random(2) == 2 and -1 or 1
end

function play_next_head(note, velocity)
	assert(note)
	assert(velocity)
	play(
		head{
			note 	= note, 
			amp 	= velocity / 4096, 
			normal	= .2 + math.random() / 5.0, 
			dir 	= random_dir(), 
			loop 	= true
		})
end

-- MIDI dispatch --------------------------------------------------------------
-------------------------------------------------------------------------------

instr_channel = 0

MIDI.receive_note_on = filter_channel(function (note, velocity)
	local count = 0
	while #free_heads > 0 and count < 16 do
		play_next_head(note, velocity)
		count = count + 1
	end
end)

MIDI.receive_note_off = filter_channel(function(note, velocity)
	local done_heads = {}
	done_heads, busy_heads = removed_matching(busy_heads, function (h) 
		return h.note == note 
	end)
	for_each(done_heads, function(head) pause(head) end)
end)

MIDI.receive_cc = filter_channel(function (byte1, byte2)
	if byte1 == 1 then
		for_each(busy_heads, function(head)
			head.speed = head.base_speed + (.5 - (127.0 - byte2) / 127.0)
		end)
	end
end)

-- Audio events ---------------------------------------------------------------s
-------------------------------------------------------------------------------

Audio.head_was_added = function(head)
	table.insert(busy_heads, head)
	v0:add_head{head=head}
end

Audio.head_was_removed = function(head)
	table.insert(free_heads, head)
	v0:remove_head{head=head}
end
