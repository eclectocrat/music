//
// Copyright (c) 2017 Jeremy Jurksztowicz
//
//     Permission is hereby granted, free of charge, to any person obtaining a
//     copy of this software and associated documentation files (the
//     "Software"), to deal in the Software without restriction, including
//     without limitation the rights to use, copy, modify, merge, publish,
//     distribute, sublicense, and/or sell copies of the Software, and to permit
//     persons to whom the Software is furnished to do so, subject to the
//     following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef audio_h
#define audio_h

#include <RtAudio.h>
#include <memory>
#include <vector>
#include <readerwriterqueue/readerwriterqueue.h>
#include "expects.h"
#include "sample.h"
#include "sample_function.h"
#include "sample_head_processor.h"

namespace ec {

class Wave_head;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Wave_audio_driver: public Sample_head_processor {
public:
    void init();
    void deinit();
    int process(void *output_bufferv, void *input_bufferv,
                unsigned int frame_count, double stream_time,
                RtAudioStreamStatus);
    
    size_t channel_count() const { return _channel_count; }
    size_t sample_rate  () const { return _sample_rate; }
    size_t buffer_frames() const { return _buffer_frames; }
    
private:
    const size_t                    _channel_count = 2;
    const size_t                    _sample_rate = 44100;
    unsigned int                    _buffer_frames = 256;
    std::shared_ptr<RtAudio>        _driver;
    std::vector<Wave_head*>         _wave_heads;
    std::array<Sample_buffer, 2>    _head_bufs;
};

} // END namespace ec
#endif /* audio_h */
