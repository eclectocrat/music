//
// Copyright (c) 2017 Jeremy Jurksztowicz
//
//     Permission is hereby granted, free of charge, to any person obtaining a
//     copy of this software and associated documentation files (the
//     "Software"), to deal in the Software without restriction, including
//     without limitation the rights to use, copy, modify, merge, publish,
//     distribute, sublicense, and/or sell copies of the Software, and to permit
//     persons to whom the Software is furnished to do so, subject to the
//     following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef audio_stream_h
#define audio_stream_h

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sndfile.h>
#include <string>
#include "expects.h"
#include "exceptions.h"
#include "range.h"
#include "span.h"

#include "test.h"
#include <iostream>

namespace ec {
namespace audio {
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
class File_stream {
public:
    EC_ERROR(open_fail, desc_error);
    EC_ERROR(seek_fail, desc_error);
    EC_ERROR(read_fail, error_base);

    File_stream(std::string const &file)
        : _file_path(file), _snd_file(nullptr) {}

    ~File_stream() {
        if (_snd_file) {
            const int err = sf_close(_snd_file);
            if (err) {
                std::cerr << "ERROR[audio_stream] closing audio_stream: "
                          << sf_error_number(err) << std::endl;
            }
        }
    }

    File_stream(File_stream &&other)
        : _file_path(std::move(other._file_path)), _snd_info(other._snd_info),
          _snd_file(other._snd_file) {
        other._file_path = "";
        other._snd_file = nullptr;
        std::memset(&other._snd_info, 0, sizeof(SF_INFO));
    }
    
    std::string file_path() const {
        return _file_path;
    }

    bool is_open() const {
        return _snd_file != nullptr;
    }

    void open() {
        expects(not _snd_file);
        std::memset(&_snd_info, 0, sizeof(SF_INFO));
        _snd_file = sf_open(_file_path.c_str(), SFM_READ, &_snd_info);
        if (not _snd_file) {
            EC_ERR << "ERROR[audio_stream] streaming audio_stream "
                   << _file_path << ": " << sf_strerror(_snd_file)
                   << EC_THROW(open_fail);
        }
    }

    void seek(double pos) {
        seek(size_t(pos * size()));
    }

    void seek(size_t offset) {
        expects(_snd_file);
        expects(offset < frame_count());
        if (sf_seek(_snd_file, offset, SEEK_SET) < 0) {
            EC_ERR << "[audio_file_stream] ERROR seeking to " << offset << " : "
                   << sf_strerror(_snd_file) << EC_THROW(seek_fail);
        }
    }

    typedef Span<float> Sample_span;

    File_stream& operator >> (Sample_span& samples) {
        const auto read_amt = read(samples);
        if(read_amt != samples.size()) {
            throw read_fail();
        }
        return *this;
    }

    size_t read(Sample_span buffer) {
        expects(_snd_file);
        expects(buffer.size() > 0);
        expects(buffer.size() % channel_count() == 0);
        return sf_readf_float(_snd_file, buffer.data(),
                              buffer.size() / channel_count());
    }

    Sample_span contiguous_buffer(Range<size_t> rng) {
        return Sample_span();
    }

    size_t size() const { return _snd_info.frames * channel_count(); }
    size_t frame_count() const { return _snd_info.frames; }
    size_t sample_rate() const { return _snd_info.samplerate; }
    size_t channel_count() const { return _snd_info.channels; }

private:
    File_stream(const File_stream &);
    File_stream operator=(File_stream &);

    std::string _file_path;
    SF_INFO _snd_info;
    SNDFILE *_snd_file;
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

namespace test {
using namespace std;
TEST_CASE("[audio_stream]") {
}
} // END namespace test
} // END namespace audio
} // END namespace ec

#endif /* audio_stream_h */
