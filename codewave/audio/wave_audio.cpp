//
//  audio.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/23/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "wave_audio.h"
#include "constexpr_algo.h"
#include "sample_interface.h"
#include <iostream>

using namespace ec;
using namespace std;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int forward_process(void *output_bufferv, void *input_bufferv,
                    unsigned int frame_count, double stream_time,
                    RtAudioStreamStatus status, void *param) {
    expects(param != nullptr);
    auto _this = reinterpret_cast<Wave_audio_driver *>(param);
    return _this->process(output_bufferv, input_bufferv, frame_count,
                          stream_time, status);
}

void Wave_audio_driver::init() {
    _driver = make_shared<RtAudio>();
    if (_driver->getDeviceCount() < 1) {
        cerr << "ERROR: No audio devices found!" << endl;
    } else {
        cout << "System audio devices:\n";
        for(unsigned int i=0; i<_driver->getDeviceCount(); ++i) {
            const auto& info = _driver->getDeviceInfo(i);
            cout << "[" << i
                 << (i == _driver->getDefaultOutputDevice() ? "*" : " ") << "] "
                 << info.name << endl;
        }
        RtAudio::StreamParameters parameters;
        parameters.deviceId = _driver->getDefaultOutputDevice();
        parameters.nChannels = static_cast<unsigned int>(_channel_count);
        parameters.firstChannel = 0;
        RtAudio::StreamOptions options;
        options.flags = RTAUDIO_NONINTERLEAVED;
        _driver->openStream(&parameters, nullptr, RTAUDIO_FLOAT32,
                            int(_sample_rate), &_buffer_frames,
                            &forward_process, reinterpret_cast<void *>(this),
                            &options);
        prepare_resources(_buffer_frames);
        _driver->startStream();
        cout << "Began stereo audio output with " << _buffer_frames
             << " 32 bit float samples per chunk @ " << _sample_rate << " Hz."
             << endl;
    }
}

void Wave_audio_driver::deinit() {
    _driver->stopStream();
    _driver->closeStream();
    _driver.reset();
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int Wave_audio_driver::process(void *output_bufferv, void *input_bufferv,
                               unsigned int frame_count, double stream_time,
                               RtAudioStreamStatus status) {
    using namespace std;
    
    memset(output_bufferv, 0, frame_count*sizeof(float)*channel_count());
    auto buf = reinterpret_cast<float *>(output_bufferv);
    array<Sample_span, 2> out_bufs = {
        Sample_span(buf, buf + frame_count),
        Sample_span(buf + frame_count, buf + frame_count + frame_count)
    };
    Sample_head_processor::process({out_bufs[0], out_bufs[1]}, stream_time);
    return 0;
}
