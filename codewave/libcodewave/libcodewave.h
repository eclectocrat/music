//
//  libcodewave.h
//  libcodewave
//
//  Created by Jeremy Jurksztowicz on 7/19/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef libcodewave_
#define libcodewave_
#pragma GCC visibility push(default)

// C API
#include "RangeSet.h"
#include "Wave.h"
#include "WaveHead.h"

// C++ API
#ifdef __cplusplus
#include "sample_channels.h"
#include "sample_function.h"
#include "sample_head_processor.h"
#include "sample_head.h"
#include "sample.h"
#endif // __cplusplus

#pragma GCC visibility pop
#endif
