//
//  sample_channels.h
//  codewave
//
//  Created by Jeremy Jurksztowicz on 6/8/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef sample_channels_h
#define sample_channels_h

#include <array>
#include "range.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
template<typename T, size_t MaxChannels = 2>
class Channels {
public:
    static const size_t max_channels = MaxChannels;

private:
    using Storage = std::array<T, max_channels>;
    Storage _channels;
    size_t _size = 0; // 1...max_channels
    
public:
    Channels(T && c) {
        _size = 1;
        _channels[0] = std::move(c);
    }
    Channels(T const& c) {
        _size = 1;
        _channels[0] = c;
    }
    Channels(T && c1, T && c2) {
        _size = 2;
        _channels[0] = std::move(c1);
        _channels[1] = std::move(c2);
    }
    Channels(T const& c1, T const& c2) {
        _size = 2;
        _channels[0] = c1;
        _channels[1] = c2;
    }
    Channels(Channels const&) = default;
    Channels& operator=(Channels const&) = default;
    
    size_t size()const { return _size; }
    size_t frame_count()const { return _channels[0].size(); }
    
    T&       operator[](size_t n)       { return _channels[n]; }
    T const& operator[](size_t n)const  { return _channels[n]; }
    
    using iterator = typename Storage::iterator;
    using const_iterator = typename Storage::const_iterator;
    
    iterator        begin   ()         { return _channels.begin(); }
    const_iterator  begin   () const   { return _channels.begin(); }
    iterator        end     ()         { return _channels.begin()+_size; }
    const_iterator  end     () const   { return _channels.begin()+_size; }
    
    template<typename F>
    void per_channel(F f) {
        for(size_t c=0; c<_size; ++c) {
            f(c, _channels[c]);
        }
    }
    
    Channels<T> slice(Range<size_t> r) {
        auto copy = *this;
        copy.per_channel([=](size_t c, auto& t) {
            t = t.subspan(r.begin(), r.size());
        });
        return copy;
    }
};

} // namespace
#endif /* sample_channels_h */
