//
//  sample_head.hpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/21/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef sample_head_h
#define sample_head_h

#include "sample.h"
#include "sample_channels.h"
#include <atomic>

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Sample_head {
public:
    enum class Direction { forward, backward };

    // setup/teardown
    void attach(std::shared_ptr<Sample> const&, Sample_range, double speed,
                size_t pos);
    std::shared_ptr<Sample> detach();
    std::shared_ptr<Sample> const &attached() { return _sample; }
    std::shared_ptr<const Sample> attached() const { return _sample; }

    // accessors/properties
    size_t      pos         () const        { return _pos.load(); }
    void        set_pos     (size_t p)      { _pos = p; }
    Direction   dir         () const        { return _dir.load(); }
    void        set_dir     (Direction d)   { _dir = d;}
    float       amp         () const        { return _amp_mod.load(); }
    void        set_amp     (float f)       { _amp_mod = f; }
    int         loop        () const        { return _loop.load(); }
    void        set_loop    (int n)         { _loop = n; }
    double      speed       () const        { return _speed.load(); }
    /// @note   has soft limits, so expect h.set_speeed(x); x != h.speed();
    ///         when you are sending in values ~< 0.1 and ~> 10.0.
    void        set_speed   (double);
    size_t  range_begin     () const        { return _range_begin; }
    size_t  range_end       () const        { return _range_end; }
    void    set_range_begin (size_t b)      { _range_begin = b; }
    void    set_range_end   (size_t e)      { _range_end = e; }
    size_t  range_size      () const        { return _range_end - _range_begin; }
    
    // operations
    std::pair<Sample_range, size_t> read(size_t, Sample_channels);

private:
    std::shared_ptr<Sample> _sample;

    std::atomic<Direction>  _dir        = Direction::forward;
    std::atomic<int>        _loop       = 0;
    
    // Sample_range         _range;
    // std::atomic<uint64_t>   _packed_range = 0; // TODO
    
    std::atomic<size_t>     _range_begin= 0;
    std::atomic<size_t>     _range_end  = 0;
    
    std::array<Sample_range, 2> _ranges;
    std::atomic<int> _range_index; // swap between ^
    
    std::atomic<size_t>     _pos        = 0;
    std::atomic<double>     _speed      = 1.0;
    std::atomic<float>      _amp_mod    = 1.0;
    std::atomic<size_t>     _fade_dur   = 512;
    
    std::array<Sample_buffer, 2> _swap_buffers;
    Sample_channels make_chans(Sample*, size_t);
    
    static const size_t max_frames_per_slice = 512;
};

} // namespace
#endif /* sample_head_h */
