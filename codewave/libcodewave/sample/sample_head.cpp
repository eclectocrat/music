//
//  sample_head.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/21/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "sample_head.h"
#include "sample_function.h"
#include "sample_channels.h"
#include "n_ranges_for.h"
#include <algorithm>
#include <cmath>

using namespace ec;
using namespace std;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// Fades a buffer if we are near the extents of a range.
///
/// @param forward      True if the logical direction is from begin to end,
///                     false if it is from end to begin.
/// @param range        The range that gives the extents for begin and end.
///
/// @param fade_dur     The number of frames the fade is to be applied over.
/// @param buffers      Contains the memory to possible apply a fade over.
///
void fade_if_near_ends(bool forward, Range<size_t> range, size_t pos,
                       size_t end_pos, size_t fade_dur,
                       Sample_channels buffers) {

    // fade out end
    const auto frames_from_end =
        forward ? end(range) - end_pos : end_pos - begin(range);
    if (frames_from_end < fade_dur) {
        const auto f = Discrete_function(
            1.0, 0.0, split_range(range, range.size() - fade_dur).second);
        buffers.per_channel([&](auto, auto &buf) {
            f(end(range) - frames_from_end,
              begin(buf) + max(int(frames_from_end) - int(pos), 0),
              end(buf));
        });
    }
    // fade in begin
    const auto frames_from_begin =
        forward ? end_pos - begin(range) : end(range) - end_pos;
    if (frames_from_begin < fade_dur) {
        const auto f =
            Discrete_function(0.0, 1.0, split_range(range, fade_dur).first);
        buffers.per_channel([&](auto, auto &buf) {
            f(begin(range) + frames_from_begin, begin(buf),
              begin(buf) + min(int(fade_dur) - int(frames_from_begin),
                               int(buffers.frame_count())));
        });
    }
}

// time_transform :: speed -> amp -> inp -> (outp, out_frames)
size_t time_transform(float speed, float amp, Sample_channels inp,
                      Sample_channels outp) {
    // TODO: select interpolation type.
    N_ranges<size_t> gen(inp[0].size(),
                         max(size_t(1), min(size_t(inp[0].size() / speed),
                                            size_t(outp[0].size()))));
    if (inp.size() > 1) {
        // Stereo path
        for_all_n_ranges(gen, [&](auto i, auto r) {
            outp.per_channel([&](auto c, auto&) {
                outp[c][i] = inp[c][r.begin()-(1-r.size())] * amp;
            });
        });
    } else {
        // Mono path
        for_all_n_ranges(gen, [&](auto i, auto r) {
            if (r.size() < 2) {
                outp[0][i] = inp[0][r.begin() - (1 - r.size())] * amp;
            } else {
                outp[0][i] = accumulate(begin(inp[0]) + r.begin(),
                                        begin(inp[0]) + r.end(), 0.0) /
                             float(r.size()) * amp;
            }
        });
    }
    return gen.ranges_size();
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void Sample_head::attach(std::shared_ptr<Sample> const &s, Sample_range r,
                         double sp, size_t p) {
    expects(s != nullptr);
    expects(sp > .0);
    _range_begin = r.begin();
    _range_end = r.end();
    _pos = p / sp;
    _speed = sp;
    expects(_pos <= _range_end - _range_begin);
    expects(s->channel_count() <= 2);
    
    // TODO: options for playing with this. Can save a lot of memory keeping
    // this small, especially if speed is always >= 1.0.
    if (_swap_buffers[0].size() < 4096) {
        _swap_buffers[0] = Sample_buffer(4096);
        _swap_buffers[1] = Sample_buffer(4096);
    }
    
    // this is used as a flag, so should be set last to mark completion.
    _sample = s;
}

std::shared_ptr<Sample> Sample_head::detach() {
    auto temp = _sample;
    _sample = nullptr;
    return temp;
}

void Sample_head::set_speed(double d) {
    // _swap_buffers won't support speeds lower than this
    d = max(d, 1.0 / (double(_swap_buffers[0].size()) / max_frames_per_slice) +
                   __DBL_EPSILON__ * 2.0);
    _speed = d;
}

Sample_channels Sample_head::make_chans(Sample* sample, size_t frames_read) {
    expects(frames_read > 0);
    expects(_swap_buffers[0].size() >= frames_read);
    Sample_span left(begin(_swap_buffers[0]),
                     begin(_swap_buffers[0]) + frames_read);
    if (sample->channel_count() > 1) {
        Sample_span right(begin(_swap_buffers[1]),
                          begin(_swap_buffers[1]) + frames_read);
        return Sample_channels(left, right);
    } else {
        return Sample_channels(left);
    }
}

pair<Sample_range, size_t> Sample_head::read(size_t frames_req,
                                             Sample_channels buffers) {
    auto sample = _sample.get();
    expects(sample->channel_count() <= buffers.size());
    expects(frames_req > 0);
    expects(frames_req <= buffers[0].size());
    expects(buffers.frame_count() <= max_frames_per_slice);

    const auto speed        = _speed.load();
    const auto dir          = _dir.load();
    const auto amp          = _amp_mod.load();
    const auto fade_dur     = _fade_dur.load();
    const auto range_begin  = _range_begin.load();
    const auto range_end_   = _range_end.load();
    const auto range_end    = range_end_ > range_begin ? range_end_ : range_begin;
    
    int frames_read = ceil(frames_req * speed);
    size_t pos = 0;
    int jump = 0;
    do {
        pos = _pos.load();
        jump = dir == Direction::forward
                        ? min(frames_read, int(range_end - pos))
                        : -(min(frames_read, int(pos - range_begin)));
    } while (not _pos.compare_exchange_strong(pos, pos + jump));
    frames_read = abs(jump);
    if (frames_read < 1) {
        return {Sample_range(pos, pos), 0};
    }
    const auto end_pos =
        pos + (dir == Direction::forward ? frames_read : -int(frames_read));
    expects(pos <= range_end);
    expects(end_pos <= range_end);
    expects(end_pos >= range_begin);
    
    auto bufs = make_chans(sample, frames_read);
    auto range = Sample_range(pos, pos + frames_read);
    bufs.per_channel([&](auto c, auto& buf) {
        sample->copy_samples(range, c, buf);
    });
    
    if (dir == Direction::backward) {
        bufs.per_channel([](size_t c, auto& buf) {
            reverse(begin(buf), end(buf));
        });
    }

    if (abs(speed - 1.0) < __DBL_EPSILON__) { // approx-equal
        bufs.per_channel([&](auto c, auto& buf) {
            transform(begin(buf), end(buf), begin(buffers[c]),
                  [=](float f) { return f * amp; });
        });
    } else {
        frames_read = int(time_transform(speed, amp, bufs, buffers));
    }

    fade_if_near_ends(dir == Direction::forward, {range_begin, range_end},
                      pos, end_pos, fade_dur, buffers);
    return {{pos, pos + frames_read}, frames_read};
}
