//
//  sample.h
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/17/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef sample_h
#define sample_h

#include "expects.h"
#include "mmap_allocator.h"
#include "range.h"
#include "buffer.h"
#include "span.h"
#include "for_chunks.h"
#include "audio_file_stream.h"
#include "sample_channels.h"

#include <list>

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
template<typename T>
struct AllocBase {
    virtual T *alloc(const size_t) = 0;
    virtual void dealloc(T*, const size_t) = 0;
};

template<typename T>
struct Malloc: AllocBase<T>, private Allocator<T> {
    T *alloc(const size_t n) override final {
        return ec::Allocator<T>::alloc(n);
    }
    void dealloc(T* p, const size_t n) override final {
        ec::Allocator<T>::dealloc(p, n);
    }
};

template<typename T>
struct Mmap_alloc: AllocBase<T>, private Mmap_allocator<T> {
    using Mmap_allocator<T>::Mmap_allocator;
    using Mmap_allocator<T>::buff_size;
    T *alloc(const size_t n) override final {
        return ec::Mmap_allocator<T>::alloc(n);
    }
    void dealloc(T* p, const size_t n) override final {
        ec::Mmap_allocator<T>::dealloc(p, n);
    }
};
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
using Sample_range = Range<size_t>;
using Sample_buffer = Buffer<float, AllocBase>;
using Sample_span = Span<float>;
using Sample_channels = Channels<Sample_span, 2>;

struct Sample {
    // A Sample object is a linked list of clips of indepenent lengths:
    // [--Clip0--][-----Clip1-----][-Clip2-][---Clip3---]
    // Each clip can be split at a point to create two clips. Clips can be
    // cheaply moved into and out of Samples, see std::list::splice().
    class Clip {
    public:
        friend class Wave;
        using File_stream = audio::File_stream;
        using Buffer_list = std::vector<Sample_buffer>;
    
        Clip() = default;
        Clip(Clip const&) = default;
        Clip& operator=(Clip const&) = default;
        Clip(Clip&&) = default;
        Clip& operator=(Clip&&) = default;
        Clip(const size_t f): _frame_offset(f) {}
        Clip(const size_t f, std::shared_ptr<File_stream> const &fs,
             Sample_range const &rng);

        // accessors/properties
        size_t              channel_count   () const;
        size_t              frame_count     () const;
        size_t              offset          () const    { return _frame_offset; }
        size_t              next_offset     () const    { return offset() + frame_count(); }
        void                set_offset      (size_t n)  { _frame_offset = n; }
        void                move_offset     (int n)     { _frame_offset += n; }
        Sample_range        range           () const    { return Sample_range(offset(), next_offset()); }
        Sample_range const& stream_range    () const    { return _stream_range; }
        Buffer_list&        samples         ()          { return _samples; }
        Buffer_list const&  samples         () const    { return _samples; }
        Buffer_list&        rms             ()          { return _rms; }
        Buffer_list const&  rms             () const    { return _rms; }
        
        // actions
        bool open_stream(std::string const &,
                         Sample_range const & = Sample_range());
        void close_stream();
        bool is_stream_open() const { return _file_stream != nullptr; }
        void render_stream_rms(const size_t length);
        void render_mapped_rms(const size_t length);
        void map_stream_frames(Sample_range);
        void unmap_frames();
        Clip split_back(size_t);
        
        // predicates
        
        /// @return true if the passed clip can be joined to the back of this
        ///         clip. i.e. this->end() == other->begin();
        bool can_join_back(Clip const&);
        
    private:
        // source of data; nullable
        std::shared_ptr<File_stream> _file_stream;
        // the range of the stream; may be empty iff file_stream==nullptr
        Sample_range _stream_range;
        Buffer_list _samples;
        Buffer_list _rms;
        size_t _frame_offset = 0;
    };
    
// ; types
    using Clip_list = std::list<Clip>;
    using Clip_iter = Clip_list::iterator;
    using Clip_const_iter = Clip_list::const_iterator;
    
// ; make/destroy
    Sample() = default;
    Sample(Sample const &) = default;
    Sample &operator=(Sample const &) = default;
    Sample(Sample &&) = default;
    Sample &operator=(Sample &&) = default;
    
// ; get data from somewhere
    void open_stream(std::string const&);
    
// ; put data somewhere
    void copy_samples(Sample_range, size_t, Sample_span);
    
// ; properties
    size_t frame_count() const;
    size_t channel_count() const;
    bool empty() const;
    
    Clip_list& clips() { return _clips; }
    Clip_list const& clips() const { return _clips; }
    
// ; mutating interface
    void clear();
    void erase(Sample_range);
    void take(Sample_range, Sample& from, size_t to);
    void copy(Sample_range, Sample& from, size_t to);
    void insert_samples(size_t pos, size_t amt, float=.0f);

    /// for_range :: Range -> (off :: Clip -> Pos) -> (buf :: Clip -> [Sample]))
    ///                    -> (f :: Sample -> Void) -> Void
    ///
    /// Visits a range of data, possibly spread over multiple contiguous clips,
    /// one element at a time
    /// @param range The range of data to visit.
    /// @param offset_f The function to retrieve the offset of a clip.
    /// @param buffer_f The function to retreive the buffer of a clip - either wave, rms, etc.
    /// @param func The visitor function to apply to each sample. (Sample&) -> void;
    template <typename F, typename Offset, typename Buffer>
    void for_range(Sample_range const &range, Offset offset_f, Buffer buffer_f,
                  F func);
    
    template <typename F>
    void for_samples(Sample_range const &, const size_t, F);
    
    /// Splits the wave at the given frame if not already split there.
    /// @param n The frame at which to split.
    /// @return A pair of iterators pointing to the left and right clips
    ///         respectively.
    std::pair<Clip_iter, Clip_iter> split_if_needed(size_t n);
    
protected:
    template<typename Offset>
    Clip_iter find_clip(const size_t n, Offset offset_f);
    
    Clip_list _clips;
};

using Sample_clip = Sample::Clip;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
template <typename F, typename Offset, typename Buffer>
void Sample::for_range(Sample_range const &range, Offset offset_f,
                       Buffer buffer_f, F func) {
    for_chunks(_clips, range, offset_f, buffer_f, [&](auto b, auto e) {
        std::for_each(b, e, func);
    });
}

template <typename F>
void Sample::for_samples(Sample_range const &range, const size_t chan, F func) {
    for_range(range, [&](auto c) { return c.frame_count(); },
              [&](auto c) { return c.samples[chan]; }, func);
}

template <typename Offset>
Sample::Clip_iter Sample::find_clip(const size_t n, Offset offset_f) {
    return lower_bound(std::begin(_clips), std::end(_clips), Clip(n),
                       [&](auto const &a, auto const &b) {
                           return offset_f(a) < offset_f(b);
                       });
}

} // END namespace
#endif /* sample_h */
