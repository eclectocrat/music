//
//  sample_head_processor.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 6/5/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "sample_head_processor.h"
#include "constexpr_algo.h"
#include "sample_interface.h"
#include <range/v3/all.hpp>
#include <iostream>

using namespace ec;
using namespace std;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
Sample_head_processor::Sample_head_processor():
    _from_processor(1024), _to_processor(1024) {
    _fade_outs.reserve(512);
    _fade_ins.reserve(512);
}

void Sample_head_processor::send_message(
    Sample_head_processor::Message const &msg) {
    if (not _to_processor.try_enqueue(msg)) {
        throw runtime_error("to audio queue overflow!");
    }
}

bool Sample_head_processor::try_recieve_message(
    Sample_head_processor::Message &msg) {
    return _from_processor.try_dequeue(msg);
}

void Sample_head_processor::try_send_from(
    Sample_head_processor::Message const &msg) {
    if (not _from_processor.try_enqueue(msg)) {
        throw runtime_error("from audio queue overflow!");
    }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void Sample_head_processor::prepare_resources(size_t slice_max_frames) {
    _head_bufs = {Sample_buffer(slice_max_frames),
                  Sample_buffer(slice_max_frames)};
}

Sample_range Sample_head_processor::process_head(
    shared_ptr<Sample_head>     head,
    shared_ptr<Sample>          sample,
    Sample_head_processor::Fade * fade,
    size_t                      frame_count,
    Sample_channels             out_bufs,
    Sample_channels             head_bufs) {
    
    expects(head != nullptr);
    expects(sample != nullptr);
    expects(frame_count > 0);
    
    // read head
    const auto [rng, frames_read] = head->read(frame_count, head_bufs);
    expects(frames_read <= _head_bufs[0].size());
    if (frames_read < frame_count) {
        const auto loop = head->loop();
        if (loop > 0) {
            // lots of atomic reads here, if you do some extra crazy things
            // like rapidly toggle the direction you might get unexpected fun.
            head->set_pos(head->dir() == Sample_head::Direction::backward
                              ? head->range_end()
                              : head->range_begin());
            auto subspans = head_bufs.slice({frames_read, frame_count});
            const auto[rng2, frames_read2] =
                head->read(subspans[0].size(), subspans);
            expects(frames_read2 + frames_read == frame_count);
            return rng2;
        }
    }
    // fade if applicable
    if (fade) {
        fade->func(0, begin(head_bufs[0]), begin(head_bufs[0]) + frames_read);
        if (sample->channel_count() > 1) {
            fade->func(0, begin(head_bufs[1]),
                       begin(head_bufs[1]) + frames_read);
        }
    }
    // mix into output
    transform(begin(_head_bufs[0]), begin(_head_bufs[0]) + frames_read,
              begin(out_bufs[0]), begin(out_bufs[0]),
              [](auto l, auto r) { return l + r; });
    const auto source = sample->channel_count() == 2 ? 1 : 0;
    transform(begin(_head_bufs[source]),
              begin(_head_bufs[source]) + frames_read, begin(out_bufs[1]),
              begin(out_bufs[1]), [](auto l, auto r) { return l + r; });
    return rng;
}

void Sample_head_processor::process(Sample_channels out_buffers,
                                    double stream_time) {
    using namespace std;
    try_send_from({this, nullptr, Message::will_begin_frame_slice, stream_time});
    
    const size_t frame_count = out_buffers[0].size();
    expects(frame_count > 0);
    
//////////////////////////////
// Handle incoming messages //
//////////////////////////////
    Message msg;
    while (_to_processor.try_dequeue(msg)) {
        expects(msg.head != nullptr);
        switch (msg.meta) {
        case Message::add:
            if (find(begin(_heads), end(_heads), msg.head) ==
                end(_heads)) {
                if (_fade_ins.size() >= _fade_ins.capacity()) {
                    throw runtime_error("Insertion stack overflow");
                } else {
                    const auto fade_func = Discrete_function(
                        0.f, 1.f, Sample_range(0, frame_count));
                    _fade_ins.push_back({msg.head, fade_func});
                }
            }
            try_send_from(msg); // confirm
            break;
        case Message::remove:
            if (auto i = find(begin(_heads), end(_heads), msg.head);
                i != end(_heads)) {
                if (_fade_outs.size() >= _fade_outs.capacity()) {
                    throw runtime_error("Removal stack overflow");
                } else {
                    const auto fade_func = Discrete_function(
                        1.f, 0.f, Sample_range(0, frame_count));
                    _fade_outs.push_back({*i, fade_func});
                }
                _heads.erase(i);
            }
            break;
        default: break;
        }
    }
    
////////////////////////
// Process wave heads //
////////////////////////
    
    // Sort heads pointing to the same samples by their position in the sample.
    // This should help with page thrashing or more likely cache coherency by
    // reading all the heads that are in the same range before moving on to
    // other part of memory, rather than jumping back and forth randomly.
    //
    // TODO: I've found this helped in wavelet-type usage of heads (hundreds or
    // thousands of heads over a single sample), but this is a pessimization in
    // many cases. Not sure how to expose this configuration.
    ranges::sort(_heads, [](auto a, auto b) {
        return a->attached() != b->attached() ? a->attached() < b->attached()
                                              : a->pos() < b->pos();
    });

    expects(_head_bufs[0].size() >= frame_count);
    for_each(begin(_heads), end(_heads), [&](auto &h) {
        if (auto sample = h->attached()) {
            const auto r =
                process_head(h, sample, nullptr, frame_count, out_buffers,
                             head_chans());
            try_send_from(
                {this, h, Message::played, stream_time, r.begin(), r.end()});
        } // else...?
    });

//////////////////////////////////
// Process fading in wave heads //
//////////////////////////////////
    for_each(begin(_fade_ins), end(_fade_ins), [&](auto &fade) {
        if (auto sample = fade.head->attached()) {
            const auto r =
                process_head(fade.head, sample, &fade, frame_count, out_buffers,
                             head_chans());
            _heads.push_back(fade.head);
            try_send_from({this, fade.head, Message::played, stream_time,
                           r.begin(), r.end()});
        }
    });
    _fade_ins.clear();

///////////////////////////////////
// Process fading out wave heads //
///////////////////////////////////
    for_each(begin(_fade_outs), end(_fade_outs), [&](auto &fade) {
        if (auto sample = fade.head->attached()) {
            const auto r =
                process_head(fade.head, sample, &fade, frame_count, out_buffers,
                             head_chans());
            try_send_from({this, fade.head, Message::played, stream_time,
                           r.begin(), r.end()});
            try_send_from({this, fade.head, Message::remove});
        }
    });
    _fade_outs.clear();
    
    try_send_from({this, nullptr, Message::did_end_frame_slice, stream_time});
}
