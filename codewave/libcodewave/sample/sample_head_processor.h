//
//  sample_head_processor.hpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 6/5/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef sample_head_processor_h
#define sample_head_processor_h

#include <memory>
#include <vector>
#include <readerwriterqueue/readerwriterqueue.h>
#include "expects.h"
#include "sample.h"
#include "sample_head.h"
#include "sample_function.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Sample_head_processor {
public:
    Sample_head_processor();
    void process(Sample_channels outs, double stream_time);
    
    struct Message {
        Sample_head_processor * processor = nullptr;
        std::shared_ptr<Sample_head> head;
        enum Meta {
            // Head events
            add, remove,
            played,                 // begin, end == range of frames played.
            
            // Stream events
            will_begin_frame_slice, // time == stream time
            did_end_frame_slice     // time == stream_time
        };
        Meta    meta    = add;
        double  time    = .0;
        size_t  begin   = 0;
        size_t  end     = 0;
    };
    
    void send_message(Message const&);
    bool try_recieve_message(Message&);
    
    // call before processing begins:
    void prepare_resources(size_t slice_max_frames);
    
private:
    using Message_queue = moodycamel::ReaderWriterQueue<Message>;
    Message_queue _to_processor, _from_processor;

    void try_send_from(Message const&);

    struct Fade {
        std::shared_ptr<Sample_head> head;
        const Discrete_function func;
    };
    std::vector<Fade> _fade_outs;
    std::vector<Fade> _fade_ins;

    Sample_range process_head(
        std::shared_ptr<Sample_head>,
        std::shared_ptr<Sample>,
        Fade *,
        size_t frame_count,
        Sample_channels outp,
        Sample_channels bufs
    );
    
    using Head_vec = std::vector<std::shared_ptr<Sample_head>>;

    Head_vec _heads;
    std::array<Sample_buffer, 2> _head_bufs;
    inline Sample_channels head_chans() {
        return {{begin(_head_bufs[0]), end(_head_bufs[0])},
                {begin(_head_bufs[1]), end(_head_bufs[1])}};
    }
};

} // END namespace ec

#endif /* sample_head_processor_h */
