//
//  sample.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 5/17/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include <Accelerate/Accelerate.h>
#include "sample.h"
#include "n_ranges_for.h"
#include "constexpr_algo.h"

using namespace std;
using namespace ec;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
size_t Sample::Clip::frame_count() const {
    if (not _samples.empty()) {
        return _samples.front().size();
    } else if(_file_stream) {
        return _stream_range.size();
    } else {
        return 0;
    }
}

size_t Sample::Clip::channel_count() const {
    if (not _samples.empty()) {
        return _samples.size();
    } else if(_file_stream) {
        return _file_stream->channel_count();
    } else {
        return 1;
    }
}

void Sample::Clip::render_stream_rms(const size_t length) {
    expects(length > 0);
    expects(_file_stream != nullptr);
    
    // reset all
    const auto chan_count = channel_count();
    expects(chan_count > 0);
    _rms.clear();
    _rms.resize(chan_count);
    for_each(begin(_rms), end(_rms),
             [&](auto &buf) { buf = Sample_buffer(length); });
    _file_stream->seek(_stream_range.begin());
    
    N_ranges<size_t> rgen(_stream_range.size(), length);
    Sample_buffer str_buf(rgen.range_max_size() * chan_count);
    for_all_n_ranges(rgen, [&](auto i, auto range) {
        // read exact peak chunk size
        const auto samples_size = range.size() * chan_count;
        expects(samples_size <= str_buf.size());
        const size_t read_frames = _file_stream->read(
            {begin(str_buf), begin(str_buf) + samples_size});
        expects(read_frames * chan_count <= str_buf.size());
        // calc rms per channel
        float rms_value = 0;
        for (int c = 0; c < chan_count; ++c) {
            vDSP_rmsqv(str_buf.ptr() + c, chan_count, &rms_value, read_frames);
            _rms[c][i] = rms_value;
        }
    });
}

void Sample::Clip::render_mapped_rms(const size_t length) {
    expects(length > 0);
    expects(not _samples.empty());

    // reset
    const auto chan_count = _samples.size();
    _rms.clear();
    _rms.resize(chan_count);
    for_each(begin(_rms), end(_rms),
             [&](auto &buf) { buf = Sample_buffer(length); });

    // calc rms per channel
    N_ranges<size_t> rgen(_samples[0].size(), length);
    for_all_n_ranges(
        rgen, [&](auto i, auto range) {
            float rms_value = 0;
            for (int c = 0; c < chan_count; ++c) {
                vDSP_rmsqv(_samples[c].ptr() + range.begin(), 1, &rms_value,
                           range.size());
                _rms[c][i] = rms_value;
            }
        });
}

bool Sample::Clip::open_stream(string const &file_path, Sample_range const &rng) {
    _samples.clear();
    _file_stream = make_shared<audio::File_stream>(file_path);
    _file_stream->open();
    _stream_range = rng.empty() ? Sample_range(_file_stream->frame_count()) : rng;
    expects(rng.size() <= _file_stream->frame_count());
    return _file_stream->is_open();
}

void Sample::Clip::close_stream() {
    _file_stream.reset();
    _stream_range = Sample_range();
}

void Sample::Clip::map_stream_frames(Sample_range rng) {
    expects(_file_stream != nullptr);
    expects(not rng.empty());
    expects(rng.end() <= _file_stream->frame_count());
    _samples.clear();
    
    // create memory for frames
    const size_t chans = _file_stream->channel_count();
    auto map_buffer_alloc = make_shared<Mmap_alloc<float>>(rng.size(), chans);
    for (size_t i = 0; i < chans; ++i) {
        const auto buf = Sample_buffer(rng.size(), map_buffer_alloc);
        _samples.push_back(buf);
    }
   
    // copy from stream -> memory
    const size_t total_frames = _file_stream->frame_count();
    const size_t chunk_frames = min(4096ul * 2ul, total_frames);
    const size_t num_chunks = total_frames / (total_frames / chunk_frames + 1);
    auto buf = Sample_buffer(chunk_frames * chans);
    
    const auto n_ranges = N_ranges<size_t>(total_frames, num_chunks);
    for_all_n_ranges(n_ranges, [&](auto, auto range) {
        // data for this range
        const size_t samples_req = range.size() * chans;
        expects(samples_req <= buf.size());
        const auto span = Sample_span(begin(buf), begin(buf) + samples_req);
        const size_t frames_read = _file_stream->read(span);
        const size_t samples_read = frames_read * chans;
        expects(frames_read > 0);
        expects(frames_read + range.begin() <= _samples[0].size());
        
        // copy to output
        if (chans == 1) {
            std::copy(begin(buf), begin(buf) + samples_read,
                      begin(_samples[0]) + range.begin());
        } else {
            expects(chans == 2);
            expects(_samples.size() == 2);
            // deinterleave
            algo::copy_stride(begin(buf), begin(buf) + samples_read, 2,
                              begin(_samples[0]) + range.begin());
            algo::copy_stride(begin(buf) + 1, begin(buf) + samples_read,
                              2, begin(_samples[1]) + range.begin());
        }
    });
    _stream_range = rng;
}

void Sample::Clip::unmap_frames() { _samples.clear(); }

Sample::Clip Sample::Clip::split_back(size_t n) {
    expects(n > 0); // can't split at beginning or I'd destroy myself
    expects(n <= frame_count());
    if (n == frame_count()) {
        return Clip(); // split back of zero length
    }
    Clip ret;
    ret._frame_offset = _frame_offset + n;
    ret._file_stream = _file_stream;
    ret._stream_range = not _stream_range.empty()
                            ? _stream_range.slice(n, _stream_range.end())
                            : Sample_range();
    if (not _samples.empty()) {
        ret._samples.resize(_samples.size());
        for(size_t c=0; c<channel_count(); ++c) {
            ret._samples[c] = _samples[c].split_back(n);
        }
    }
    return ret;
}

bool Sample::Clip::can_join_back(Sample::Clip const &r) {
    if (not _samples.empty() and _samples.size() == r._samples.size()) {
        for (size_t c = 0; c != _samples.size(); ++c) {
            if (ec::end(_samples[c]) != ec::begin(r._samples[c])) {
                return false;
            }
        }
        return true;
    } else {
        return (_samples.empty() and r._samples.empty() and
                _file_stream == r._file_stream and
                _stream_range.end() == r._stream_range.begin());
    }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
size_t Sample::frame_count() const {
    return _clips.empty() ? 0 : _clips.back().next_offset();
}

size_t Sample::channel_count() const {
    return _clips.empty() ? 1 : _clips.front().channel_count();
}

bool Sample::empty() const {
    return _clips.empty() or frame_count() == 0;
}

void Sample::clear() {
    _clips.clear();
}

void Sample::erase(Sample_range range) {
    auto l = split_if_needed(range.begin());
    auto r = split_if_needed(range.end());
    _clips.erase(l.second, r.second);
    for_each(l.first, std::end(_clips), [=](auto& c) {
        c.move_offset(-int(range.size()));
    });
}

void Sample::take(Sample_range range, Sample& from, size_t to) {
    auto l = from.split_if_needed(range.begin());
    auto r = from.split_if_needed(range.end());
    auto i = split_if_needed(to);
    _clips.splice(i.second, from._clips, l.second, r.second);
    for_each(l.first, std::end(from._clips), [=](auto& c) {
        c.move_offset(-int(range.size()));
    });
}

void Sample::copy(Sample_range range, Sample& from, size_t to) {
    // TODO: Destructively splits. Alternatively create a new list:
    Sample copy = from;
    auto l = copy.split_if_needed(range.begin());
    auto r = copy.split_if_needed(range.end());
    auto i = split_if_needed(to);
    std::copy(l.second, r.second, inserter(_clips, i.second));
}

void Sample::copy_samples(Sample_range range, size_t chan, Sample_span to) {
    expects(to.size() >= range.size());
    expects(range.end() <= frame_count());
    size_t pos = 0;
    for_chunks(_clips, range,
               [](auto &c) {
                   return c.range();
               },
               [=](auto &c) -> auto & {
                   expects(c.channel_count() > chan);
                   expects(not c.samples()[chan].empty());
                   return c.samples()[chan];
               },
               [&](auto b, auto e) {
                   std::copy(b, e, begin(to) + pos);
                   pos += distance(b, e);
               });
}

void Sample::insert_samples(size_t pos, size_t amt, float v) {
    expects(pos <= frame_count());
    if (amt==0) {
        return;
    }
    auto l = split_if_needed(pos);
    auto r = _clips.insert(l.first, Clip());
    for(size_t c=0; c<channel_count(); ++c) {
        r->samples().push_back(Sample_buffer(amt));
        fill(ec::begin(r->samples()[c]), ec::end(r->samples()[c]), v);
    }
}

void Sample::open_stream(string const& file_path) {
    clear();
    _clips.insert(std::begin(_clips), Clip());
    _clips.front().open_stream(file_path);
}

pair<Sample::Clip_iter, Sample::Clip_iter> Sample::split_if_needed(size_t n) {
    expects(n <= frame_count());
    auto b = find_clip(n, [](auto &c) { return c.offset(); });
    if (b == std::end(_clips)) {
        return {std::end(_clips), std::end(_clips)};
    }
    auto& left = *b;
    if (n == left.next_offset()) {
        auto e = b;
        return {b, ++e};
    }
    auto e = b; // b guaranteed to be < end(_clips)
    const auto right = left.split_back(n - left.offset());
    return {b, _clips.insert(++e, right)};
}

namespace test {
using namespace std;
TEST_CASE("Sample::insert_samples") {
    Sample sample;
    SUBCASE("no-ops 0 samples") {
        sample.insert_samples(0, 0);
        CHECK(sample.clips().empty());
    } SUBCASE("throws out of bounds") {
        REQUIRE_THROWS(sample.insert_samples(1, 1));
    } SUBCASE("inserts expected amount of frames") {
        sample.insert_samples(0, 1);
        CHECK(sample.frame_count() == 1);
        const auto& clips = sample.clips();
        CHECK(clips.size() == 1);
        const auto& c = clips.front();
        CHECK(c.samples().size() == 1);
        CHECK(c.samples().front().size() == 1);
        CHECK(c.samples().front()[0] - .0 < FLT_EPSILON);
    } SUBCASE("inserts one clip") {
        sample.insert_samples(0, 1);
        CHECK(sample.clips().size() == 1);
    }
}
} // namespace test
