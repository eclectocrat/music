//
//  sample_function.h
//  codewave
//
//  Created by Jeremy Jurksztowicz on 6/1/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef sample_function_h
#define sample_function_h

#include "range.h"
#include <algorithm>
#include <cmath>

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
struct Function {
    Function(float a_, float b_): a(a_), b(b_) {}
    Function() = default;
    Function(Function const&) = default;
    Function(Function&&) = default;
    const float a = .0f;
    const float b = .0f;
    float operator()(float time) const {
        return operator()(a, b, time);
    }
    float operator()(float a, float b, float time) const {
        return a + (b - a) * time;
    }
    Function reversed() const { return Function(b, a); }
};

inline bool operator==(Function const& f1, Function const& f2) {
    return std::fabs(f1.a - f2.a) < __FLT_EPSILON__
        and std::fabs(f1.b - f2.b) < __FLT_EPSILON__;
}
inline bool operator!=(Function const& f1, Function const& f2) {
    return not (operator==(f1, f2));
}
inline Function operator+(Function const &f1, Function const &f2) {
    return Function(f1.a + f2.a, f1.b + f2.b);
}
inline Function operator+(Function const &f1, float c) {
    return Function(f1.a + c, f1.b + c);
}
inline Function operator-(Function const &f1, Function const &f2) {
    return Function(f1.a - f2.a, f1.b - f2.b);
}
inline Function operator-(Function const &f1, float c) {
    return Function(f1.a - c, f1.b - c);
}
inline Function operator*(Function const &f1, Function const &f2) {
    return Function(f1.a * f2.a, f1.b * f2.b);
}
inline Function operator*(Function const &f1, float c) {
    return Function(f1.a * c, f1.b * c);
}
inline Function operator/(Function const &f1, Function const &f2) {
    return Function(f1.a / f2.a, f1.b / f2.b);
}
inline Function operator/(Function const &f1, float c) {
    return Function(f1.a / c, f1.b / c);
}

template <typename Begin, typename End, typename User_func>
void apply_over_range(Function const &fun, Begin b, End e, float t1, float t2,
                      User_func f) {
    size_t index = 0;
    const float size = std::distance(b, e);
    std::for_each(b, e, [&](auto& v) {
        f(fun(t1 + (float(index++)/size) * (t2 - t1)), v);
    });
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
struct Discrete_function: Function {
    Discrete_function(Function const& f, Range<size_t> const& r):
        Function(f), range(r) {}
    Discrete_function(float a, float b, Range<size_t> const& r):
        Function(a, b), range(r) {}
    Discrete_function() = default;
    Discrete_function(Discrete_function const&) = default;
    Discrete_function(Discrete_function&&) = default;
    
    const Range<size_t> range;
    
    Discrete_function with_range(Range<size_t> const& r) const {
        return Discrete_function(a, b, r);
    }
    Discrete_function with_function(Function const& f) const {
        return Discrete_function(f.a, f.b, range);
    }

    template <typename Begin, typename End>
    void operator()(const size_t pos, Begin b, End e) const {
        const auto t1 =
            (float(pos) - float(range.begin())) / float(range.size());
        const auto t2 =
            (float(pos + std::distance(b, e)) - float(range.begin())) /
            float(range.size());
        apply_over_range(*this, b, e, t1, t2,
                         [](float fun_val, float &v) { v *= fun_val; });
    }

    float operator()(size_t p) const {
        return Function::operator()((float(p) - float(range.begin())) /
                                    float(range.size()));
    }
};

} // namespace
#endif /* sample_function_hpp */
