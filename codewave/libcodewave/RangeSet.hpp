//
//  RangeSet.hpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef RangeSet_hpp
#define RangeSet_hpp

#include "range.h"
#include "range_cont.h"
#include <list>

using Pos_t = int;
using Range_t = ec::Range<Pos_t>;
using Range_set_t = std::list<Range_t>;

#define UNWRAP_RSET(set, name) \
    expects(set != nullptr); \
    auto& name = *reinterpret_cast<Range_set_t*>(set); \
    (void)name;

#endif /* RangeSet_hpp */
