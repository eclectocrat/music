//
//  Wave.hpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef Wave_hpp
#define Wave_hpp

#include "sample.h"

using Wave_t = ec::Sample;

#define UNWRAP_WAVE(wave, name) \
    expects(wave != nullptr); \
    auto& name = *reinterpret_cast<Wave_t*>(wave); \
    (void)name;

#endif /* Wave_hpp */
