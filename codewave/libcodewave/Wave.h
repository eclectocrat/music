//
//  Wave.h
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef Wave_h
#define Wave_h

#include "RangeSet.h"

#ifdef __cplusplus
extern "C" {
#endif


/// Sample :: Real
typedef float Sample;
typedef void* Wave;


/// make_empty :: Void -> W
Wave Wave_make_empty();


/// make_copy :: W -> W
Wave Wave_make_copy(Wave);


/// destroy :: W -> Void
void Wave_destroy(Wave);


/// frame_count :: W -> N
int Wave_frame_count(Wave);


/// channel_count :: W -> N where(0 <= N <= 2)
int Wave_channel_count(Wave);


/// open_stream :: W -> String -> Void then{
///    fail!, W.stream != Void
/// }
void Wave_open_stream(Wave, const char *);


/// stream :: W -> {String, Void}
const char * Wave_stream(Wave);


/// map_stream_frames :: W -> Void where{
///     fail!, W.stream != Void
/// } then (
///     TODO: how to state/check that samples are available?
/// )
void Wave_map_stream_frames(Wave);


/// unmap_frames :: W -> Void
void Wave_unmap_frames(Wave);


/// copy_samples :: W -> channel: Int -> Pos -> amount: N -> [Sample] -> Void
/// where(
///     0 <= channel < W.channel_count,
///     0 <= amount,
///     0 <= Pos,
///     0 <= Pos + amount <= W.frame_count,
/// )
void Wave_copy_samples(Wave, int channel, Pos pos, int amount, Sample * output);


/// write_samples :: W -> [Sample] -> amount: N -> channel: N -> Pos -> Void
/// where(
/// )
void Wave_write_samples(Wave, Sample * samples, int amount, int channel, Pos);


/// remove :: W -> RangeSet -> Void where(
///     0 <= RangeSet.bounds.begin <= RangeSet.bounds.end <= W.frame_count
/// ) then (
///     W.frame_count -= RangeSet.bounds.size
/// )
void Wave_remove(Wave, RangeSet);


/// take_from :: to: W -> from: W -> RangeSet -> insert_pos: Pos
/// where(
///     0 <= RangeSet.bounds.begin <= RangeSet.bounds.end <= from.frame_count,
///     0 <= Pos <= to.frame_count
/// ) then (
///     from.frame_count -= RangeSet.bounds.size
///     to.frame_count += RangeSet.bounds.size
/// )
void Wave_take_from(Wave, Wave from, RangeSet, Pos);


/// copy_from :: to: W -> from: W -> RangeSet -> Pos -> Void where(
///     RangeSet.end <= from.frame_count,
///     Pos <= to.frame_count
/// )
void Wave_copy_from(Wave, Wave from, RangeSet, Pos);


/// crop :: W -> RangeSet -> Void
///     then W.frame_count = reduce RangeSet f :: Range -> N Range.size
void Wave_crop(Wave, RangeSet);


#ifdef __cplusplus
} // end extern "C"
#endif

#endif // Wave_h
