//
//  WaveHead.hpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef WaveHead_hpp
#define WaveHead_hpp

#include "sample_head.h"

using Wave_head_t = ec::Sample_head;

#define UNWRAP_HEAD(head, name) \
    expects(head != nullptr); \
    auto& name = *reinterpret_cast<Wave_head_t*>(head); \
    (void)name;

#endif /* WaveHead_hpp */
