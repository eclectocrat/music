//
//  WaveHead.h
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef WaveHead_h
#define WaveHead_h

#include "RangeSet.h"
#include "Wave.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef void* WaveHead;

/// make :: Void -> H
WaveHead WaveHead_make();


/// destroy :: H -> Void where {fail!, H.running = False}
void WaveHead_destroy(WaveHead);


enum WaveHeadDirection {
    WaveHeadDirection_forward,
    WaveHeadDirection_backward
};

/// set dir :: H -> d: {Forward, Backward} -> Void then H.dir = d
void WaveHead_set_dir(WaveHead h, WaveHeadDirection d);


/// dir :: H -> {Forward, Backward}
WaveHeadDirection WaveHead_dir(WaveHead);


/// set pos :: H -> Pos -> Void where(
///     {fail!, H.wave != Void},
///     {fail!, 0 <= Pos <= H.wave.frame_count}
/// ) then H.pos = <unknown?>
/// pos :: H -> {Pos, 0}

/// Sets the play position to the given absolute frame index. If there is no
/// Wave attached, or the position is out of range of the attched Wave, then
/// calling this is a logic error.

void WaveHead_set_pos(WaveHead, Pos);

Pos WaveHead_pos(WaveHead);


/// set normal :: H -> Real -> Void where{fail!, H.wave != Void}
///     normal :: H -> Real         where{fail!, H.wave != Void}

/// Sets the play position, as a normalized Real value in the interval (0, 1).
/// If the position given is outside of (0, 1), it's absolute value is truncated
/// to to be within it. If there is no Wave attached, calling this is an error.

void WaveHead_set_normal(WaveHead h, Real p);

Real WaveHead_normal(WaveHead);


/// let x := <implementation defined limit>
/// set speed :: H -> Real -> Void where{fail!, x < Real}
///     speed :: H -> Real

/// Sets the playback speed of the head. Due to how the buffers are read
/// internally, there is a lower limit to the speed.

/// TODO: Expose this limit, or better yet just remove the limitation.

void WaveHead_set_speed(WaveHead, Real);

Real WaveHead_speed(WaveHead);


/// set amp mod :: H -> Real -> Void
///     amp mod :: H -> Real

void WaveHead_set_amp_mod(WaveHead, Real);

Real WaveHead_amp_mod(WaveHead);


/// set looping :: H -> {True, False} -> Void
///     looping :: H -> {True, False}

void WaveHead_set_looping(WaveHead, bool);

bool WaveHead_looping(WaveHead);


/// set range :: H -> Range -> Pos -> Void
/// where(
///     {fail!, H.wave != Void},
///     {fail!, 0 <= Range.begin <= H.wave.frame_count},
///     {fail!, Range.end <= H.wave.frame_count}
/// ) then (
///     Range.begin <= H.pos <= Range.end
/// )
/// range :: H -> Range where{
///     0 <= Range.begin <= H.wave.frame_count,
///     Range == (0, 0)
/// }

void WaveHead_set_range(WaveHead, Range);

Range WaveHead_range(WaveHead);


/// set wave :: H -> Wave -> Void where{fail!, H.running == False}
///     wave :: H -> {Wave, Void}

void WaveHead_set_wave(WaveHead, Wave);

Wave WaveHead_wave(WaveHead);


#ifdef __cplusplus
} // end extern "C"
#endif

#endif // WaveHead_h
