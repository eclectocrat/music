//
//  Wave.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "Wave.h"
#include "Wave.hpp"
#include "RangeSet.hpp"

using namespace ec;
using namespace std;

inline Range<size_t> unsigned_range(Range_t const& r) {
    expects(0 <= begin(r));
    return Range<size_t>(
        static_cast<size_t>(begin(r)),
        static_cast<size_t>(end(r))
    );
}

::Wave Wave_make_empty() {
    return new Wave_t;
}

::Wave Wave_make_copy(::Wave wave) {
    UNWRAP_WAVE(wave, w)
    return new Wave_t{w};
}

void Wave_destroy(::Wave wave) {
    expects(wave != nullptr);
    delete reinterpret_cast<Wave_t*>(wave);
}

int Wave_frame_count(::Wave wave) {
    UNWRAP_WAVE(wave, w)
    return static_cast<int>(w.frame_count());
}

int Wave_channel_count(::Wave wave) {
    UNWRAP_WAVE(wave, w)
    return static_cast<int>(w.channel_count());
}

void Wave_open_stream(::Wave wave, const char * stream_path) {
    UNWRAP_WAVE(wave, w)
    w.open_stream(stream_path);
}

void Wave_map_stream_frames (::Wave wave) {
    UNWRAP_WAVE(wave, w)
    
    // TODO: this function, this concept, does it make sense? I don't like how
    // it works now, but acknowledge that it's an important performance concern.
    
    for_each(begin(w.clips()), end(w.clips()), [&](auto &c) {
        // NOTE: assumes that all clips in the wave are sourced from the
        // same stream and haven't been shuffled. If _stream_range and
        // _file_stream are out of sync or heterogenous across clips, then
        // bizarro land will manifest on the tip of your cat's tail.
        c.map_stream_frames(c.range());
    });
}

void Wave_unmap_frames(::Wave wave) {
    UNWRAP_WAVE(wave, w)
    for_each(begin(w.clips()), end(w.clips()),
             [](auto &c) { c.unmap_frames(); });
}

void Wave_copy_samples(::Wave wave, int channel, Pos pos, int amount, ::Sample * output) {
    UNWRAP_WAVE(wave, w)
    expects(channel < w.channel_count());
    expects(pos+amount < w.frame_count());
    expects(amount >= 0);
    expects(output != nullptr);
    w.for_range({size_t(pos), size_t(pos + amount)},
                [](auto &c) { return c.range(); },
                [&](auto &c) -> auto & { return c.samples()[channel]; },
                [&](float f) { *output++ = f; });
}

void Wave_write_samples(::Wave wave, ::Sample * samples, int amount, int channel, Pos pos) {
    UNWRAP_WAVE(wave, w)
    expects(channel < w.channel_count());
    expects(pos+amount < w.frame_count());
    expects(amount >= 0);
    expects(samples != nullptr);
    w.for_range({size_t(pos), size_t(pos + amount)},
                [](auto &c) { return c.range(); },
                [&](auto &c) -> auto & { return c.samples()[channel]; },
                [&](float &f) { f = *samples++; });
}

void Wave_remove(::Wave wave, RangeSet set) {
    UNWRAP_WAVE(wave, w)
    UNWRAP_RSET(set, rs)
    for_each(begin(rs), end(rs), [&](auto r) {
        w.erase(unsigned_range(r));
    });
}

void Wave_take_from(::Wave wave, ::Wave from, RangeSet set, Pos to) {
    UNWRAP_WAVE(wave, w)
    UNWRAP_WAVE(from, f)
    UNWRAP_RSET(set, rs)
    expects(to >= 0);
    expects(to <= w.frame_count());
    for_each(rbegin(rs), rend(rs), [&](auto r) {
        w.take(unsigned_range(r), f, size_t(to));
    });
}

void Wave_copy_from(::Wave wave, ::Wave from, RangeSet set, Pos to) {
    UNWRAP_WAVE(wave, w)
    UNWRAP_WAVE(from, f)
    UNWRAP_RSET(set, rs)
    expects(to >= 0);
    expects(to <= w.frame_count());
    for_each(rbegin(rs), rend(rs), [&](auto r) {
        w.copy(unsigned_range(r), f, size_t(to));
    });
}

void Wave_crop(::Wave wave, RangeSet set) {
    UNWRAP_WAVE(wave, w)
    UNWRAP_RSET(set, rs)
    
    // ... TODO ...
}
