//
//  RangeSet.h
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef RangeSet_h
#define RangeSet_h

#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

typedef double Real;
typedef int Pos;

/// Range :: (begin: Pos, end: Pos) where Range.begin <= Range.end
typedef struct { Pos begin; Pos end; } Range;


/// make_empty :: Void -> Range
Range Range_make_empty();

/// make :: Pos -> Pos -> Range
Range Range_make(Pos, Pos);


/// copy :: Range -> Range
Range Range_copy(Range);


typedef void* RangeSet;

/// make_empty :: Void -> RangeSet
RangeSet RangeSet_make_empty();


/// make :: Range -> RangeSet
RangeSet RangeSet_make(Range);


/// make_copy :: RangeSet -> RangeSet
RangeSet RangeSet_make_copy(RangeSet);


/// destroy :: RangeSet -> Void
void RangeSet_destroy(RangeSet);


/// bounds :: RangeSet -> Range
Range RangeSet_bounds(RangeSet);


/// range_count :: RangeSet -> Int
int RangeSet_range_count(RangeSet);


/// range_at_index :: RangeSet -> Int -> Range
///     where 0 <= Int < RangeSet.range_count
Range RangeSet_range_at_index(RangeSet, int);


/// begin :: RangeSet -> Pos
Pos RangeSet_begin(RangeSet);


/// end :: RangeSet -> Pos
Pos RangeSet_end(RangeSet);


/// insert :: RangeSet -> RangeSet -> Void
void RangeSet_insert(RangeSet, RangeSet);


/// insert_range :: RangeSet -> Range -> Void
void RangeSet_insert_range(RangeSet, Range);


/// remove :: RangeSet -> RangeSet -> Void
void RangeSet_remove(RangeSet, RangeSet);


/// remove_range :: RangeSet -> Range -> Void
void RangeSet_remove_range(RangeSet, Range);


/// crop :: RangeSet -> RangeSet -> Void
void RangeSet_crop(RangeSet, RangeSet);


///
void RangeSet_crop_range(RangeSet, Range);


/// scale :: RangeSet -> Real -> Void
void RangeSet_scale_by(RangeSet, Real);


/// offset_by :: RangeSet -> Pos -> Void
void RangeSet_offset_by(RangeSet, Pos);


/// reflect :: RangeSet -> Pos -> Void
void RangeSet_reflect(RangeSet, Pos);


/// repeat :: RangeSet -> Int -> Pos where 0 <= Int
void RangeSet_repeat(RangeSet, int times, Pos space_between);


#ifdef __cplusplus
} // end extern "C"
#endif

#endif /* RangeSet_h */
