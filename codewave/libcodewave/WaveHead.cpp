//
//  WaveHead.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "WaveHead.h"
#include "WaveHead.hpp"
#include "Wave.h"
#include "Wave.hpp"
#include "RangeSet.hpp"

using namespace ec;
using namespace std;


///
WaveHead WaveHead_make() {
    return new Wave_head_t;
}

///
void WaveHead_destroy(WaveHead head) {
    expects(head != nullptr);
    delete reinterpret_cast<Wave_head_t*>(head);
}

///
void WaveHead_set_dir(WaveHead head, WaveHeadDirection dir) {
    UNWRAP_HEAD(head, h)
    h.set_dir(dir == WaveHeadDirection_forward
        ? Sample_head::Direction::forward
        : Sample_head::Direction::backward);
}

///
WaveHeadDirection WaveHead_dir(WaveHead head) {
    UNWRAP_HEAD(head, h)
    return h.dir() == Sample_head::Direction::forward
        ? WaveHeadDirection_forward
        : WaveHeadDirection_backward;
}

///
void WaveHead_set_pos(WaveHead head, Pos pos) {
    UNWRAP_HEAD(head, h)
    h.set_pos(static_cast<size_t>(pos));
}

///
Pos WaveHead_pos(WaveHead head) {
    UNWRAP_HEAD(head, h)
    return static_cast<Pos>(h.pos());
}

///
void WaveHead_set_normal(WaveHead head, Real normal) {
    UNWRAP_HEAD(head, h)
    auto w = h.attached();
    expects(w != nullptr);
    h.set_pos(static_cast<size_t>(Real(h.pos()) / Real(w->frame_count())));
}

///
Real WaveHead_normal(WaveHead head) {
    UNWRAP_HEAD(head, h)
    if (auto w = h.attached(); w != nullptr) {
        return Pos(Real(h.pos()) / Real(w->frame_count()));
    } else {
        return .0;
    }
}

///
void WaveHead_set_speed(WaveHead head, Real speed) {
    UNWRAP_HEAD(head, h)
    h.set_speed(speed);
}

///
Real WaveHead_speed(WaveHead head) {
    UNWRAP_HEAD(head, h)
    return h.speed();
}

///
void WaveHead_set_amp_mod(WaveHead head, Real amp) {
    UNWRAP_HEAD(head, h)
    h.set_amp(static_cast<float>(amp));
}

///
Real WaveHead_amp_mod(WaveHead head) {
    UNWRAP_HEAD(head, h)
    return static_cast<Real>(h.amp());
}

///
void WaveHead_set_looping(WaveHead head, bool loop) {
    UNWRAP_HEAD(head, h)
    h.set_loop(loop ? 1 : 0);
}

///
bool WaveHead_looping(WaveHead head) {
    UNWRAP_HEAD(head, h)
    return h.loop() != 0;
}

///
void WaveHead_set_wave(WaveHead head, Wave wave) {
    UNWRAP_HEAD(head, h)
    UNWRAP_WAVE(wave, w)
    //h.attach(
    //    make_shared<Wave_t>(reinterpret_cast<Wave_t*>(wave), [](void *) {}),
    //    Sample_range(), 1.0, 0);
}

///
Wave WaveHead_wave(WaveHead head) {
    UNWRAP_HEAD(head, h)
    // return h.attached();
    return nullptr;
}
