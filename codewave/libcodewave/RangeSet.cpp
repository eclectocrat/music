//
//  RangeSet.cpp
//  codewave
//
//  Created by Jeremy Jurksztowicz on 7/16/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#include "RangeSet.h"
#include "RangeSet.hpp"
#include "lua_script_util.h"
#include "lua_sample_util.h"
#include <limits>

using namespace ec;
using namespace std;

::Range Range_make_empty() {
    return ::Range{};
}

::Range Range_make(Pos b, Pos e) {
    expects(b <= e);
    return ::Range{b, e};
}
RangeSet RangeSet_make_empty() {
    return new Range_set_t{};
}

RangeSet RangeSet_make(::Range r) {
    return new Range_set_t{{r.begin, r.end}};
}

#define UNWRAP_RSET(set, name) \
    expects(set != nullptr); \
    auto& name = *reinterpret_cast<Range_set_t*>(set); \
    (void)name;

RangeSet RangeSet_make_copy(RangeSet set) {
    UNWRAP_RSET(set, rs)
    return new Range_set_t{rs};
}

void RangeSet_destroy(RangeSet set) {
    expects(set != nullptr);
    delete reinterpret_cast<Range_set_t*>(set);
}

::Range RangeSet_bounds(RangeSet set) {
    UNWRAP_RSET(set, rs)
    return {begin(rs.front()), end(rs.back())};
}

int RangeSet_range_count(RangeSet set) {
    UNWRAP_RSET(set, rs)
    return static_cast<int>(rs.size());
}

::Range RangeSet_range_at_index(RangeSet set, int index) {
    UNWRAP_RSET(set, rs)
    expects(index < rs.size());
    auto i = begin(rs);
    std::advance(i, index);
    return {begin(*i), end(*i)};
}

Pos RangeSet_begin(RangeSet set) {
    UNWRAP_RSET(set, rs)
    return begin(rs.front());
}

Pos RangeSet_end(RangeSet set) {
    UNWRAP_RSET(set, rs)
    return end(rs.back());
}

void RangeSet_insert(RangeSet set, RangeSet insert_set) {
    UNWRAP_RSET(set, rs)
    UNWRAP_RSET(insert_set, to_insert)
    for (auto r: to_insert) {
        ec::insert_range(rs, r);
    }
}

void RangeSet_insert_range(RangeSet set, ::Range r) {
    UNWRAP_RSET(set, rs)
    ec::insert_range(rs, Range_t(r.begin, r.end));
}

void RangeSet_remove(RangeSet set, RangeSet remove_set) {
    UNWRAP_RSET(set, rs)
    UNWRAP_RSET(remove_set, to_remove)
    for (auto r: to_remove) {
        ec::remove_range(rs, r);
    }
}

void RangeSet_remove_range(RangeSet set, ::Range r) {
    UNWRAP_RSET(set, rs)
    ec::remove_range(rs, Range_t(r.begin, r.end));
}

void RangeSet_crop(RangeSet set, RangeSet crop_set) {
    UNWRAP_RSET(set, rs)
    UNWRAP_RSET(crop_set, to_crop)
    
    // ... TODO ...
}

void RangeSet_crop_range(RangeSet set, ::Range r) {
    UNWRAP_RSET(set, rs)
    
    // ... TODO ...
}

void RangeSet_scale_by(RangeSet set, Real by) {
    UNWRAP_RSET(set, rs)
    transform(begin(rs), end(rs), begin(rs), [=](auto r) {
        return Range_t(
            Pos(Real(begin(r)) * by),
            Pos(Real(end(r)) * by)
        );
        // TODO: use vectorized instructions, as an array of ranges is just
        // an array of int's.
    });
}

void RangeSet_offset_by(RangeSet set, Pos by) {
    UNWRAP_RSET(set, rs)
    transform(begin(rs), end(rs), begin(rs), [=](auto r) {
        return Range_t(begin(r) + by, end(r) + by);
    });
}

void RangeSet_reflect(RangeSet set, Pos at) {
    UNWRAP_RSET(set, rs)
    // TODO: `at` param?
    
    const auto off = RangeSet_begin(set);
    const auto len = RangeSet_end(set) - off;
    transform(begin(rs), end(rs), begin(rs), [=](auto r) {
        return Range_t(len - begin(r) + off, len - end(r) + off);
    });
    rs.sort([](auto a, auto b) { return begin(a) < begin(b); });
}

void RangeSet_repeat(RangeSet set, int times, Pos space_between) {
    UNWRAP_RSET(set, rs)
    const auto b = begin(rs), e = end(rs);
    for (int i=0; i<times; i++) {
        const auto offset = RangeSet_end(set) + space_between;
        transform(b, e, back_inserter(rs), [&](auto r) {
            return Range_t(begin(r) + offset, end(r) + offset);
        });
    }
}
