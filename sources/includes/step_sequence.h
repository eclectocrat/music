#ifndef _step_sequence_h
#define _step_sequence_h

#include <array>
#include "expects.h"
#include "clock.h"
#include "range.h"
#include "test.h"

namespace ec {

template <typename Uint_t, typename T, typename U>
constexpr U calculate_step(const Clock<T> clock, Range<U> const &range,
                           const Expects_nonzero<Uint_t> clocks_per_step,
                           const Uint_t swing_mod = 0) TESTEXCEPT {
    expects(clock.weak() >= swing_mod);
    return begin(range) + (((clock.weak() - swing_mod) / clocks_per_step) % size(range));
}

}
#endif
