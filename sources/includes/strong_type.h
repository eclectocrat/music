#ifndef ec_strong_type_h
#define ec_strong_type_h

#include <type_traits>
#include <utility>

namespace ec {

template <class Tag, typename T>
class Strong_type {
public:
    using Weak_type = T;
    constexpr Strong_type() : _value() {}
    constexpr explicit Strong_type(const Weak_type& value) : _value(value) {}
    constexpr explicit Strong_type(Weak_type &&value) noexcept(
        std::is_nothrow_move_constructible<Weak_type>::value)
        : _value(std::move(value)) {}
    constexpr explicit operator Weak_type&() noexcept { return _value; }
    constexpr explicit operator Weak_type const&() const noexcept { return _value; }
    constexpr Weak_type const& weak() const noexcept { return _value; }
    constexpr Weak_type& weak() noexcept { return _value; }
    constexpr friend void swap(Strong_type& a, Strong_type& b) noexcept {
        std::swap(static_cast<Weak_type&>(a), static_cast<Weak_type&>(b));
    }

private:
    Weak_type _value;
};

}
#endif
