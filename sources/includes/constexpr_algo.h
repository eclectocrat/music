#ifndef constexpr_algo_h
#define constexpr_algo_h

namespace ec {
namespace algo {

// FILL
template<typename I1, typename I2, typename T>
constexpr void fill(I1 b, I2 e, T const& proto) {
    for (; b!=e; ++b) {
        *b = proto;
    }
}

template<typename C, typename T>
constexpr void fill(C && c, T const& proto) {
    fill(begin(c), end(c), proto);
}

// FIND
template<typename I1, typename I2, typename F>
constexpr auto find(I1 b, I2 e, F&& f) {
   for (; b!=e; ++b) {
        if (f(*b)) {
            return b;
        }
    }
    return e;
}

template<typename C, typename F>
constexpr auto find(C && c, F&& f) {
    return find(begin(c), end(c), f);
}

// FOR_EACH
template<typename I1, typename I2, typename F>
constexpr void for_each(I1 b, I2 e, F&& f) {
    for (; b!=e; ++b) {
        f(*b);
    }
}

template<typename C, typename F>
constexpr void for_each(C && c, F&& f) {
    for_each(begin(c), end(c), f);
}

// COPY STRIDE
template<typename I1, typename I2, typename I3, typename Uint_t>
constexpr void copy_stride(I1 b, I2 e, Uint_t stride, I3 d) {
    for (; b<e; b+=stride) {
        *d++ = *b;
    }
}

template<typename C, typename I, typename Uint_t>
constexpr void copy_stride(C && c, Uint_t stride, I i) {
    copy_stride(begin(c), end(c), stride, i);
}

// ZIP
template<typename I1, typename I2, typename I3, typename I4>
constexpr void zip(I1 b, I2 e, I3 b2, I4 d) {
    while (b != e) {
        *d++ = *b++;
        *d++ = *b2++;
    }
}

} // namespace
} // namespace
#endif
