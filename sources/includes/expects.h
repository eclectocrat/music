#ifndef _expects_h
#define _expects_h

#include <type_traits>
#include <cstddef>
#include <cassert>

namespace ec {
constexpr void expects(const bool test) {
#if !DEBUG
    // Probably ignore on-device assertion failures, but you can do something here.
    // if (not test) {
    //   hal_plot_led(TYPEPAD, 10, MAXLED, 0, 0);
    // }
#else
    if (not test) { throw int(-1); }
#endif
}

#define static_expects(b) static_assert(b, "static_expects() failed in DEBUG mode.")

template<bool Flag> constexpr void _static_expects() {
#if DEBUG
    static_assert(Flag, "static_expects() failed in DEBUG mode.");
#endif
}

template<typename T, typename = std::enable_if<std::is_integral<T>::value>>
struct Expects_nonzero {
    template <typename U>
    constexpr Expects_nonzero(U const& v): _value(v) { expects(_value != 0); }
    constexpr Expects_nonzero(T const& v): _value(v) { expects(_value != 0); }
    constexpr Expects_nonzero(Expects_nonzero const&) = default;
    Expects_nonzero& operator=(Expects_nonzero const&) = default;
    constexpr operator T() const { return _value; }
    constexpr T value() const { return _value; }
    constexpr T& weak() { return _value; }
private:
    T _value = T();
};

template<typename T>
using Expects_nonnull_ptr = Expects_nonzero<T*>;

template<typename T, size_t Begin, size_t End, typename = std::enable_if<std::is_integral<T>::value>>
struct Expects_within {
    template <typename U>
    constexpr Expects_within(U const& v): _value(v) {
        expects(_value >= Begin);
        expects(_value < End);
    }
    constexpr Expects_within(T const& v): _value(v) {
        expects(_value >= Begin);
        expects(_value < End);
    }
    constexpr Expects_within(Expects_within const&) = default;
    Expects_within& operator=(Expects_within const&) = default;
    constexpr operator T() const { return _value; }
    constexpr T value() const { return _value; }
    constexpr T& weak() { return _value; }
private:
    T _value = T();
};

}
#endif
