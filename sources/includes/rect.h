#ifndef EC_Rect_h
#define EC_Rect_h

#include "point.h"
#include "size.h"

namespace ec {

template <typename T, 
          template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
struct Rect {
    Point_type<T> origin;
    Size_type<T> size;

    constexpr Rect() noexcept = default;
    constexpr Rect(Size_type<T> const &s) noexcept: size(s) {}
    constexpr Rect(Point_type<T> const &o, Size_type<T> const &s) noexcept
        : origin(o), size(s) {}
    constexpr Rect(T x, T y, T w, T h) noexcept: origin(x, y), size(w, h) {}
    template <typename U>
    constexpr explicit Rect(Rect<U> const& r): origin(r.origin), size(r.size) {}

    constexpr T min_x() const noexcept { return origin.x; }
    constexpr T max_x() const noexcept { return origin.x + size.w; }
    constexpr T min_y() const noexcept { return origin.y; }
    constexpr T max_y() const noexcept { return origin.y + size.h; }
    
    constexpr T area() const noexcept { return size.w * size.h; }

    constexpr Point_type<T> bottom_left() const noexcept { return Point_type<T>(min_x(), min_y()); }
    constexpr Point_type<T> top_left() const noexcept { return Point_type<T>(min_x(), max_y()); }
    constexpr Point_type<T> bottom_right() const noexcept { return Point_type<T>(max_x(), min_y()); }
    constexpr Point_type<T> top_right() const noexcept { return Point_type<T>(max_x(), max_y()); }

    constexpr bool contains_point(Point_type<T> pt) const noexcept {
        return pt.x >= min_x() and pt.x < max_x() and pt.y >= min_y() and pt.y < max_y();
    }
    constexpr bool contains_rect(Rect const &r2) const noexcept {
        return this->contains_point(r2.bottomLeft()) and
               this->contains_point(r2.topRight() - Point_type<T>(1, 1));
    }
    constexpr auto inset(Size_type<T> sz) const noexcept {
        return Rect(Point_type<T>(origin.x + sz.w, origin.y + sz.h),
                    Size_type<T>(size.w - sz.w * 2, size.h - sz.h * 2));
    }
    constexpr Rect translated(Size_type<T> amt) const noexcept {
        return Rect(origin.translated(amt), size);
    }
    constexpr Rect centered(Point_type<T> pt) const noexcept {
        return Rect(Point<T>(pt.x - size.w / 2, pt.y - size.h / 2), size);
    }
    constexpr Rect clipped(Rect const &bounds) const noexcept {
        Rect Rect = *this;
        if (Rect.min_x() < bounds.min_x()) {
            Rect.origin.x = bounds.min_x();
        }
        if (Rect.min_y() < bounds.min_y()) {
            Rect.origin.y = bounds.min_y();
        }
        if (Rect.max_x() > bounds.max_x()) {
            Rect.size.w = bounds.max_x() - Rect.origin.x - 1;
        }
        if (Rect.max_y() > bounds.max_y()) {
            Rect.size.h = bounds.max_y() - Rect.origin.y - 1;
        }
        return Rect;
    }
};

template <typename T, template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
constexpr bool operator==(Rect<T, Point_type, Size_type> const &r1,
                          Rect<T, Point_type, Size_type> const &r2) noexcept {
    return r1.origin == r2.origin and r1.size == r2.size;
}

template <typename T, template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
constexpr bool operator!=(Rect<T, Point_type, Size_type> const &r1,
                          Rect<T, Point_type, Size_type> const &r2) noexcept {
    return r1.origin != r2.origin or r1.size != r2.size;
}

template <typename T>
constexpr bool Size<T>::contains_point(const Point<T> p) const noexcept {
    return Rect<T>(*this).contains_point(p);
}

}
#endif
