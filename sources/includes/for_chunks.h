//
// Copyright (c) 2018 Jeremy Jurksztowicz
//
//     Permission is hereby granted, free of charge, to any person obtaining a
//     copy of this software and associated documentation files (the
//     "Software"), to deal in the Software without restriction, including
//     without limitation the rights to use, copy, modify, merge, publish,
//     distribute, sublicense, and/or sell copies of the Software, and to permit
//     persons to whom the Software is furnished to do so, subject to the
//     following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef _for_chunks_h
#define _for_chunks_h

#include <algorithm>
#include <numeric>
#include <iterator>
#include "expects.h"
#include "test.h"
#include "range.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// Generic algorithm to process a range of elements in a container of
/// containers of elements, where each sub-container extends the previous
/// sub-container, resulting in an aggregate container with an aggregate sum
/// size(). i.e: std::list<vector<int>>, or std::vector<MyType>.
/// Each element in the top level container must expose two interfaces:
/// offset_f()->size_t which returns the position of the sub-container in the
/// overall aggregate container, and buffer_f()-> which returns a range of
/// elements (anything returning begin() and end() iterators).
///
template <typename List, typename Range, typename F, typename Range_f,
          typename Buffer_f>
void for_chunks(List &list, Range range, Range_f range_f, Buffer_f buffer_f,
                F func) {
    using namespace std;
    expects(not list.empty());
    expects(end(range_f(list.back())) >= end(range));
    auto b = lower_bound(begin(list), end(list),
                         typename List::value_type(begin(range)),
                         [&](auto const &a, auto const &b) {
                             return end(range_f(a)) < end(range_f(b));
                         });
    auto e = upper_bound(begin(list), end(list),
                         typename List::value_type(end(range)),
                         [&](auto const &a, auto const &b) {
                             return begin(range_f(a)) < begin(range_f(b));
                         });
    if (b == e) {
        expects(e != end(list));
        const auto rng = offset(range, -int(begin(range_f(*b))));
        auto& buf = buffer_f(*b);
        func(begin(buf) + begin(rng), begin(buf) + end(rng));
    } else {
        while (b != e) {
            const auto r = range_f(*b);
            const auto rng = offset(intersection(range, r), -int(begin(r)));
            auto &buf = buffer_f(*b);
            func(begin(buf) + begin(rng), begin(buf) + end(rng));
            ++b;
        }
    }
}

namespace test {
using namespace std;
struct Node {
    Node() = default;
    Node(size_t n): off(n) {}
    size_t off = 0;
    size_t len = 0;
    vector<int> vec;
};
TEST_CASE("for_chunks() empty") {
    array<Node, 0> empty;
    SUBCASE("empty list and/or empty range") {
        REQUIRE_THROWS(for_chunks(
            empty, Range(), [](auto) { return Range(); },
            [](auto &n) -> auto & { return n.vec; }, [&](auto b, auto e) {}));
    } SUBCASE("empty node") {
        array<Node, 1> one;
        REQUIRE_THROWS(for_chunks(
            one, Range(0ul, 1ul), [](auto) { return Range(); },
            [](auto &n) -> auto & { return n.vec; }, [&](auto b, auto e) {}));
    }
}
TEST_CASE("for_chunks() single node") {
    array<Node, 1> one;
    SUBCASE("visits all elements once") {
        one.front().vec.resize(9, 0);
        one.front().len = 9;
        for_chunks(
            one, Range(0ul, 9ul),
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [](auto b, auto e) { for_each(b, e, [](auto &i) { i += 1; }); });
        CHECK(find_if(begin(one.front().vec), end(one.front().vec),
                      [](auto i) { return i != 1; }) == end(one.front().vec));
    } SUBCASE("visits a subrange of elements once and skips others") {
        one.front().vec.resize(9, 0);
        one.front().len = 9;
        for_chunks(
            one, Range(5ul, 9ul),
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [](auto b, auto e) { for_each(b, e, [](auto &i) { i += 1; }); });
        CHECK(find_if(begin(one.front().vec), begin(one.front().vec) + 5,
                      [](auto i) { return i != 0; }) ==
              begin(one.front().vec) + 5);
        CHECK(find_if(begin(one.front().vec) + 5, end(one.front().vec),
                      [](auto i) { return i != 1; }) == end(one.front().vec));
    }
}
TEST_CASE("for_chunks() multi-node") {
    array<Node, 3> multi;
    size_t chunk_size = 7;
    size_t chunk_off = 0;
    for_each(begin(multi), end(multi), [&](auto& n) {
        n.vec.resize(chunk_size);
        n.off = chunk_off;
        n.len = chunk_size;
        chunk_off += chunk_size;
        chunk_size += 3;
    });
    const size_t total_size =
        accumulate(begin(multi), end(multi), 0,
                   [](auto t, auto &n) { return t + n.vec.size(); });
    expects(total_size == 30); // assertion for test code, not an actual test
    expects(multi[0].off == 0 and multi[0].len == 7);
    expects(multi[1].off == 7 and multi[1].len == 10);
    expects(multi[2].off == 17 and multi[2].len == 13);
    SUBCASE("throws out of bounds") {
        REQUIRE_THROWS(for_chunks(
            multi, Range(31ul, 32ul), [](auto &n) { return Range(); },
            [](auto &n) -> auto & { return n.vec; }, [](auto b, auto e) {}));
    } SUBCASE("visits all elements once") {
        for_chunks(
            multi, Range(0ul, total_size),
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [](auto b, auto e) { for_each(b, e, [](auto &i) { i += 1; }); });
        for_each(begin(multi), end(multi), [&](auto &n) {
            CHECK(find_if(begin(n.vec), end(n.vec),
                          [](auto i) { return i != 1; }) == end(n.vec));
        });
    } SUBCASE("visits a range of elements once and ignores rest") {
        for_chunks(
            multi, Range(4ul, total_size-4), // span all nodes
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [](auto b, auto e) { for_each(b, e, [](auto &i) { i += 1; }); });
        CHECK(find_if(begin(multi[0].vec) + 4, end(multi[0].vec),
                      [](auto i) { return i != 1; }) == end(multi[0].vec));
        CHECK(find_if(begin(multi[1].vec), end(multi[1].vec),
                      [](auto i) { return i != 1; }) == end(multi[1].vec));
        CHECK(find_if(begin(multi[2].vec), end(multi[2].vec) - 4,
                      [](auto i) { return i != 1; }) == end(multi[2].vec) - 4);
        CHECK(find_if(begin(multi[2].vec), begin(multi[2].vec) + 4, [](auto i) {
                  return i != 1;
              }) == begin(multi[2].vec) + 4);
        CHECK(find_if(end(multi[2].vec) - 4, end(multi[2].vec),
                      [](auto i) { return i != 0; }) == end(multi[2].vec));
    }
}
} // END namespace test
} // END namespace ec

#endif /* _for_chunks_h */
