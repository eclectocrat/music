#ifndef _point_h
#define _point_h

#include <cmath>
#include <limits>

namespace ec {

template <typename T> struct Point {
    T x = 0;
    T y = 0;

    constexpr Point() noexcept = default;
    constexpr Point(T x_, T y_) noexcept : x(x_), y(y_) {}
    template <typename U>
    constexpr explicit Point(Point<U> p): x(p.x), y(p.y) {}
    void operator+=(Point const &p) { x += p.x; y += p.y; }
    void operator-=(Point const &p) { x -= p.x; y -= p.y; }
    constexpr Point operator-() const { return Point(-x, -y); }
    template <typename Size> constexpr Point translated(Size s) const {
        return Point(x + s.w, y + s.h);
    }
};

template <typename T> constexpr bool operator==(Point<T> const &p1, Point<T> const &p2) { return p1.x == p2.x and p1.y == p2.y; }
template <typename T> constexpr bool operator!=(Point<T> const &p1, Point<T> const &p2) { return p1.x != p2.x or p1.y != p2.y; }
template <typename T> constexpr auto operator+(Point<T> const &p1, Point<T> const &p2) { return Point<T>(p1.x + p2.x, p1.y + p2.y); }
template <typename T> constexpr auto operator-(Point<T> const &p1, Point<T> const &p2) { return Point<T>(p1.x - p2.x, p1.y - p2.y); }
template <typename T> constexpr auto operator*(Point<T> const &p, T m) { return Point<T>(p.x * m, p.y * m); }
template <typename T> constexpr auto operator/(Point<T> const &p, T m) { return Point<T>(p.x / m, p.y / m); }
template <typename T> constexpr auto min_y(Point<T> const &p1, Point<T> const &p2) { return p1.y <= p2.y ? p1 : p2; }
template <typename T> constexpr auto max_y(Point<T> const &p1, Point<T> const &p2) { return p1.y >= p2.y ? p1 : p2; }
template <typename T> constexpr auto min_x(Point<T> const &p1, Point<T> const &p2) { return p1.x <= p2.x ? p1 : p2; }
template <typename T> constexpr auto max_x(Point<T> const &p1, Point<T> const &p2) { return p1.x >= p2.x ? p1 : p2; }

template <typename T> constexpr double slope(Point<T> p1, Point<T> p2) {
    const auto d = (p2.x - p1.x);
    return d ? (p2.y - p1.y) / d : std::numeric_limits<float>::max();
}

template <typename T> constexpr bool operator<(Point<T> const &p1, Point<T> const &p2) {
    if (p1.y == p2.y) {
        return p1.x < p2.x;
    } else {
        return p1.y < p2.y;
    }
}

template <typename T> constexpr double linear_distance(Point<T> p1, Point<T> p2) {
    const double dx = std::fabs(p1.x - p2.x);
    const double dy = std::fabs(p1.y - p2.y);
    return std::sqrt(std::pow(dx, 2) + std::pow(dy, 2));
}

}
#endif
