#ifndef clock_h
#define clock_h

#include "strong_type.h"

namespace ec {

template<typename C>
struct Clock: Strong_type<Clock<C>, C> {
    using Counter_t = C;
    using Strong_t = Strong_type<Clock<C>, C>;
    using Strong_t::Strong_t;
    using Weak_t = typename Strong_t::Weak_type;

    constexpr void tick() noexcept { ++Strong_t::weak(); }
    constexpr Weak_t tick_count() const noexcept { return Strong_t::weak(); }
    constexpr Clock& operator++() noexcept {
        tick();
        return *this;
    }
    constexpr Clock operator++(int) noexcept {
        auto c = *this;
        tick();
        return c;
    }
};

template<typename C, typename N>
constexpr Clock<C> operator - (const Clock<C> c, const N n) noexcept {
    return Clock<C>(c.weak() - n);
}

}
#endif
