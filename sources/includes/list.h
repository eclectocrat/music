
template <typename N>
struct Node {
    Node() = default;
    std::atomic<N*> next = nullptr;
};

template<typename N>    // N must inherit FreeListNode or have the same fields (and initialization)
struct List {
    List() = default;

    inline void append(N * node) {
        auto h = head.load(std::memory_order_relaxed);
        auto nh = Head_ptr{ node };
        do {
            nh.tag = h.tag + 1;
            node->next.store(h.ptr, std::memory_order_relaxed);
        } while (!head.compare_exchange_weak(h, nh, std::memory_order_release,
                                             std::memory_order_relaxed));
    }

    inline N * try_get()
    {
        auto h = head.load(std::memory_order_acquire);
        HeadPtr nh;
        while (h.ptr != nullptr) {
            nh.ptr = h.ptr->next.load(std::memory_order_relaxed);
            nh.tag = h.tag + 1;
            if (head.compare_exchange_weak(h, nh, std::memory_order_release,
                                           std::memory_order_acquire)) {
                break;
            }
        }
        return h.ptr;
    }

    N * head_unsafe() const {
        return head.load(std::memory_order_relaxed).ptr;
    }

private:
    struct Head_ptr {
        N* ptr;
        std::uintptr_t tag;
    };

    std::atomic<Head_ptr> head = Head_ptr();
};

template <typename T> class Slist {
    struct Node {
        T t;
        shared_ptr<Node> next;
    };
    atomic<shared_ptr<Node>> head;

public:
    Slist() = default;
    ~Slist() = default;

    class reference {
        shared_ptr<Node> p;

    public:
        reference(shared_ptr<Node> p_) : p{_p} {}
        T &operator*() { return p->t; }
        T *operator->() { return &p->t; }
    };

    auto find(T const& t) const {
        auto p = head.load();
        while (p and p->t != t) {
            p = p->next;
        }
        return reference{move(p)};
    }

    void push_front(T t) {
        auto p = make_shared<Node>();
        p->t = t;
        p->next = head;
        while (head.compare_exchange_weak(p->next, p)) {
        }
    }

    void pop_front() {
        auto p = head.load();
        while (p and not head.compare_exchange_weak(p, p->next)) {
        }
    }
};
