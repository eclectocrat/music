#ifndef log_h
#define log_h

#if DEBUG
#include <iostream>
namespace ec {
    auto& log_str = std::cout;
    auto log_end = '\n';
}
#else
namespace {
    class Null_stream {};
    template<typename T> Null_stream& operator << (Null_stream& os, T const& t) {
        return os;
    };
    auto log_str = Null_stream();
    auto log_end = false;
}
#endif
#endif /* log_h */
