#include "program.h"
#include <iostream>
#include <GL/gl3w.h>
#include <SDL2/SDL.h>
#include <imgui/imgui.h>
#include <imgui/imgui_impl_sdl_gl3.h>
#include <stdio.h>

using namespace std;
using namespace ec;

Imgui_program const * program = nullptr;
const ImVec4 clear_color = ImColor(40, 40, 40);

static void draw(SDL_Window *window) {
    ImGui_ImplSdlGL3_NewFrame(window);
    glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x,
               (int)ImGui::GetIO().DisplaySize.y);
    glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
    glClear(GL_COLOR_BUFFER_BIT);
    program->run_windows();
    ImGui::Render();
    SDL_GL_SwapWindow(window);
}

static int resizing_event(void *data, SDL_Event *event) {
    if (event->type == SDL_WINDOWEVENT and
        event->window.event == SDL_WINDOWEVENT_RESIZED) {
        SDL_Window *window = SDL_GetWindowFromID(event->window.windowID);
        if (window == reinterpret_cast<SDL_Window *>(data)) {
            draw(window);
        }
    }
    return 0;
}

int ec::imgui_program_main(Imgui_program const& prog, int, char **) {
    program = &prog;
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0) {
        printf("Error: %s\n", SDL_GetError());
        return -1;
    }
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_DisplayMode current;
    SDL_GetCurrentDisplayMode(0, &current);
    SDL_Window *window = SDL_CreateWindow(
        "imgui-program", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        static_cast<int>(prog.window_width),
        static_cast<int>(prog.window_height),
        SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    SDL_AddEventWatch(resizing_event, window);
    SDL_GLContext glcontext = SDL_GL_CreateContext(window);
    gl3wInit();
    ImGui_ImplSdlGL3_Init(window);
    bool done = false;
    Uint32 delta_time = 0;
    Uint32 clock = SDL_GetTicks();
    Uint32 render_timer = 0;
    while (not done) {
        delta_time = SDL_GetTicks() - clock;
        clock = SDL_GetTicks();
        if (render_timer >= 15) {
            render_timer -= 15;
            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                ImGui_ImplSdlGL3_ProcessEvent(&event);
                switch (event.type) {
                case SDL_QUIT: {
                    done = true;
                } break;
                case SDL_KEYUP:
                    if (prog.key_up) {
                        prog.key_up(event.key.keysym.sym);
                    }
                    break;
                case SDL_KEYDOWN:
                    if (prog.key_down) {
                        prog.key_down(event.key.keysym.sym);
                    }
                    break;
                case SDL_MOUSEMOTION: {
                    if (prog.mouse_moved) {
                        prog.mouse_moved(
                            event.motion.state & SDL_BUTTON_LEFT,
                            event.motion.state & SDL_BUTTON_RIGHT,
                            {size_t(event.motion.x), size_t(event.motion.y)});
                    }
                } break;
                case SDL_MOUSEBUTTONDOWN: {
                    if (prog.mouse_down) {
                        prog.mouse_down(
                            event.button.state & SDL_BUTTON_LEFT,
                            event.button.state & SDL_BUTTON_RIGHT,
                            {size_t(event.button.x), size_t(event.button.y)});
                    }
                } break;
                case SDL_MOUSEBUTTONUP: {
                    if (prog.mouse_up) {
                        prog.mouse_up(
                            event.button.state & SDL_BUTTON_LEFT,
                            event.button.state & SDL_BUTTON_RIGHT,
                            {size_t(event.button.x), size_t(event.button.y)});
                    }
                } break;
                case SDL_WINDOWEVENT: {
                    switch(event.window.event) {
                    // case SDL_WINDOWEVENT_RESIZED:
                    case SDL_WINDOWEVENT_SIZE_CHANGED:
                        if (prog.window_state) {
                            prog.window_state(Size<float>(event.window.data1, event.window.data2));
                        }
                    }
                } break;
                }
            }
            draw(window);
        } else if(prog.idle) {
            prog.idle(15 - render_timer);
        } else {
            SDL_Delay(15 - render_timer);
        }
        render_timer += delta_time;
    }
    prog.cleanup();
    
    // Cleanup
    ImGui_ImplSdlGL3_Shutdown();
    SDL_GL_DeleteContext(glcontext);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}

