//
// Copyright (c) 2017 Jeremy Jurksztowicz
//
//     Permission is hereby granted, free of charge, to any person obtaining a
//     copy of this software and associated documentation files (the
//     "Software"), to deal in the Software without restriction, including
//     without limitation the rights to use, copy, modify, merge, publish,
//     distribute, sublicense, and/or sell copies of the Software, and to permit
//     persons to whom the Software is furnished to do so, subject to the
//     following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef __imgui_grid_h
#define __imgui_grid_h

#include "imgui-tools/window.h"
#include <imgui/imgui_internal.h>
#include <vector>
#include <tuple>
#include <iostream>

namespace ec {
namespace imgui {
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
class Grid_window: public imgui::Window {
public:
    Grid_window(std::string const &name_, Wrect const &frame_,
                Size<size_t> grid_size_, bool show_)
        : Window(name_, frame_, show_), _grid_size(grid_size_),
          _pads_state(Rect<size_t>(grid_size_).area(), 0) {}

protected:
    struct Pad_index {
        Pad_index(Point<size_t> p, size_t s): coords(p), flat_index(s) {}
        Pad_index(Pad_index const&) = default;
        Pad_index& operator=(Pad_index const&) = delete;
        Point<size_t> coords;
        size_t flat_index;
    };

    struct Pad_state {
        bool pressed = false;
        bool held = false;
        bool hovered = false;
    };

private:
    void render_window() override {
        using namespace ImGui;
        auto draw_list = ImGui::GetWindowDrawList();
        expects(draw_list != nullptr);
        const Rect<size_t> grid_rect(_grid_size);
        _pad_size = Wsize(Size<size_t>(size() - _pad_padding) / _grid_size) -
                    _pad_padding;
        ImGuiWindow *window = GetCurrentWindow();
        ImGuiIO& io = GetIO();
        if (window->SkipItems) {
            return;
        }
        std::for_each(begin(grid_rect), end(grid_rect), [&](auto pt) {
            const auto index =
                Pad_index(pt, pt.y * size_t(_grid_size.w) + pt.x);
            expects(index.flat_index < _pads_state.size());
            const auto state = pad(window, index);
            if(state.pressed) {
                if(io.MouseDown[0]) {
                    if(not _pads_state[index.flat_index]) {
                        _pads_state[index.flat_index] = 1;
                        pad_was_pressed(index);
                    }
                } else {
                    if(_pads_state[index.flat_index]) {
                        _pads_state[index.flat_index] = 0;
                        pad_was_released(index);
                    }
                }
            } else if(io.MouseDown[0] and not state.hovered) {
                if(_pads_state[index.flat_index]) {
                    _pads_state[index.flat_index] = 0;
                    pad_was_released(index);
                }
            }
        });
    }

    // To override:
    virtual void render_pad(Pad_index const &, const ImRect bb,
                            Pad_state const &state) {
        using namespace ImGui;
        const ImU32 col = GetColorU32(
            (state.hovered && state.held)
                ? ImGuiCol_ButtonActive
                : state.hovered ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
        RenderFrame(bb.Min, bb.Max, col, true, GImGui->Style.FrameRounding);
    }

    virtual void pad_was_pressed(Pad_index const&) = 0;
    virtual void pad_was_released(Pad_index const&) = 0;
   
    // Private details:
    Pad_state pad(ImGuiWindow *window, Pad_index index) {
        using namespace ImGui;
        const char pad_name[2] = {char(index.coords.x - '0'),
                                  char(index.coords.y - '0')};
        const ImGuiID id = window->GetID(pad_name);
        const auto pos = location().translated(_pad_padding) +
                         Wpoint(index.coords) * (_pad_size + _pad_padding);
        const auto end_pos = pos.translated(_pad_size);
        const ImRect bb(to_ImVec(pos), to_ImVec(end_pos));
        ItemSize(bb, GImGui->Style.FramePadding.y);
        if (!ItemAdd(bb, &id)) {
            return {false, false, false};
        }
        auto state = check_pad(window, id, bb);
        render_pad(index, bb, state);
        return state;
    }

    Pad_state check_pad(ImGuiWindow *window, ImGuiID id, ImRect const& bb) {
        bool hovered, held;
        bool pressed = ImGui::ButtonBehavior(bb, id, &hovered, &held, _flags);
        return {pressed, held, hovered};
    }

    Size<size_t> _grid_size = {10, 10};
    std::vector<unsigned char> _pads_state;
    Wsize _pad_padding = {4, 4};
    Wsize _pad_size;
    ImGuiButtonFlags _flags =
        ImGuiButtonFlags_PressedOnClick | ImGuiButtonFlags_PressedOnRelease;
};
} // namespace
} // namespace
#endif // __imgui_grid_h
