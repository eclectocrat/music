//
// Copyright (c) 2017 Jeremy Jurksztowicz
//
//     Permission is hereby granted, free of charge, to any person obtaining a
//     copy of this software and associated documentation files (the
//     "Software"), to deal in the Software without restriction, including
//     without limitation the rights to use, copy, modify, merge, publish,
//     distribute, sublicense, and/or sell copies of the Software, and to permit
//     persons to whom the Software is furnished to do so, subject to the
//     following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef ec_window_h
#define ec_window_h

#include "expects.h"
#include "point.h"
#include "size.h"
#include "rect.h"
#include "rect_iter.h"
#include <imgui/imgui.h>
#include <cassert>
#include <string>

namespace ec {
namespace imgui {
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
typedef Point<float> Wpoint;
typedef Size<float> Wsize;
typedef Rect<float> Wrect;

inline auto to_ImVec(Point<float> p) {
    return ImVec2(p.x, p.y);
}
inline auto to_ImVec(Size<float> s) {
    return ImVec2(s.w, s.h);
}
inline auto to_ImVec(Rect<float> r) {
    return ImVec4(r.origin.x, r.origin.y, r.size.w, r.size.h);
}
inline auto to_Point(ImVec2 v) {
    return Wpoint(v.x, v.y);
}
inline auto to_Size(ImVec2 v) {
    return Wsize(v.x, v.y);
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
class Window {
public:
    Window(std::string const &name_, Wrect const &frame_, bool show_)
        : _name(name_), _frame(frame_), _show(show_) {}

    virtual ~Window() {}

    void set_name(std::string const &s) { _name = s; }
    std::string const &name() const { return _name; }

    void set_origin(Wpoint p) { _frame.origin = p; }
    Wpoint origin() const { return _frame.origin; }

    void set_size(Wsize s) { _frame.size = s; }
    Wsize size() const { return _frame.size; }

    void show() { _show = true; }
    void hide() { _show = false; }
    void set_showing(bool b) { _show = b; }
    bool is_showing() const { return _show; }

    void set_has_titlebar(bool b) { _has_titlebar = b; }
    bool has_titlebar() const { return _has_titlebar; }

    void set_has_border(bool b) { _has_border = b; }
    bool has_border() const { return _has_border; }

    void set_is_resizable(bool b) { _is_resizable = b; }
    bool is_resizable() const { return _is_resizable; }

    void set_is_moveable(bool b) { _is_moveable = b; }
    bool is_moveable() const { return _is_moveable; }

    void set_has_scrollbar(bool b) { _has_scrollbar = b; }
    bool has_scrollbar() const { return _has_scrollbar; }

    void set_is_collapsable(bool b) { _is_collapsable = b; }
    bool is_collapsable() const { return _is_collapsable; }

    void set_has_menu(bool b) { _has_menu = b; }
    bool has_menu() const { return _has_menu; }

    ImGuiWindowFlags window_flags() const {
        ImGuiWindowFlags window_flags = 0;
        if (not _has_titlebar) {
            window_flags |= ImGuiWindowFlags_NoTitleBar;
        }
        if (_has_border) {
            window_flags |= ImGuiWindowFlags_ShowBorders;
        }
        if (not _has_scrollbar) {
            window_flags |= ImGuiWindowFlags_NoScrollbar;
        }
        if (_has_menu) {
            window_flags |= ImGuiWindowFlags_MenuBar;
        }
        if (not _is_resizable) {
            window_flags |= ImGuiWindowFlags_NoResize;
        }
        if (not _is_moveable) {
            window_flags |= ImGuiWindowFlags_NoMove;
        }
        if (not _is_collapsable) {
            window_flags |= ImGuiWindowFlags_NoCollapse;
        }
        return window_flags;
    }

    // Don't override unless you have to, overriding `render_window()` is
    // usually enough.
    virtual void run() {
        if (not _show) {
            return;
        }
        if (begin_window()) {
            if (not is_moveable()) {
                ImGui::SetWindowPos(to_ImVec(_frame.origin));
            }
            render_window();
        }
        end_window();
    }

protected:
    bool *show_address() { return &_show; }
    bool const *show_address() const { return &_show; }

    // only run these in your `render_window()` implementation.
    Wpoint location() const { return to_Point(ImGui::GetWindowPos()); }

private:
    virtual void render_window() {
        expects(false and "No default implementation.");
    }

    virtual bool begin_window() {
        ImGui::SetNextWindowSize(ImVec2(_frame.size.w, _frame.size.h));
        return ImGui::Begin(name().c_str(), show_address(), window_flags());
    }

    virtual void end_window() {
        ImGui::End();
    }

    std::string _name;
    Wrect _frame;

    bool _show = true;

    bool _has_titlebar = false;
    bool _has_border = false;
    bool _has_scrollbar = false;
    bool _has_menu = false;

    bool _is_resizable = false;
    bool _is_moveable = false;
    bool _is_collapsable = false;
};

} // namespace
} // namespace
#endif /* ec_window_h */
