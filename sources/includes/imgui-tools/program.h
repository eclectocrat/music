#ifndef _imgui_app_h
#define _imgui_app_h

#include "rect.h"
#include <map>
#include <functional>

namespace ec {

struct Imgui_program {
    using Key_code = int;
    using Key_state = bool;
    using Run_windows_f = std::function<void ()>;
    using Idle_f        = std::function<void (size_t)>;
    using Cleanup_f     = std::function<void ()>;
    using Key_state_f   = std::function<void (Key_code)>;
    using Mouse_state_f = std::function<void (bool, bool, Point<size_t>)>;
    using Window_state_f= std::function<void (Size<float>)>;

    size_t window_width = 800;
    size_t window_height = 800;

    Run_windows_f   run_windows;
    Cleanup_f       cleanup;
    Idle_f          idle;
    Key_state_f     key_down;
    Key_state_f     key_up;
    Mouse_state_f   mouse_moved;
    Mouse_state_f   mouse_down;
    Mouse_state_f   mouse_up;
    Window_state_f  window_state;
};

int imgui_program_main(Imgui_program const&, int, char **);

} // namespace
#endif // __imgui_app_h
