#ifndef ec_rect_iter_h
#define ec_rect_iter_h

#include "rect.h"
#include <iterator>
#include <algorithm>

namespace ec {

template <typename T, template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
struct Rect_iter : std::iterator<std::forward_iterator_tag, Point_type<T>,
                                 size_t, Point_type<T> *, Point_type<T> &> {
    Rect<T, Point_type, Size_type> rect;
    Point_type<T> location;

    constexpr Rect_iter() noexcept = default;
    constexpr Rect_iter(Rect<T, Point_type, Size_type> const &r) noexcept: rect(r), location(r.origin.x, r.origin.y) {}
    constexpr Rect_iter(Size_type<T> const &s) noexcept: rect(Rect<T>(Point_type<T>(), s)), location(0, 0) {}
    constexpr Rect_iter(Rect<T, Point_type, Size_type> const &r, Point_type<T> p) noexcept: rect(r), location(p) {}
    constexpr Rect_iter(Rect_iter const&) = default;
    Rect_iter& operator=(Rect_iter const&) = default;
    constexpr bool operator==(Rect_iter const& i) const noexcept { return rect == i.rect and location == i.location; }
    constexpr bool operator!=(Rect_iter const& i) const noexcept { return rect != i.rect or location != i.location; }
    constexpr Point_type<T> const &operator*() const noexcept { return location; }
    constexpr Point_type<T> const *operator->() const noexcept { return &location; }
    constexpr Rect_iter& operator++() noexcept { next(); return *this; }
    constexpr Rect_iter  operator++(int) noexcept { 
        const auto r = *this; 
        next(); 
        return r; 
    }

private:
    constexpr void next() noexcept {
        if (++location.x >= rect.max_x()) {
            location.x = rect.min_x();
            ++location.y;
        }
    }
};

template <typename T, template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
auto begin(Rect<T, Point_type, Size_type> const& r) noexcept {
    return Rect_iter<T, Point_type, Size_type>(r);
}
template <typename T, template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
auto end(Rect<T, Point_type, Size_type> const& r) noexcept {
    return Rect_iter<T, Point_type, Size_type>(r, r.top_left());
}

}
#endif
