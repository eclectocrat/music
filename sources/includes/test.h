#ifndef ec_test_h
#define ec_test_h
#if USE_TEST_FRAMEWORK
    #include "doctest.h"
    #define TESTEXCEPT noexcept
    #define DOCTEST_CONFIG_DISABLE 1
    #define DOCTEST_CONFIG_NO_EXCEPTIONS_BUT_WITH_ALL_ASSERTS 1
#else
    #define TESTEXCEPT
#endif
#endif
