#ifndef EC_Size_h
#define EC_Size_h

#include "point.h"

namespace ec {

template <typename T> struct Size {
    T w = 0;
    T h = 0;

    constexpr Size() noexcept = default;
    constexpr Size(T w_, T h_) noexcept : w(w_), h(h_) {}
    template <typename U>
    constexpr explicit Size(Size<U> s): w(s.w), h(s.h) {}
    
    constexpr T area() const { return w * h; }
    constexpr bool contains_size(const Size s) const { return w >= s.w and h >= s.h; }
    constexpr bool contains_point(const Point<T> p) const noexcept;

    void operator+=(Size sz) { w += sz.w; h += sz.h; }
    void operator-=(Size sz) { w -= sz.w; h -= sz.h; }
    void operator+=(T val) { w += val; h += val; }
    void operator-=(T val) { w -= val; h -= val; }
    void operator*=(Size sz) { w *= sz.w; h *= sz.h; }
    void operator/=(Size sz) { w /= sz.w; h /= sz.h; }
    void operator*=(T val) { w *= val; h *= val; }
    void operator/=(T val) { w /= val; h /= val; }
};

template <typename T> constexpr bool operator==(Size<T> const &s1, Size<T> const &s2) { return s1.w == s2.w and s1.h == s2.h; }
template <typename T> constexpr bool operator!=(Size<T> const &s1, Size<T> const &s2) { return s1.w != s2.w or s1.h != s2.h; }
template <typename T> constexpr bool operator<(Size<T> const &s1, Size<T> const &s2) { return s1.area() < s2.area(); }
template <typename T> constexpr auto operator-(Size<T> const &s1) { return Size<T>(-s1.w, -s1.h); }
template <typename T> constexpr auto operator-(Size<T> const &s1, Size<T> const &s2) { return Size<T>(s1.w - s2.w, s1.h - s2.h); }
template <typename T> constexpr auto operator-(Size<T> const &s1, T a) { return Size<T>(s1.w - a, s1.h - a); }
template <typename T> constexpr auto operator+(Size<T> const &s1, Size<T> const &s2) { return Size<T>(s1.w + s2.w, s1.h + s2.h); }
template <typename T> constexpr auto operator+(Size<T> const &s1, T a) { return Size<T>(s1.w + a, s1.h + a); }
template <typename T> constexpr auto operator/(Size<T> const &s1, Size<T> const &s2) { return Size<T>(s1.w / s2.w, s1.h / s2.h); }
template <typename T> constexpr auto operator/(Size<T> const &s1, T a) { return Size<T>(s1.w / a, s1.h / a); }
template <typename T> constexpr auto operator*(Size<T> const &s1, Size<T> const &s2) { return Size<T>(s1.w * s2.w, s1.h * s2.h); }
template <typename T> constexpr auto operator*(Size<T> const &s1, T a) { return Size<T>(s1.w * a, s1.h * a); }
template <typename T> constexpr auto operator+(Point<T> const &p, Size<T> const &s) { return Point<T>(p.x + s.w, p.y + s.h); }
template <typename T> constexpr auto operator-(Point<T> const &p, Size<T> const &s) { return Point<T>(p.x - s.w, p.y - s.h); }
template <typename T> constexpr auto operator%(Point<T> const &p, Size<T> const &s) { return Point<T>(p.x % s.w, p.y % s.h); }
template <typename T> constexpr auto operator/(Point<T> const &p, Size<T> const &s) { return Point<T>(p.x / s.w, p.y / s.h); }
template <typename T> constexpr auto operator*(Point<T> const &p, Size<T> const &s) { return Point<T>(p.x * s.w, p.y * s.h); }

template <typename T> constexpr auto distance(Point<T> const &p1, Point<T> const &p2) {
    return Size<T>(p1.x - p2.x, p1.y - p2.y);
}

}
#endif
