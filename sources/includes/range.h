#ifndef _range_h
#define _range_h

#include "expects.h"
#include "test.h"
#include <utility>
#include <type_traits>

namespace ec {

template<typename I = size_t>
class Range {
public:
    using Index = I;

    constexpr Range() noexcept = default;
    constexpr Range(Index begin, Index end) TESTEXCEPT : _begin(begin), _end(end) { expects(_begin <= _end); }
    constexpr Range(Index end) TESTEXCEPT : _begin(0), _end(end) { expects(_begin <= _end); }
    constexpr Range(std::pair<Index, Index> const &p) noexcept: Range(p.first, p.second) {}
    constexpr Index begin() const noexcept { return _begin; }
    constexpr Index end() const noexcept { return _end; }
    constexpr bool empty() const noexcept { return begin() == end(); }
    constexpr size_t size() const noexcept { return end() - begin(); }
    constexpr bool contains(Index i) const noexcept { return begin() <= i and end() >= i; }
    constexpr bool contains(Range const &other) const noexcept { return begin() <= other.begin() and end() >= other.end(); }
    constexpr bool is_left_of(Range const& other) const noexcept { return end() == other.begin(); /* TODO: <= ? */ }
    constexpr bool is_right_of(Range const& other) const noexcept { return begin() == other.end(); /* TODO: >= ? */ }
    constexpr Range slice(const Index b, const Index e) const {
        expects(b <= size());
        expects(e <= size());
        expects(e - b <= size());
        expects(b <= e);
        return Range(begin() + b, begin() + e);
    }
    
private:
    Index _begin = Index();
    Index _end = Index();
};

template <typename Index> constexpr bool operator<(Range<Index> const &a, Range<Index> const &b) noexcept {
    return a.begin() < b.begin();
}

template <typename Index> constexpr bool operator==(Range<Index> const &a, Range<Index> const &b) noexcept {
    return a.begin() == b.begin() and a.end() == b.end();
}

template <typename Index> constexpr Range<Index> make_range(Index begin, Index end) TESTEXCEPT {
    return Range<Index>(begin, end);
}

template <typename I> constexpr I begin(Range<I> const &r) noexcept { return r.begin(); }
template <typename I> constexpr I end(Range<I> const &r) noexcept { return r.end(); }
template <typename I> constexpr bool empty(Range<I> const &r) noexcept { return r.empty(); }
template <typename I> constexpr size_t size(Range<I> const &r) noexcept { return r.size(); }

template<typename I, typename U>
constexpr Range<I> operator / (Range<I> const& r, U div) {
    expects(div != 0);
    return Range<I>(static_cast<I>(static_cast<U>(begin(r)) / div),
                    static_cast<I>(static_cast<U>(end(r)) / div));
}

template<typename I, typename U>
constexpr Range<I> operator * (Range<I> const& r, U mul) {
    return Range<I>(static_cast<I>(static_cast<U>(begin(r)) * mul),
                    static_cast<I>(static_cast<U>(end(r)) * mul));
}

/// Returns the pair of ranges made by splitting the given range at the given
/// offset from `begin()`.
/// @param r        The range to split.
/// @param index    The index at which to split this range. Must be
///                 0 <= index <= end.
/// @return A pair of ranges, the `first` being the range left of the split
///         index (begin->index), and the `second` being the range right of the
///         split index (index->end).
template<typename I>
constexpr auto split_range(Range<I> const& r, const I index) {
    expects(index <= size(r));
    return std::make_pair(Range<I>(begin(r), begin(r) + index),
                          Range<I>(begin(r) + index, end(r)));
}

template<typename Rng>
constexpr Rng merged_ranges(Rng a, Rng b) {
    expects(not (b < a));
    if(empty(a)) {
        return b;
    } else if(empty(b)) {
        return a;
    } else if(begin(b) <= end(a)) {
        return Rng(begin(a), std::max(end(a), end(b)));
    } else {
        return Rng();
    }
}

/** Returns the given range increased by the given size, split between the left
    and right sides of the range.
 
    @param r    The range to increase in size.
 
    @param n    The discrete amount to increase the range by. This value will be
                split between the left (beginning) and right (end) sides of the
                range, starting with the left side. For example, an n of 3 will
                move the left side of the range down by two, and the right side
                of the range up by one.
 
    @param left_first   Whether the remainder of odd `n` values is added to the
                        left or right sides of the increased range. Defaults to
                        the left side:
                            `increased(r, 1) == range(r.begin()-1, r.end())`
 
    @return     A new range with the size increased by n.
                Note that this ranges bounds are always clamped above zero, but
                you must clamp upper values yourself.
*/
template<typename I>
constexpr Range<I> increased(Range<I> r, size_t n, const bool left_first=true) {
    size_t left = n/2, right = n/2, remainder = n%2;
    if(left_first) {
        left += remainder;
    } else {
        right += remainder;
    }
    return Range<I>(begin(r)-left, end(r)+right);
}

template<typename I>
constexpr Range<I> decreased(Range<I> r, size_t n, const bool left_first=true) {
    auto left = begin(r)+n/2, right = end(r)-n/2, remainder = n%2;
    if(left_first) {
        left += remainder;
    } else {
        right -= remainder;
    }
    return Range<I>(left, right);
}

template<typename I>
constexpr Range<I> offset(Range<I> r, int amt) {
    return Range<I>(begin(r)+amt, end(r)+amt);
}

template<typename I>
constexpr Range<I> clamped(Range<I> r, Range<I> limit) {
    I b = begin(r), e = end(r);
    b = b < begin(limit) ? begin(limit) : b;
    b = b > end(limit) ? end(limit) : b;
    e = e > end(limit) ? end(limit) : e;
    e = e < begin(limit) ? begin(limit) : e;
    return Range<I>(b, e);
}

template<typename I>
constexpr Range<I> clamped_increase(Range<I> r, const int arg, Range<I> limit) {
    if (std::is_unsigned<I>()) {
        // Special path with shifting
        auto temp = increased(offset(r, static_cast<int>(arg)), arg);
        temp = clamped(temp, offset(limit, static_cast<int>(arg)));
        return offset(temp, -static_cast<int>(arg));
    } else {
        clamped(increased(r, arg), limit);
    }
}

template<typename I>
constexpr Range<I> intersection(Range<I> a, Range<I> b) {
    if (end(a) <= begin(b) or end(b) <= begin(a)) {
        return Range<I>();
    } else {
        return Range<I>(std::max(begin(a), begin(b)), std::min(end(a), end(b)));
    }
}

}
#endif
