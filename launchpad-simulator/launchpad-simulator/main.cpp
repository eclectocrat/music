#include "imgui-tools/program.h"
#include "imgui-tools/grid.h"
#include "expects.h"
#include <SDL2/SDL.h>
#include <RtMidi.h>
#include <range/v3/all.hpp>
#include <iostream>

using namespace std;
using namespace ec;
using namespace ec::imgui;

extern "C" void app_init(const uint16_t *adc_buffer);
extern "C" void app_timer_event(void);
extern "C" void app_midi_event(uint8_t port, uint8_t status, uint8_t d1, uint8_t d2);
extern "C" void app_sysex_event(uint8_t port, uint8_t *data, uint16_t count);
extern "C" void app_cable_event(uint8_t type, uint8_t value);
extern "C" void app_surface_event(uint8_t type, uint8_t index, uint8_t value);
extern "C" void app_aftertouch_event(uint8_t index, uint8_t value);

constexpr size_t converted_index(Expects_within<size_t, 1, 99> i) {
    expects(i > 0);
    return ((9 - i/10) * 10) + i % 10;
}

struct Launchpad_window: Grid_window {
private:
    enum class Pad_group {
        top, left, right, bottom, grid, group
    };
    constexpr Pad_group pad_group_for_raw_index(const uint8_t index) {
        if(index > 89) {
            return Pad_group::bottom;
        } else if(index < 10) {
            return Pad_group::top;
        } else if(index % 10 == 0) {
            return Pad_group::left;
        } else if(index % 10 == 9) {
            return Pad_group::right;
        } else {
            return Pad_group::grid;
        }
    }

public:
    Launchpad_window(std::string const &name_, Wrect const &frame_,
                     Size<size_t> grid_size_)
        : Grid_window(name_, frame_, grid_size_, true) {
        pads_colors.resize(100);
        ranges::fill(pads_colors, ImVec4(.5, .5, .5, 1));
    }
   
private:
    void pad_was_pressed(Pad_index const& index) override {
        const auto idx = converted_index(index.flat_index);
        cout << "pad " << idx << " down" << endl;
        app_surface_event(0, idx, 1);
    }
    
    void pad_was_released(Pad_index const& index) override {
        const auto idx = converted_index(index.flat_index);
        cout << "pad " << idx << " up" << endl;
        app_surface_event(0, idx, 0);
    }

    void render_pad(Pad_index const &index, const ImRect bb,
                    Pad_state const &state) override {
        using namespace ImGui;
        static const size_t skip_pads[] = {0, 9, 90, 99};
        if (find(begin(skip_pads), end(skip_pads), index.flat_index) !=
            end(skip_pads)) {
            return;
        }
        const auto c = GetColorU32(pads_colors[index.flat_index]);
        if (auto pad_group = pad_group_for_raw_index(index.flat_index);
            pad_group != Pad_group::grid) {
            auto min = bb.Min;
            auto max = bb.Max;
            min.x += 10;
            min.y += 10;
            max.x -= 10;
            max.y -= 10;
            RenderFrame(min, max, c, true, 30);
        } else {
            RenderFrame(bb.Min, bb.Max, c, 10);
        }
    }

public:
    vector<ImVec4> pads_colors;
};

// Globals
Launchpad_window * launchpad = nullptr;
RtMidiIn * midi_in = nullptr;
RtMidiOut * midi_out = nullptr;

extern "C" void hal_plot_led(uint8_t type, uint8_t index, uint8_t red,
                             uint8_t green, uint8_t blue) {
    expects(index > 0);
    expects(index < 99);
    launchpad->pads_colors[converted_index(index)] =
        ImVec4(float(red) / 64.f, float(green) / 64.f, float(blue) / 64.f, 1.0);
}

extern "C" void hal_send_midi(uint8_t port, uint8_t status, uint8_t data1,
                              uint8_t data2) {
    static vector<unsigned char> vec;
    vec.resize(3);
    vec[0] = status;
    vec[1] = data1;
    vec[2] = data2;
    midi_out->sendMessage(&vec);
}

extern "C" void hal_send_sysex(uint8_t port, const uint8_t *data,
                               uint16_t length) {
    expects(false and "unimplemented");
}

extern "C" void hal_read_flash(uint32_t offset, uint8_t *data,
                               uint32_t length) {
    expects(false and "unimplemented");
}

extern "C" void hal_write_flash(uint32_t offset, const uint8_t *data,
                                uint32_t length) {
    expects(false and "unimplemented");
}

Uint32 tick_clock(Uint32, void*) {
    app_timer_event();
    return 1;
}

void midi_callback(double time_stamp, std::vector<unsigned char> *message,
                   void *) {
    expects(message);
    expects(message->size() >= 3);
    const auto status = message->front();
    app_midi_event(status & 0x0F, status, (*message)[1], (*message)[2]);
}

int main(int argc, char ** argv) {
    try {
    // Setup MIDI
        midi_in = new RtMidiIn;
        midi_in->setCallback(midi_callback);
        midi_in->openVirtualPort();
        midi_out = new RtMidiOut;
        midi_out->openVirtualPort();
        
   // Setup UI
        launchpad = new Launchpad_window("launchpad_grid", Wrect(0, 0, 800, 800).inset(Wsize(10, 10)), Size<size_t>(10, 10));
        launchpad->set_is_moveable(false);

        Imgui_program prog;
        prog.run_windows = [&] { launchpad->run(); };
        prog.cleanup = [&] {
            midi_in->closePort();
            midi_out->closePort();
        };
        
    // Setup launchpad API
        uint16_t adc[256];
        app_init(adc);
        SDL_AddTimer(1, tick_clock, nullptr);
    
    // Run program
        return imgui_program_main(prog, argc, argv);
    
    // Handle errors
    } catch (RtMidiError &error) {
        error.printMessage();
    } catch (...) {
        cout << "Unknown exception, aborting..." << endl;
    }
    return -1;
}

