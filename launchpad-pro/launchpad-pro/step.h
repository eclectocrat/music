#ifndef step_h
#define step_h

#include <array>
#include "app_defs.h"
#include "constexpr_algo.h"

namespace ec {

struct Step {
    constexpr Step() = default;
    Step(const Step&) = default;
    Step& operator=(const Step&) = default;
    
    static constexpr Byte midi_buffer_size = 6;
    static constexpr Byte midi_byte_slide = 0xff;
    typedef std::array<Byte, midi_buffer_size> Byte_array;
    
    Byte_array midi_buffer = Byte_array();

    Byte& operator[](Byte i) { return midi_buffer[i]; }
    Byte const& operator[](Byte i) const { return midi_buffer[i]; }
    void clear() { algo::fill(midi_buffer, 0); }
    bool is_empty() const { return midi_buffer[0] == 0; }
    
    Byte note() const { return midi_buffer[0]; }
    Byte velocity() const { return midi_buffer[1]; }
    Byte gate() const { return midi_buffer[2]; }
    
    // poly versions
    Byte note(const Byte voice) const { return midi_buffer[0] & (0x1 << voice); }
    Byte velocity(const Byte voice) const {
        return voice % 2 == 1 ? (midi_buffer[1 + voice / 2] & 0xF0) >> 4
                              :  midi_buffer[1 + voice / 2] & 0x0F;
    }
    void set_velocity(Byte voice, Byte vel) {
        midi_buffer[1 + voice / 2] =
            voice % 2 == 1 ? (midi_buffer[1 + voice / 2] & 0x0F) | ((vel & 0x0F) << 4)
                           : (midi_buffer[1 + voice / 2] & 0xF0) | (vel & 0x0F);
    }
};

}
#endif
