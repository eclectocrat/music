#ifndef modal_midi_port_h
#define modal_midi_port_h

#include "modal.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_midi_port: public Modal {
public:
    void activate() override {
        Modal::activate();
        _color = CUR_SEQ->base_color;
    }

    void deactivate() override {
        Modal::deactivate();
        APP->arrow_up_pad.draw();
        APP->arrow_down_pad.draw();
        APP->arrow_right_pad.draw();
        APP->arrow_left_pad.draw();
    }

    void display() override {
        algo::for_each(*PADS, [](Pad& p) {
            p.set_color(no_color);
            p.draw();
        });
        if(_io_dir == Midi_port_dir::midi_in) {
            APP->arrow_up_pad.draw_clear_color();
            APP->arrow_down_pad.draw_color(green_color);
        } else {
            APP->arrow_down_pad.draw_clear_color();
            APP->arrow_up_pad.draw_color(green_color);
        }
        switch(_port) {
        case Midi_port::usb_port: {
            plot_U(1, _color);
        } break;
        case Midi_port::standalone_port: {
            plot_S(1, _color);
        } break;
        case Midi_port::din_port: {
            plot_M(1, _color);
        } break;
        default: break;
        }
    }

    void handle_top_button(Pad_raw_index event, Byte value) override {
        if (value) {
            if (event == Pad_raw_index::arrow_up and
                _io_dir != Midi_port_dir::midi_out) {
                _io_dir = Midi_port_dir::midi_out;
            } else if (event == Pad_raw_index::arrow_down and
                       _io_dir != Midi_port_dir::midi_in) {
                _io_dir = Midi_port_dir::midi_in;
            } else if (event == Pad_raw_index::arrow_right) {
                Byte n = static_cast<Byte>(_port);
                _port = ++n >= static_cast<Byte>(Midi_port::no_port)
                           ? Midi_port::standalone_port
                           : static_cast<Midi_port>(n);
                APP->arrow_right_pad.draw_color(green_color);
            } else if (event == Pad_raw_index::arrow_left) {
                Byte n = static_cast<Byte>(_port);
                _port =
                    n < 1 ? Midi_port::din_port : static_cast<Midi_port>(--n);
                APP->arrow_left_pad.draw_color(green_color);
            } else {
                Modal::handle_top_button(event, value);
            }
        } else {
            if (event == Pad_raw_index::arrow_up and
                _io_dir != Midi_port_dir::midi_out) {
                if (_io_dir != Midi_port_dir::midi_out) {
                    APP->arrow_up_pad.draw();
                }
            } else if (event == Pad_raw_index::arrow_down and
                       _io_dir != Midi_port_dir::midi_in) {
                if (_io_dir != Midi_port_dir::midi_in) {
                    APP->arrow_down_pad.draw();
                }
            } else if (event == Pad_raw_index::arrow_right) {
                APP->arrow_right_pad.draw();
            } else if (event == Pad_raw_index::arrow_left) {
                APP->arrow_left_pad.draw();
            } else {
                Modal::handle_top_button(event, value);
            }
        }
    }
    
protected:
    Midi_port       _port    = Midi_port::din_port;
    Midi_port_dir   _io_dir  = Midi_port_dir::midi_out;
    Color_code      _color   = no_color;
};

} // END namespace

#endif /* modal_midi_port_h */
