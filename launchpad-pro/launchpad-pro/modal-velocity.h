#ifndef modal_velocity_h
#define modal_velocity_h

#include "modal-transport.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_velocity: public Modal_transport {
public:
    void activate() override {
        APP->velocity_pad.draw_color(green_color);
        Modal_transport::activate();
    }
    
    void deactivate() override {
        APP->velocity_pad.draw_color(no_color);
        Modal_transport::deactivate();
    }
    
    void display() override {
        algo::for_each(*PADS, [](Pad& p) { p.set_color(no_color); });
        draw_velocity(CUR_SEQ->view, 0, *PADS, leftmost_step(),
                      CUR_SEQ->base_color);
        Modal_transport::display();
        algo::for_each(*PADS, [](Pad& p) { p.draw(); });
    }

    void handle_pad_grid(Pad_index pad_index, Byte value) override {
        if (not value) {
            return;
        }
        //const Byte pad_index = pads().pad_index_from_raw_index(index);
        const Byte velocity = level_for_pad(pad_index);
        if (APP->shift_pad.touch_state()) {
            for (auto& step: *CUR_SEQ) {
                step[1] = velocity;
            }
            display();
        } else {
            const Byte step_index = leftmost_step() + step_for_pad(pad_index);
            auto &step = CUR_SEQ[step_index];
            if (step.note()) {
                step[1] = velocity;
                const Byte pad_x = step_index - leftmost_step();
                draw_vbar(*PADS, pad_x, velocity/16, CUR_SEQ->base_color);
                for_column(*PADS, Pad_index(pad_x + Pad_grid::row_width),
                           [](Pad &p) { p.draw(); });
            }
        }
    }
};

} // END namespace
#endif // modal_velocity_h
