#ifndef modal_transport_h
#define modal_transport_h

#include "modal.h"
#include "flash.h"
#include "draw_step_sequence.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_transport: public Modal {
public:
    void activate() override {
        _last_drawn_step = 0;
        if(APP->follow) {
            const auto step = CUR_SEQ->calculate_step(SEQUENCER->clock());
            _leftmost_step = (step / Pad_grid::row_width) * Pad_grid::row_width;
        }
        _step_flash.reset();
        APP->follow_pad.draw_color(APP->follow ? green_color : no_color);
        Modal::activate();
    }
    
    void deactivate() override {
        APP->follow_pad.draw_clear_color();
        Modal::deactivate();
    }
    
    void did_step(Clock_t clock) override {
        const Byte step = CUR_SEQ->calculate_step(clock);
        const Byte prev_step = CUR_SEQ->calculate_step(clock-1);
        if(step != prev_step) {
            if (APP->follow and
                (step < _leftmost_step or
                 step >= _leftmost_step + Pad_grid::row_width)) {
                _leftmost_step =
                    (step / Pad_grid::row_width) * Pad_grid::row_width;
                display();
            } else {
                draw_active_step(step);
            }
        }
        _step_flash.tick_flash([&]{
            _flashing_step = (step / Pad_grid::row_width) + 1;
            PADS[Pad_index(_flashing_step-1)].draw_color(white_color);
        }, [&]{
            PADS[Pad_index(_flashing_step-1)].draw();
        });
    }
    
    void display() override {
        draw_active_step(CUR_SEQ->calculate_step(SEQUENCER->clock()));
        draw_transport(*CUR_SEQ, *PADS, leftmost_step(), yellow_light_color,
                       yellow_color, red_light_color, red_color);
    }
    
    void handle_top_button(Pad_raw_index event, Byte value) override {
        switch(event) {
        case Pad_raw_index::arrow_left: {
            if(not value) {
                APP->arrow_left_pad.draw_clear_color();
            } else if(_leftmost_step > 0) {
                APP->arrow_left_pad.draw_color(green_color);
                _leftmost_step--;
                set_follow(false);
                display();
            }
        } break;
        case Pad_raw_index::arrow_right: {
            if(not value) {
                APP->arrow_right_pad.draw_clear_color();
            } else if(_leftmost_step < PADS->size() - Pad_grid::row_width) {
                APP->arrow_right_pad.draw_color(green_color);
                _leftmost_step++;
                set_follow(false);
                display();
            }
        } break;
        default:
            Modal::handle_top_button(event, value);
        }
    }
    
    void handle_left_button(Pad_raw_index index, Byte value) override {
        if(index == Pad_raw_index::follow and value) {
            set_follow(not APP->follow);
        } else {
            Modal::handle_left_button(index, value);
        }
    }
    
    void handle_pad(Pad_index index, Byte value) override {
        if(value and index.weak() >= 0 and index.weak() < 8) { // transport pads
            const Byte new_leftmost = index.weak() * Pad_grid::row_width;
            if(new_leftmost != _leftmost_step) {
                _leftmost_step = new_leftmost;
                if(APP->follow) {
                    set_follow(false);
                }
                display();
            }
        } else {
            handle_pad_grid(index, value);
        }
    }
    
    virtual void handle_pad_grid(Pad_index, Byte) {
        // subclasses implement
    }
    
protected:
    constexpr Byte note_for_pad(Pad_index pad_index) const {
        return (8 - pad_index.weak()/8);
    }
    
    constexpr Byte step_for_pad(Pad_index pad_index) const {
        return pad_index.weak() % 8;
    }
    
    Pad& pad_for_step_note(Byte step, Byte note) {
        Byte pad_i = (Pad_grid::row_width - note) * Pad_grid::row_width + step;
        expects(pad_i < pad_count);
        return PADS[Pad_index(pad_i)];
    }
    
    
    inline Pad* pad_for_step(Byte step) {
        if(const Byte note = CUR_SEQ[_leftmost_step+step][0]) {
            return &pad_for_step_note(step, note);
        } else {
            return nullptr;
        }
    }
    
    constexpr Byte level_for_pad(Pad_index pad) {
        return (8 - pad.weak()/8) * 16;
    }

    void set_follow(bool b) {
        APP->follow = b;
        APP->follow_pad.draw_color(b ? green_color : no_color);
    }
    
    void draw_active_step(Byte step) {
        // refresh last position
        for(int y=1; y<Pad_grid::row_width; ++y) {
            const Pad_index index(y*Pad_grid::row_width + _last_drawn_step);
            PADS[index].draw();
        }
        if (step < _leftmost_step or
            step >= _leftmost_step + Pad_grid::row_width) {
            return;
        }
        // draw new position
        const Byte x = step - _leftmost_step;
        for(int y=1; y<Pad_grid::column_height; ++y) {
            const Pad_index index(y*Pad_grid::row_width + x);
            PADS[index].draw_color(white_color); // TODO
        }
        // save for later
        _last_drawn_step = x;
    }
    
    constexpr Byte leftmost_step() const { return _leftmost_step; }
    
private:
    Byte _last_drawn_step = 0;
    Byte _leftmost_step = 0;
    Flash_action _step_flash;
    Byte _flashing_step = 0;
};

} // END namespace

#endif /* modal_transport_h */
