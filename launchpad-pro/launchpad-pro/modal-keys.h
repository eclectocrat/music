#ifndef modal_keys_h
#define modal_keys_h

#include "modal.h"
#include "constexpr_algo.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_keys: public Modal {
public:
    void activate() override {
        Modal::activate();
        _last_drawn_step = Pad_index(0);
        _base_note = 22;
        algo::fill(_held_notes, 0);
        APP->keys_pad.draw_color(green_color);
    }
    
    void deactivate() override {
        Modal::deactivate();
        APP->keys_pad.draw_clear_color();
        algo::for_each(*PADS, [](Pad& p) { p.clear_flags(); });
        algo::for_each(_held_notes, [&](Byte note) {
            if (note) {
                hal_send_midi(Byte(CUR_SEQ->out_port),
                              NOTEOFF | CUR_SEQ->channel, note, 127);
            }
        });
    }
    
    void did_step(Clock_t clock) override {
        const auto step = CUR_SEQ->calculate_step(clock);
        const auto prev_step = CUR_SEQ->calculate_step(clock-1);
        if(step != prev_step) {
            draw_active_step(step);
        }
    }
    
    void display() override {
        generate_key_pads(CUR_SEQ->base_color);
        algo::for_each(*PADS, [](Pad& p) { p.draw(); });
        draw_active_step(CUR_SEQ->calculate_step(SEQUENCER->clock()));
    }
    
    void handle_top_button(Pad_raw_index event, Byte value) override {
        switch(event) {
        case Pad_raw_index::arrow_left: {
            if(not value) {
                APP->arrow_left_pad.draw_clear_color();
            } else if(_base_note > 1) {
                APP->arrow_left_pad.draw_color(green_color);
                _base_note--;
                display();
            }
        } break;
        case Pad_raw_index::arrow_right: {
            if(not value) {
                APP->arrow_right_pad.draw_clear_color();
            } else if(_base_note + PADS->size() < 110) {
                APP->arrow_right_pad.draw_color(green_color);
                _base_note++;
                display();
            }
        } break;
        default:
            Modal::handle_top_button(event, value);
        }
    }
    
    void handle_pad(Pad_index pad_index, Byte value) override {
        pad_index = Pad_index(Pad_grid::row_width *
                                  ((Pad_grid::row_width - 1) -
                                   pad_index.weak() / Pad_grid::row_width) +
                              (pad_index.weak() % Pad_grid::row_width));
        const Byte note = pad_index.weak() + _base_note;
        auto& pad = pad_for_note(note);
        if (value) {
            did_touch_note(note, pad);
        } else {
            hal_send_midi(Byte(CUR_SEQ->out_port), NOTEOFF | CUR_SEQ->channel,
                          note, 127);
            pad.draw();
            auto i = std::find(_held_notes.begin(), _held_notes.end(), note);
            if (i != _held_notes.end()) {
                *i = 0;
            }
        }
    }
    
protected:
    Byte base_note() const { return _base_note; }
    
    virtual void did_touch_note(Byte note, Pad& pad) {
        auto i = algo::find(_held_notes, [](auto n) { return not n; });
        if (i != _held_notes.end()) {
            *i = note;
        } else {
            return;
        }
        hal_send_midi(Byte(CUR_SEQ->out_port), NOTEON | CUR_SEQ->channel, note, 127);
        pad.draw_color(white_color);
    }
    
    virtual void generate_key_pads(const Color_code col) {
        Byte note = _base_note;
        const Color_code light_color = paired_color(col);
        for(int y=Pad_grid::column_height-1; y >= 0; --y) {
            for(int x=0; x < Pad_grid::row_width; ++x) {
                auto& pad = PADS[Pad_index(y * Pad_grid::row_width + x)];
                pad.set_flags(note++);
                pad.set_color(dark_color_for_note(note) ? col : light_color);
            }
        }
    }
    
    void draw_active_step(Byte step_index) {
        const auto& step = CUR_SEQ[step_index];
        PADS[_last_drawn_step].draw();
        if(not CUR_SEQ->mute) {
            if (step[0] >= _base_note and step[0] < _base_note + PADS->size()) {
                _last_drawn_step = Pad_index(step[0] - _base_note);
                PADS[_last_drawn_step].draw_color(white_color);
            }
        }
    }
    
    Pad& pad_for_note(Byte note) {
        const Byte n = (note - _base_note);
        const Byte y = (Pad_grid::column_height-1) - (n / Pad_grid::row_width);
        const Byte x = n % Pad_grid::row_width;
        return PADS[Pad_index(y * Pad_grid::row_width + x)];
    }
    
    bool dark_color_for_note(Byte note) {
        switch(note % 12) {
        case 1: case 3: case 6: case 8: case 10:
            return true;
        default:
            return false;
        }
    }
    
private:
    Pad_index _last_drawn_step{0};
    Byte _base_note = 22;
    std::array<Byte, 8> _held_notes;
};

} // END namespace
#endif /* modal_keys_h */
