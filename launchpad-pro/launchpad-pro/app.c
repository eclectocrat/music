/******************************************************************************
 
 Copyright (c) 2015, Focusrite Audio Engineering Ltd.
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 * Neither the name of Focusrite Audio Engineering Ltd., nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 *****************************************************************************/

//______________________________________________________________________________
//
// Headers
//______________________________________________________________________________

#include "app.h"

extern void cpp_app_surface_event(u8 type, u8 index, u8 value);
extern void cpp_app_midi_event(u8 port, u8 status, u8 d1, u8 d2);
extern void cpp_app_sysex_event(u8 port, u8 *data, u16 count);
extern void cpp_app_aftertouch_event(u8 index, u8 value);
extern void cpp_app_cable_event(u8 type, u8 value);
extern void cpp_app_timer_event(void);
extern void cpp_app_init(const u16 *adc_raw);

//______________________________________________________________________________

void app_surface_event(u8 type, u8 index, u8 value) {
    cpp_app_surface_event(type, index, value);
}

void app_midi_event(u8 port, u8 status, u8 d1, u8 d2) {
    cpp_app_midi_event(port, status, d1, d2);
}

void app_sysex_event(u8 port, u8 * data, u16 count) {
    cpp_app_sysex_event(port, data, count);
}

void app_aftertouch_event(u8 index, u8 value) {
    cpp_app_aftertouch_event(index, value);
}

void app_cable_event(u8 type, u8 value) {
    cpp_app_cable_event(type, value);
}

void app_timer_event() {
    cpp_app_timer_event();
}

void app_init(const u16 *adc_raw) {
    cpp_app_init(adc_raw);

// DEBUG
//    for (int i=0; i < 10; ++i) {
//        for (int j=0; j < 10; ++j) {
//            hal_plot_led(TYPEPAD, j*10 + i, 0, 0, MAXLED);
//        }
//    }
}
