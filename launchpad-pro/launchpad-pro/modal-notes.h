#ifndef modal_notes_h
#define modal_notes_h

#include "modal-transport.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_notes: public Modal_transport {
public:
    void activate() override {
        APP->notes_pad.draw_color(green_color);
        Modal_transport::activate();
    }
    
    void deactivate() override {
        APP->notes_pad.draw_clear_color();
        Modal_transport::deactivate();
    }

    void display() override {
        algo::for_each(*PADS, [](Pad& p) { p.set_color(no_color); });
        draw_note_mono(CUR_SEQ->view, *PADS, leftmost_step(), CUR_SEQ->base_color);
        Modal_transport::display();
        algo::for_each(*PADS, [](Pad& p) { p.draw(); });
    }
    
    void handle_pad_grid(Pad_index index, Byte value) override {
        if(not value) {
            return;
        }
        const Byte step = leftmost_step() + step_for_pad(index);
        const Byte note = note_for_pad(index);
        if(CUR_SEQ[step][0] == note) {
            CUR_SEQ[step].clear();
        } else {
            if(auto old_pad = pad_for_step(step - leftmost_step())) {
                old_pad->draw_clear_color();
            }
            auto& s = CUR_SEQ[step];
            s[0] = note;
            s[1] = 127; // velocity
            s[2] = CUR_SEQ->default_gate_ticks;
        }
        display();
    }
    
    void exec_prog(Random_prog const& prog) override {
        CUR_SEQ->generate_prog(prog);
        display();
    }
};

} // END namespace

#endif /* modal_notes_h */
