#ifndef modal_seed32_h
#define modal_seed32_h

#include "modal.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_seed32: public Modal {
public:
    void activate() override {
        Modal::activate();
        APP->random_pad.draw_color(green_color);
    }
    
    void deactivate() override {
        Modal::deactivate();
        APP->random_pad.draw_clear_color();
    }
    
    void display() override {
        display_bits();
        algo::for_each(PADS->begin()+32, PADS->end(), [](Pad& p) {
            p.set_color(no_color);
        });
        // TODO: Don't hardcode numbers
        const auto bar_color = paired_color(CUR_SEQ->base_color);
        algo::for_each(PADS->begin() + 32,
                       PADS->begin() + 32 + APP->program.param1,
                       [&](Pad &p) { p.set_color(bar_color); });
        algo::for_each(PADS->begin() + 40,
                       PADS->begin() + 40 + APP->program.param2,
                       [&](Pad &p) { p.set_color(bar_color); });
        algo::for_each(PADS->begin() + 48,
                       PADS->begin() + 48 + APP->program.param3,
                       [&](Pad &p) { p.set_color(bar_color); });
        algo::for_each(PADS->begin() + 56,
                       PADS->begin() + 56 + APP->program.param4,
                       [&](Pad &p) { p.set_color(bar_color); });
        algo::for_each(PADS->begin()+32, PADS->end(), [](Pad& p) {
            p.draw();
        });
    }
    
    void handle_pad(Pad_index index, Byte value) override {
        if(index.weak() >= 32 and value) {
            if(index.weak() >= 56) {
                APP->program.param4 = Byte(index) - 55;
            } else if(index.weak() >= 48) {
                APP->program.param3 = Byte(index) - 47;
            } else if(index.weak() >= 40) {
                APP->program.param2 = Byte(index) - 39;
            } else {
                APP->program.param1 = Byte(index) - 31;
            }
            display();
        } else if(value) {
            APP->program.seed ^= (0x00000001 << Byte(index));
            display_bits();
            seed_random(APP->program.seed);
        }
    }
    
    void display_bits() {
        for(int i=0; i<32; ++i) {
            PADS[Pad_index(i)].draw_color(
                ((0x00000001 << i) & APP->program.seed) ? CUR_SEQ->base_color
                                                        : no_color);
        }
    }
};

} // END namespace
#endif /* modal_seed32_h */
