//
//  pad.h
//  launchpad-sim-macOS
//
//  Created by Jeremy Jurksztowicz on 2/19/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef pad_h
#define pad_h

#include "app.h"
#include "color.h"
#include "strong_type.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
struct Raw_index: Strong_type<Raw_index, Byte> {
    using Strong_type::Strong_type;
};

struct Pad_index: Strong_type<Pad_index, Byte> {
    using Strong_type::Strong_type;
};

struct Touch_state: Strong_type<Touch_state, Byte> {
    using Strong_type::Strong_type;
    constexpr operator bool() const noexcept { return weak(); }
};

enum Pad_flags {
    pad_enabled     = 0x01,
    pad_flashing    = 0x02
};
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Pad {
public:
    friend class Pad_grid;
    
    Pad() = default;
    explicit Pad(Raw_index pad): _raw_index(pad) {}
    
    inline void draw_color(Color c) {
        expects(_raw_index.weak() != 0);
        expects(_raw_index.weak() < 99);
        hal_plot_led(TYPEPAD, _raw_index.weak(), c.r, c.g, c.b);
    }
    inline void draw_color(Color_code c) {
        draw_color(color_for_code(c));
    }
    inline void draw() {
        draw_color(_color);
    }
    inline void draw_clear_color() {
        draw_color(no_color);
    }
    inline void draw_flash_color() {
        draw_color(_flash_color);
    }
    
    constexpr void set_color(Color_code c) {
        _color = c;
    }
    constexpr Color_code color() const {
        return _color;
    }
    constexpr void set_flash_color(Color_code c) {
        _flash_color = c;
    }
 
    constexpr void touch_down(Touch_state state=Touch_state(1)) {
        _touch_state = state;
    }
    constexpr void touch_up() {
        _touch_state = Touch_state(0);
    }
    constexpr Touch_state toggle_touch_state() {
        _touch_state = _touch_state.weak() ? Touch_state(0) : Touch_state(1);
        return _touch_state;
    }
    constexpr Touch_state touch_state() const {
        return _touch_state;
    }
    
    constexpr void set_raw_index(Raw_index i) {
        _raw_index = i;
    }
    constexpr Raw_index raw_index() const {
        return _raw_index;
    }
    
    constexpr bool has_flag(Byte flag) const {
        return _flags & flag;
    }
    
    constexpr void clear_flags() {
        _flags = 0;
    }
 
    constexpr void clear_flag(Byte mask) {
        _flags ^= mask;
    }
    
    constexpr void set_flags(Byte flags) {
        _flags = flags;
    }
 
    constexpr void set_flag(Byte mask) {
        _flags |= mask;
    }
    
private:
    Byte            _flags       = 0;
    Raw_index       _raw_index   = Raw_index(0);
    Touch_state     _touch_state = Touch_state(0);
    Color_code      _color       = no_color;
    Color_code      _flash_color = no_color;
};

} // END namespace
#endif // _pad_h
