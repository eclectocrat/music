#ifndef modal_swing_h
#define modal_swing_h

#include "modal-edit-number.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_swing: public Modal_edit_number {
public:
    void activate() override {
        Modal_edit_number::activate();
        APP->timing_pad.draw_color(green_color);
    }
    
    void deactivate() override {
        Modal_edit_number::deactivate();
        APP->timing_pad.draw_clear_color();
    }

    void display() override {
        display_number(CUR_SEQ->swing);
    }
    
    void handle_top_button(Pad_raw_index event, u8 value) override {
        switch(event) {
        case Pad_raw_index::arrow_up:
            if (value and CUR_SEQ->swing < 6) {
                CUR_SEQ->swing++;
                APP->arrow_up_pad.draw_color(green_color);
                display();
            } else {
                APP->arrow_up_pad.draw_clear_color();
            }
            break;
        case Pad_raw_index::arrow_down:
            if (value and CUR_SEQ->swing > 0) {
                CUR_SEQ->swing--;
                APP->arrow_down_pad.draw_color(green_color);
                display();
            } else {
                APP->arrow_down_pad.draw_clear_color();
            }
            break;
        default: break;
        }
    }
};

} // END namespace

#endif /* modal_swing_h */
