#ifndef _color_h
#define _color_h

#include "app_defs.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
enum Color_code {
    no_color = 0,
    
    red_color,
    red_light_color,
    
    green_color,
    green_light_color,
    
    darkgreen_color,
    darkgreen_light_color,
    
    blue_color,
    blue_light_color,
    
    pink_color,
    pink_light_color,
    
    magenta_color,
    magenta_light_color,
    
    turquoise_color,
    turquoise_light_color,
    
    darkturquoise_color,
    darkturquoise_light_color,
    
    purple_color,
    purple_light_color,
    
    orange_color,
    orange_light_color,
    
    yellow_color,
    yellow_light_color,
    
    yellow_off_color,
    yellow_pale_light_color,
    
    white_color,
    indigo_color,
    
    color_count
};
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
struct Color {
    Byte r = 0;
    Byte g = 0;
    Byte b = 0;
    
    constexpr Color() = default;
    constexpr Color(Byte r_, Byte g_, Byte b_): r(r_), g(g_), b(b_) {}
};
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static constexpr std::array<Color, color_count> color_map = {
#if DEBUG
    Color(32,       32,     32),
#else
    Color(),
#endif
    Color(40,       0,      0),
    Color(MAXLED,   20,     20),
    Color(0,        40,     0),
    Color(20,       MAXLED, 20),
    Color(30,       50,     0),
    Color(50,       MAXLED, 20),
    Color(0,        0,      40),
    Color(10,       10,     MAXLED),
    Color(MAXLED,   10,     20),
    Color(MAXLED,   30,     40),
    Color(50,       15,     0),
    Color(MAXLED,   30,     10),
    Color(40,       0,      40),
    Color(MAXLED,   20,     MAXLED),
    Color(0,        40,     MAXLED),
    Color(20,       40,     MAXLED),
    Color(5,        45,     25),
    Color(25,       MAXLED, 60),
    Color(MAXLED,   MAXLED, 0),
    Color(MAXLED,   MAXLED, 30),
    Color(20,       20,     0),
    Color(MAXLED,   47,     7),
    Color(20,       0,      40),
    Color(40,       20,     MAXLED),
    Color(MAXLED,   MAXLED, MAXLED),
    Color(0,        1,      0)};
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static constexpr Color const& color_for_code(Color_code code) {
    expects(code < color_count);
    return color_map[code];
}

static constexpr Color_code paired_color(Color_code code) {
    expects(code < color_count);
    return Color_code(code == 0 or code >= white_color ? code : (
        code % 2 == 1 ? code + 1 : code - 1
    ));
}

} // END namespace
#endif // _color_h
