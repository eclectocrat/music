#ifndef application_h
#define application_h

#include "app.h"
#include "pad_grid.h"
#include "plot.h"
#include "random.h"
#include "rprog.h"
#include "flash.h"
#include "sequence.h"
#include "sequencer.h"

namespace ec {

enum class Pad_raw_index: Byte {
    arrow_up      = 91,
    arrow_down    = 92,
    arrow_left    = 93,
    arrow_right   = 94,
    
    session_mode  = 95,
    note_mode     = 96,
    drums_mode    = 97,
    user_mode     = 98,
    
    steps         = 1,
    notes         = 2,
    velocity      = 3,
    gate          = 4,
    keys          = 5,
    timing        = 7,
    random        = 8,
    midi          = 6,
    
    shift         = 80,
    clock         = 70,
    play          = 60,
    follow        = 50,
    mute          = 40,
    delete_       = 30,
    copy          = 20,
    error         = 10,
};

class Modal;
struct Application {
    Application() = default;
    Application(const u16 *adc): raw_ADC(adc) { init(); }
    void init();
    
    enum Display_mode {
        display_mode_steps = 1,
        display_mode_notes,
        display_mode_velocity,
        display_mode_gate,
        display_mode_keys_select,
        display_mode_midi_channel,
        display_mode_clock_division,
        display_mode_random,
        display_mode_keys,
        display_mode_clock_port,
        display_mode_midi_port,
        display_mode_swing,
        display_mode_clock,
        display_mode_drums_transport,
        count
    };
    
    enum Clock_mode {
        internal = 0x01,
        external = 0x02
    };
    
    const u16 * raw_ADC = 0;

    // buttons and pads
    Pad_grid pads;
    
#define EC_PAD(name) Pad name##_pad { Raw_index(Byte(Pad_raw_index::name)) }
    EC_PAD(arrow_up);
    EC_PAD(arrow_down);
    EC_PAD(arrow_left);
    EC_PAD(arrow_right);
    EC_PAD(drums_mode);

    EC_PAD(steps);
    EC_PAD(notes);
    EC_PAD(velocity);
    EC_PAD(gate);
    EC_PAD(keys);
    EC_PAD(timing);
    EC_PAD(random);
    EC_PAD(midi);
    
    EC_PAD(shift);
    EC_PAD(clock);
    EC_PAD(play);
    EC_PAD(follow);
    EC_PAD(mute);
    Pad delete_pad { Raw_index(Byte(Pad_raw_index::delete_)) };
    EC_PAD(copy);
    EC_PAD(error);
#undef EC_PAD

    std::array<Pad, Sequencer::sequence_count> track_pads;
    
    Flash_action clock_pad_flash;
    Random_prog program;
    
    // MIDI and sequencer
    Sequencer       sequencer;
    u8              midi_clock_thru         = true;
    u8              midi_clock_in_port      = USBSTANDALONE;
    s8              clock_ms                = 20;
    Clock_mode      clock_mode              = internal;
    u32             clock_internal          = 0;
    
    // display state
    Display_mode    prev_display_mode       = display_mode_steps;
    Display_mode    current_display_mode    = display_mode_steps;
    u8              current_sequence_index  = 0;
    u8              prev_sequence_index     = 0;
    Modal *         modal                   = nullptr;
    bool            follow                  = true;
    
    void set_display_mode(Display_mode mode, bool deac=true);
    Sequence& current_sequence();
    Sequence& sequence_at(u8);
    void select_sequence_at(u8);
    void select_drum_track_at(u8);
    void dispatch_right_group_button(u8 index, u8 value);
    void dispatch_left_group_button(u8 index, u8 value);
    void dispatch_top_group_button(u8 index, u8 value);
    void dispatch_bottom_group_button(u8 index, u8 value);
    void dispatch_pad(u8 index, u8 value);
    void handle_clock_tick(u8 port);
    void draw_notes_tracks();
    void draw_drums_tracks();
};

static Application app;
struct App_t {
    Application *operator->() { return &app; }
    Application& operator*() { return app; }
}; App_t APP;
struct Sequencer_t {
    Sequencer *operator->() { return &app.sequencer; }
    Sequencer& operator*() { return app.sequencer; }
}; Sequencer_t SEQUENCER;
struct Current_sequencer_t {
    inline Sequence *operator->() { return &app.current_sequence(); }
    inline Sequence& operator*() { return app.current_sequence(); }
    inline Step& operator[] (Byte n) { return app.current_sequence()[n]; }
    inline Step const& operator[] (Byte n) const { return app.current_sequence()[n]; }
}; Current_sequencer_t CUR_SEQ;
struct Pads_t {
    Pad_grid *operator->() { return &app.pads; }
    Pad_grid& operator*() { return app.pads; }
    Pad& operator[] (Pad_index n) { return app.pads[n]; }
    Pad const& operator[] (Pad_index n) const { return app.pads[n]; }
}; Pads_t PADS;

} // END namespace ec

#include "modal-clock.h"
#include "modal-clock-port.h"
#include "modal-drums.h"
#include "modal-drum-velocity.h"
#include "modal-gate.h"
#include "modal-keys.h"
#include "modal-keys-select.h"
#include "modal-midi-channel.h"
#include "modal-midi-sequence-port.h"
#include "modal-notes.h"
#include "modal-steps.h"
#include "modal-step-division.h"
#include "modal-seed32.h"
#include "modal-swing.h"
#include "modal-velocity.h"

namespace ec {

void update_sequence_step_display(Clock_t clock) {
    app.modal->did_step(clock);
}

struct Modals {
    Modal_keys          keys;
    Modal_keys_select   keys_select;
    Modal_seed32        seed32;
    Modal_steps         steps;
    Modal_notes         notes;
    Modal_velocity      velocity;
    Modal_gate          gate;
    Modal_step_division step_division;
    Modal_midi_channel  midi_channel;
    Modal_clock_port    midi_clock_port;
    Modal_clock         midi_clock;
    Modal_sequence_port midi_sequence_port;
    Modal_swing         swing;
    Modal_drums         drums_transport;
};
static Modals modals;

}
#endif
