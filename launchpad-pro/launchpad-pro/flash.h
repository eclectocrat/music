#ifndef flash_h
#define flash_h

namespace ec {

enum class Flash_speed: Byte {
    fastest=4, fast=2, medium=1, slow=0
};

struct Flash_action {
    Flash_speed speed = Flash_speed::medium;
    unsigned int counter = 0;
    Byte state = 0;
    
    inline void reset() {
        counter = 0;
        state = 0;
    }
    
    template <typename FlashOnBranch, typename FlashOffBranch>
    void tick_flash(FlashOnBranch on, FlashOffBranch off) {
        const unsigned int midi_clocks_per_flash =
            Byte(speed) ? 24 / Byte(speed) : 32;
        if (counter++ % midi_clocks_per_flash == 0) {
            expects(not state);
            state = 1;
            on();
        } else if (state and counter % midi_clocks_per_flash == 12) {
            off();
            state = 0;
        }
    }
};

}
#endif
