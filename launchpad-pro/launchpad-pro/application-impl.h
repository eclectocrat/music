namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static constexpr Color_code color_for_seq[Sequencer::sequence_count] = {
    blue_color, pink_color, darkturquoise_color, magenta_color, turquoise_color,
    purple_color, darkgreen_color, orange_color
};

void Application::init() {
    clock_internal = clock_ms;

    Byte index = 89;
    for(auto& pad: track_pads) {
        pad.set_raw_index(Raw_index(index));
        index-=10;
    }
    
    for(Byte i=0; i<Sequencer::sequence_count; ++i) {
        auto& sequence = sequencer.sequence_at(i);
        sequence.base_color = color_for_seq[i];
        sequence.channel = i+1;
        sequence.mute = i > 0;
    }
    sequencer.drum_sequence().base_color = yellow_off_color;
    sequencer.drum_sequence().channel = 10;
    //sequencer.drum_sequence().mode = Sequence::mode_drum;
   
    current_display_mode = display_mode_steps;
    modal = &modals.steps;
    modal->activate();
    modal->display();
    
    draw_notes_tracks();
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline Sequence& Application::current_sequence() {
    return sequence_at(current_sequence_index);
}

inline Sequence& Application::sequence_at(u8 index) {
    expects(index < Sequencer::sequence_count+1); // +1 for drum sequence
    return sequencer.sequence_at(current_sequence_index);
}

void Application::select_sequence_at(Byte i) {
    if(i != current_sequence_index) {
        if(current_sequence_index == Sequencer::sequence_count) { // drum track
            drums_mode_pad.draw_clear_color();
            draw_notes_tracks();
        } else {
            auto &prev_pad = pads[Pad_index(current_sequence_index)];
            prev_pad.draw();
        }
        if(i == Sequencer::sequence_count) {
            drums_mode_pad.draw_color(yellow_off_color);
            draw_drums_tracks();
        }
    }
    modal->deactivate();
    current_sequence_index = i;
    set_display_mode(current_display_mode, false);
    mute_pad.draw_color(current_sequence().mute ? red_color : no_color);
}

void Application::select_drum_track_at(u8 i) {
    expects(i < 7);
    modal->deactivate();
    sequencer.drum_sequence().selected_drum_index = i;
    set_display_mode(current_display_mode);
}

void Application::set_display_mode(Display_mode mode, bool deac) {
    if(mode != current_display_mode) {
        prev_display_mode = current_display_mode;
    }
    current_display_mode = mode;
    if(deac) {
        modal->deactivate();
    }
    switch (current_display_mode) {
    case display_mode_steps: {
        modal = &ec::modals.steps;
    } break;
    case display_mode_notes: {
        if(current_sequence_index == Sequencer::sequence_count) {
            current_display_mode = display_mode_drums_transport;
            modal = &ec::modals.drums_transport;
        } else {
            modal = &ec::modals.notes;
        }
    } break;
    case display_mode_drums_transport: {
        if(current_sequence_index == Sequencer::sequence_count) {
            modal = &ec::modals.drums_transport;
        } else {
            current_display_mode = display_mode_notes;
            modal = &ec::modals.notes;
        }
    } break;
    case display_mode_velocity: {
        modal = &ec::modals.velocity;
    } break;
    case display_mode_gate: {
        modal = &ec::modals.gate;
    } break;
    case display_mode_keys: {
        modal = &ec::modals.keys;
    } break;
    case display_mode_keys_select: {
        modal = &ec::modals.keys_select;
    } break;
    case display_mode_clock_division: {
        modal = &ec::modals.step_division;
    } break;
    case display_mode_clock_port: {
        modal = &ec::modals.midi_clock_port;
    } break;
    case display_mode_clock: {
        modal = &ec::modals.midi_clock;
    } break;
    case display_mode_midi_channel: {
        modal = &ec::modals.midi_channel;
    } break;
    case display_mode_midi_port: {
        modal = &ec::modals.midi_sequence_port;
    } break;
    case display_mode_random: {
        modal = &ec::modals.seed32;
    } break;
    case display_mode_swing: {
        modal = &ec::modals.swing;
    } break;
    default:
        expects(false);
    }
    modal->activate();
    modal->display();
}

void Application::draw_notes_tracks() {
    for(Byte i=0; i<Sequencer::sequence_count; ++i) {
        track_pads[i].set_color(color_for_seq[i]);
        track_pads[i].draw();
    }
}

void Application::draw_drums_tracks() {
    track_pads[0].draw();
    algo::for_each(begin(track_pads)+1, end(track_pads), [](auto& pad) {
        pad.draw_color(yellow_off_color);
    });
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void Application::handle_clock_tick(u8 port) {
    if(port == midi_clock_in_port) {
        clock_pad_flash.tick_flash([&]{
            clock_pad.draw_color(red_color);
        }, [&]{
            clock_pad.draw();
        });
        if(sequencer.is_playing()) {
            sequencer.tick_clock();
        }
        // modal->did_idle(clock_internal);
    }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void Application::dispatch_top_group_button(Byte index, Byte value) {
    switch(Pad_raw_index(index)) {
    case Pad_raw_index::drums_mode:
        if(value) {
            if(drums_mode_pad.touch_state()) {
                drums_mode_pad.touch_up();
                gate_pad.draw_color(no_color);
                draw_notes_tracks();
                // select_sequence_at(prev_sequence_index);
                drums_mode_pad.draw_clear_color();
            } else {
                drums_mode_pad.touch_down();
                draw_drums_tracks();
                drums_mode_pad.draw_color(yellow_off_color);
                // prev_sequence_index = current_sequence_index;
                // select_sequence_at(Sequencer::sequence_count);
                gate_pad.draw_color(red_color);
            }
        }
        break;
    default:
        modal->handle_top_button(Pad_raw_index(index), value);
        break;
    }
}

// select sequence:
void Application::dispatch_right_group_button(Byte index, Byte value) {
    const u8 row = 8 - index/10;
    if(drums_mode_pad.touch_state()) {
        if(row == 0) {
            modal->deactivate();
            current_sequence_index = Sequencer::sequence_count;
            current_sequence().selected_drum_index = 7;
            switch(current_display_mode) {
            case display_mode_steps:
            case display_mode_keys:
            case display_mode_notes:
            case display_mode_clock_division:
            case display_mode_swing:
            case display_mode_midi_port:
            case display_mode_midi_channel:
            case display_mode_random:
                set_display_mode(current_display_mode, false);
                break;
            default:
                set_display_mode(display_mode_steps, false);
                break;
            }
        } else {
            // select_drum_track_at(row-1);
            sequencer.drum_sequence().selected_drum_index = row-1;
            switch (current_display_mode) {
            case display_mode_steps:
            case display_mode_velocity:
            case display_mode_random:
                set_display_mode(current_display_mode);
                break;
            default:
                set_display_mode(display_mode_steps);
                break;
            }
        }
    } else {
        select_sequence_at(row);
    }
    auto &next_pad = track_pads[row];
    if(value) {
        next_pad.draw_color(green_color);
    } else {
        next_pad.draw();
    }
}

// clock division, error button:
void Application::dispatch_left_group_button(Byte index, Byte value) {
    if(Pad_raw_index(index) == Pad_raw_index::shift) {
        shift_pad.draw_color(value ? white_color : no_color);
    } else if(value) {
        switch(Pad_raw_index(index)) {
        case Pad_raw_index::clock: {
            if(shift_pad.touch_state()) {
                set_display_mode(display_mode_clock_port);
            } else {
                set_display_mode(display_mode_clock);
            }
        } break;
        case Pad_raw_index::play: {
            if(shift_pad.touch_state()) {
                sequencer.set_playing(false);
                sequencer.reset_clock();
                play_pad.draw_color(red_color);
            } else {
                sequencer.set_playing(not sequencer.is_playing());
                play_pad.draw_color(sequencer.is_playing() ? green_color
                                                           : red_color);
            }
            if(not sequencer.is_playing()) {
                for(auto& sequence: sequencer) {
                    sequence.flush_note_mono_off();
                }
            }
        } break;
        case Pad_raw_index::follow: {
            modal->handle_left_button(Pad_raw_index(index), value);
        } break;
        case Pad_raw_index::mute: {
            if(shift_pad.touch_state()) {
                for(auto& sequence: sequencer) {
                    sequence.mute = true;
                    sequence.flush_note_mono_off();
                }
                current_sequence().mute = false;
                mute_pad.draw_color(indigo_color);
            } else {
                current_sequence().mute = not current_sequence().mute;
                mute_pad.draw_color(CUR_SEQ->mute ? red_color : no_color);
            }
        } break;
        case Pad_raw_index::delete_: {
            if(shift_pad.touch_state()) {
                delete_pad.draw_color(green_color);
                algo::for_each(CUR_SEQ->view, [](auto& step) { step.clear(); });
            }
            modal->display();
        } break;
        case Pad_raw_index::copy: {
            modal->handle_left_button(Pad_raw_index(index), value);
        } break;
        case Pad_raw_index::error:
            error_pad.set_color(no_color);
            error_pad.draw();
            // DEBUG
            modal->exec_prog(program);
            break;
        default: break;
        }
    } else {
        switch(Pad_raw_index(index)) {
        case Pad_raw_index::delete_:
            delete_pad.draw_clear_color();
            break;
        default: break;
        }
    }
}

void Application::dispatch_bottom_group_button(Byte index, Byte value) {
    switch (index) {
    case display_mode_gate:
        if (drums_mode_pad.touch_state()) {
            return; // no gate for drums
        }
    case display_mode_steps:
    case display_mode_notes:
    case display_mode_velocity:
        set_display_mode(Display_mode(index));
        break;
    case display_mode_keys_select:
        set_display_mode(shift_pad.touch_state() ? display_mode_keys
                                                 : display_mode_keys_select);
        break;
    case display_mode_midi_channel:
        set_display_mode(shift_pad.touch_state() ? display_mode_midi_port
                                                 : display_mode_midi_channel);
        break;
    case display_mode_clock_division:
        set_display_mode(shift_pad.touch_state() ? display_mode_swing
                                                 : display_mode_clock_division);
        break;
    case display_mode_random:
        if (shift_pad.touch_state()) {
            modal->exec_prog(program);
        } else {
            set_display_mode(display_mode_random);
        }
        break;
    default:
        break;
    }
}

inline void Application::dispatch_pad(Byte index, Byte value) {
    modal->handle_pad_raw(index, value);
}

} // END namespace
