#ifndef modal_gate_h
#define modal_gate_h

#include "modal-transport.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_gate: public Modal_transport {
public:
    void activate() override {
        APP->gate_pad.draw_color(green_color);
        Modal_transport::activate();
    }
    
    void deactivate() override {
        APP->gate_pad.draw_clear_color();
        Modal_transport::deactivate();
    }

    void display() override {
        algo::for_each(*PADS, [](Pad& p) { p.set_color(no_color); });
        draw_gate(CUR_SEQ->view, *PADS, leftmost_step(), CUR_SEQ->base_color);
        Modal_transport::display();
    }
    
    void handle_pad_grid(Pad_index pad_index, u8 value) override {
        if(not value) {
            return;
        }
        const Byte level = (8 - pad_index.weak()/8);
        if(APP->shift_pad.touch_state().weak() > 0) {
            const Byte gate = gate_for_level(level);
            for(auto& step: *CUR_SEQ) {
                if(step[0]) {
                    step[2] = gate;
                }
            }
            CUR_SEQ->default_gate_ticks = gate;
            display();
        } else {
            const Byte step_index = leftmost_step() + step_for_pad(pad_index);
            auto& step = CUR_SEQ[step_index];
            if(step[0]) {
                step[2] = gate_for_level(level);
                const Byte pad_x = step_index - leftmost_step();
                draw_vbar(*PADS, pad_x, level, CUR_SEQ->base_color);
                for_column(*PADS, Pad_index(pad_x), [](Pad& p) { p.draw(); });
            }
        }
    }
};

} // END namespace

#endif /* modal_gate_h */
