//
//  sequencerv2.h
//  launchpad-sim-macOS
//
//  Created by Jeremy Jurksztowicz on 2/20/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef sequencer_h
#define sequencer_h

#include "strong_type.h"
#include "sequence.h"
#include <algorithm>

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline Step* get_current_step(Clock_t clock, Sequence& sequence) {
    sequence.cur_swing = 0;
    const Byte prev_step_index =
        clock.weak()
            ? calculate_step<Byte>(clock - 1, sequence.view_range(),
                                   sequence.clocks_per_step, sequence.cur_swing)
            : sequence.view_range().end();
    sequence.cur_swing = (prev_step_index+1) % 2 == 1 ? sequence.swing : 0;
    const Byte step_index =
        calculate_step<Byte>(clock, sequence.view_range(),
                             sequence.clocks_per_step, sequence.cur_swing);
    auto& step = sequence[step_index];
    if (sequence.playing_note_gate_counter > 0) {
        if(--sequence.playing_note_gate_counter == 0) {
            sequence.flush_note_mono_off();
        }
    }
    if(prev_step_index != step_index) {
        return &step;
    } else {
        return nullptr;
    }
}

// render_f is not called if there is nothing to render for the given clock.
template<typename F>
void render_sequence(const Clock_t clock, Sequence& sequence, F && render_f) {
    if(auto step_ptr = get_current_step(clock, sequence)) {
        auto& step = *step_ptr;
        if(step[0]) {
            render_f(sequence, step);
        }
    }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Sequencer {
public:
    static constexpr Byte sequence_count = 8;
    static constexpr Byte drum_sequence_index = sequence_count;
    static constexpr Byte max_velocity = 127;
    
    // It's an error to run the sequencer without this set.
    void (*post_step_callback)(Clock_t) = nullptr;
    
    using Sequence_array = std::array<Sequence, sequence_count+1>;
    using iterator = typename Sequence_array::iterator;
    using const_iterator = typename Sequence_array::const_iterator;
    
    iterator begin() { return _sequences.begin(); }
    iterator end() { return _sequences.end(); }
    const_iterator begin() const { return _sequences.begin(); }
    const_iterator end() const { return _sequences.end(); }
    
    void tick_clock() {
        _clock.tick();
        for(auto& seq: _sequences) {
            render_sequence(_clock, seq, [](auto &seq, auto const &step) {
                if (seq.playing_note == step.note() and
                    step.gate() == Step::midi_byte_slide) {
                    ; // nothing
                } else if(not seq.mute) {
                    seq.flush_note_mono_off();
                    seq.send_note_mono_on(step);
                }
            });
        }
        render_sequence(_clock, drum_sequence(), [](auto &seq, auto const &step) {
            seq.flush_notes_poly_offs();
            seq.send_notes_poly_on(step);
        });
        post_step_callback(_clock);
    }
    
    inline Clock_t clock() const {
        return _clock;
    }
    inline void reset_clock() {
        _clock = Clock_t(0);
    }
    
    Sequence& drum_sequence() { return _sequences.back(); }
    Sequence& sequence_at(Byte n) { return _sequences[n]; }
    void set_playing(bool b) { _is_playing = b; }
    Byte is_playing() const { return _is_playing; }
    
private:
    std::array<Sequence, sequence_count+1> _sequences;
    Clock_t _clock{0};
    Byte _is_playing = false;
};

} // END namespace

#endif /* sequencer_h */
