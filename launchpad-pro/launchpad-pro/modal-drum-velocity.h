//
//  modal-drum-velocity.h
//  launchpad-sim-macOS
//
//  Created by Jeremy Jurksztowicz on 2/21/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef modal_drum_velocity_h
#define modal_drum_velocity_h

#include "modal-transport.h"
#include "draw_step_sequence.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_drum_velocity: public Modal_transport {
public:
    void activate() override {
        APP->velocity_pad.draw_color(green_color);
        Modal_transport::activate();
    }
    
    void deactivate() override {
        APP->velocity_pad.draw_color(no_color);
        Modal_transport::deactivate();
    }
    
    void display() override {
        draw_velocity(SEQUENCER->drum_sequence().view,
                      CUR_SEQ->selected_drum_index, *PADS, leftmost_step(),
                      SEQUENCER->drum_sequence().base_color);
        Modal_transport::display();
        algo::for_each(*PADS, [](Pad& p) { p.draw(); });
    }

    void handle_pad_grid(Pad_index pad_index, Byte value) override {
        if (not value) {
            return;
        }
        //const Byte pad_index = pads().pad_index_from_raw_index(index);
        const Byte vel = level_for_pad(pad_index);
        if (APP->shift_pad.touch_state()) {
            for(auto& step: *CUR_SEQ) {
                if (step[0]) {
                    step.set_velocity(CUR_SEQ->selected_drum_index, vel / 8);
                }
            }
            display();
        } else {
            const Byte step_index = leftmost_step() + step_for_pad(pad_index);
            auto &step = CUR_SEQ[step_index];
            if (step[0] & (0x1 << CUR_SEQ->selected_drum_index)) {
                step.set_velocity(CUR_SEQ->selected_drum_index, vel/8);
                const Byte pad_x = step_index - leftmost_step();
                draw_vbar(*PADS, pad_x, vel, CUR_SEQ->base_color);
                for_column(*PADS, Pad_index(pad_x + Pad_grid::row_width),
                                  [](Pad &p) { p.draw(); });
            }
        }
    }
};

} // END namespace
#endif /* modal_drum_velocity_h */
