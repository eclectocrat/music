#ifndef char_out_h
#define char_out_h

#include "app.h"
#include "expects.h"
#include "pad_grid.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static const u16 num_rows = 6;
static const u16 num_nums = 10;
static const u16 num_half_letters = 6;
static const u16 num_glyphs = num_rows*num_nums + num_rows*num_half_letters;

static const u8 bitmap[num_glyphs] = {
// ########################## 0
0x60,
0x90,
0x90,
0x90,
0x90,
0x60,
// ########################## 1
0xc0,
0x40,
0x40,
0x40,
0x40,
0xe0,
// ########################## 2
0x60,
0x90,
0x20,
0x40,
0x80,
0xf0,
// ########################## 3
0x60,
0x90,
0x20,
0x10,
0x90,
0x60,
// ########################## 4
0x30,
0x50,
0x90,
0xf0,
0x10,
0x10,
// ########################## 5
0xf0,
0x80,
0xe0,
0x10,
0x10,
0xe0,
// ########################## 6
0x70,
0x80,
0xe0,
0x90,
0x90,
0x60,
// ########################## 7
0xf0,
0x10,
0x20,
0x40,
0x40,
0x40,
// ########################## 8
0x60,
0x90,
0x60,
0x90,
0x90,
0x60,
// ########################## 9
0x60,
0x90,
0x70,
0x10,
0x10,
0x10,

// M - U - S

// ########################## M
0x20,
0x30,
0x20,
0x20,
0x20,
0x20,
// ##########################
0x20,
0x60,
0xa0,
0x20,
0x20,
0x20,
// ########################## U
0x20,
0x20,
0x20,
0x20,
0x20,
0x10,
// ##########################
0x20,
0x20,
0x20,
0x20,
0x60,
0xa0,
// ########################## S
0x10,
0x20,
0x10,
0x0,
0x0,
0x30,
// ##########################
0xe0,
0x0,
0xc0,
0x20,
0x20,
0xc0,
};

namespace detail {
inline void plot_color(u8 x, u8 y, Color const& c) {
    hal_plot_led(TYPEPAD, Index_map::pad_to_raw[y * Pad_grid::row_width + x],
                 c.r, c.g, c.b);
}
} // END namespace detail

void plot_bitmap_at_index(u8 val, u8 x, u8 y, Color_code on_color) {
    expects(val < num_glyphs);
    for(int i=val*6, j=0; i<(val+1)*6; ++i, ++j) {
        const u8 byte = bitmap[i];
        detail::plot_color(
            0 + x, j + y,
            color_for_code(byte & 0x80 ? on_color : no_color));
        detail::plot_color(
            1 + x, j + y,
            color_for_code(byte & 0x40 ? on_color : no_color));
        detail::plot_color(
            2 + x, j + y,
            color_for_code(byte & 0x20 ? on_color : no_color));
        detail::plot_color(
            3 + x, j + y,
            color_for_code(byte & 0x10 ? on_color : no_color));
    }
}

void plot_digit(u8 val, u8 x, u8 y, Color_code on_color) {
    plot_bitmap_at_index(val, x, y, on_color);
}

void plot_number(u8 val, u8 top_offset, Color_code c) {
    expects(val <= 99);
    const u8 x = 4;
    if(val > 9) {
        plot_digit(val/10, 0, top_offset, paired_color(c));
        val %= 10;
    }
    plot_digit(val, x, top_offset, c);
}

inline void plot_M(u8 top_offset, Color_code c) {
    plot_bitmap_at_index(10, 0, top_offset, c);
    plot_bitmap_at_index(11, 4, top_offset, c);
}

inline void plot_U(u8 top_offset, Color_code c) {
    plot_bitmap_at_index(12, 0, top_offset, c);
    plot_bitmap_at_index(13, 4, top_offset, c);
}

inline void plot_S(u8 top_offset, Color_code c) {
    plot_bitmap_at_index(14, 0, top_offset, c);
    plot_bitmap_at_index(15, 4, top_offset, c);
}

} // END namespace
#endif /* char_out_h */
