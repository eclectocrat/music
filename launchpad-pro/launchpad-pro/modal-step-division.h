#ifndef modal_step_division_h
#define modal_step_division_h

#include "modal-edit-number.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_step_division: public Modal_edit_number {
public:
    void activate() override {
        Modal_edit_number::activate();
        APP->timing_pad.draw_color(green_color);
    }
    
    void deactivate() override {
        Modal_edit_number::deactivate();
        APP->timing_pad.draw_clear_color();
    }

    void display() override {
        display_number(Sequence::clocks_per_whole/CUR_SEQ->clocks_per_step);
    }
    
    void handle_top_button(Pad_raw_index event, Byte value) override {
        switch(event) {
        case Pad_raw_index::arrow_up:
            if (value and
                CUR_SEQ->clocks_per_whole > Sequence::clocks_per_32nd) {
                CUR_SEQ->clocks_per_step /= 2;
                display();
            }
            APP->arrow_up_pad.draw_color(value ? green_color : no_color);
            break;
        case Pad_raw_index::arrow_down:
            if (value and
                CUR_SEQ->clocks_per_whole < Sequence::clocks_per_whole) {
                CUR_SEQ->clocks_per_step *= 2;
                display();
            }
            APP->arrow_down_pad.draw_color(value ? green_color : no_color);
            break;
        default: break;
        }
    }
};

} // END namespace
#endif /* modal_step_division_h */
