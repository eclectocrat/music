#ifndef modal_drums_h
#define modal_drums_h

#include "modal-transport.h"
#include "draw_step_sequence.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_drums: public Modal_transport {
public:
    void activate() override {
        APP->notes_pad.draw_color(green_color);
        Modal_transport::activate();
    }
    
    void deactivate() override {
        APP->notes_pad.draw_clear_color();
        Modal_transport::deactivate();
    }

    void display() override {
        draw_notes_poly(SEQUENCER->drum_sequence().view, *PADS, leftmost_step(),
                        SEQUENCER->drum_sequence().base_color);
        Modal_transport::display();
        algo::for_each(*PADS, [](Pad& p) { p.draw(); });
    }
    
    void handle_pad_grid(Pad_index pad_index, Byte value) override {
        if(value) {
            const Byte step_index = leftmost_step() + step_for_pad(pad_index);
            const Byte drum_index = note_for_pad(pad_index) - 1;
            const Byte bit = 0x01 << drum_index;
            auto& step = SEQUENCER->drum_sequence()[step_index];
            if(step[0] & bit) {
                step[0] ^= bit;
                display();
            } else {
                step[0] |= bit;
                step.set_velocity(drum_index, 15);
                display();
            }
        }
    }
    
    void exec_prog(Random_prog const& prog) override {
        //cur_sequence().generate_prog(prog);
        //display();
    }
};

} // END namespace

#endif /* modal_drums_h */
