#ifndef modal_steps_h
#define modal_steps_h

#include "modal.h"
#include "draw_step_sequence.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_steps: public Modal {
public:
    void activate() override {
        _held_step = 0;
        _last_drawn_step_pad_index = Pad_index(0);
        APP->steps_pad.draw_color(green_color);
        Modal::activate();
    }
    
    void deactivate() override {
        clear_selected();
        APP->steps_pad.draw_clear_color();
        Modal::deactivate();
    }
    
    void clear_selected() {
        if(CUR_SEQ->view.size() < PADS->size()) {
            for (auto i = CUR_SEQ->view_range().begin();
                      i < CUR_SEQ->view_range().end(); ++i) {
                PADS[Pad_index(i)].clear_flags();
                PADS[Pad_index(i)].set_flash_color(no_color);
                PADS[Pad_index(i)].draw();
            }
        }
    }
    
    void did_step(Clock_t clock) override {
        const auto step = CUR_SEQ->calculate_step(clock);
        const auto prev_step = CUR_SEQ->calculate_step(clock-1);
        _flash.tick_flash([&]{
            if(CUR_SEQ->view.size() < PADS->size()) { // subsequence selected
                for (auto i = CUR_SEQ->view_range().begin();
                          i < CUR_SEQ->view_range().end(); ++i) {
                    PADS[Pad_index(i)].draw_color(yellow_pale_light_color);
                    PADS[Pad_index(i)].set_flag(pad_flashing);
                    PADS[Pad_index(i)].set_flash_color(yellow_pale_light_color);
                }
            }
        }, [&]{
            clear_selected();
        });
        if(step != prev_step) {
            draw_active_step(step);
        }
    }
    
    void display() override {
        draw_steps(CUR_SEQ->view, *PADS, CUR_SEQ->base_color);
        draw_active_step(CUR_SEQ->calculate_step(SEQUENCER->clock()));
        algo::for_each(*PADS, [](Pad& p) { p.draw(); });
    }
    
    void handle_left_button(Pad_raw_index index, Byte value) override {
        const auto r = CUR_SEQ->view_range();
        if (index == Pad_raw_index::copy and r.size() < PADS->size()) {
            Byte step = r.begin();
            for (; step < r.end() and step + r.size() < PADS->size(); ++step) {
                CUR_SEQ[step + r.size()].clear();
                CUR_SEQ[step + r.size()] = CUR_SEQ[step];
            }
            CUR_SEQ->select_range(
                step, std::min(Byte(step + r.size()), PADS->size()));
            display();
        }
    }
    
    void handle_pad(Pad_index index, Byte value) override {
        const Byte step = index.weak();
        if(value) {
            if(not _held_step) {
                _held_step = step+1;
                return;
            } else {
                // Select range from held_step to step
                const Byte low_index = std::min(Byte(_held_step-1), step);
                const Byte high_index = std::max(Byte(_held_step-1), step);
                
                // deselect current
                if(CUR_SEQ->view.size() < PADS->size()) {
                    clear_selected();
                }
                CUR_SEQ->select_range(low_index, high_index+1);
            }
        } else if(_held_step-1 == step) {
            _held_step = 0;
        }
    }
    
    void exec_prog(Random_prog const& prog) override {
        CUR_SEQ->generate_prog(prog);
        display();
    }
 
private:
    void draw_active_step(Byte step) {
        auto& pad = PADS[_last_drawn_step_pad_index];
        if(pad.has_flag(pad_flashing)) {
            pad.draw_flash_color();
        } else {
            pad.draw();
        }
        const auto pad_index = Pad_index(step % PADS->size());
        PADS[pad_index].draw_color(white_color);        // draw new
        _last_drawn_step_pad_index = pad_index;         // save pos
    }
    
    Pad_index _last_drawn_step_pad_index{0};
    Byte _held_step = 0;
    Flash_action _flash;
};

} // END namespace
#endif /* modal_steps_h */
