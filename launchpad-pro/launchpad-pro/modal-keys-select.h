//
//  modal-keys-select.h
//  launchpad-sim-macOS
//
//  Created by Jeremy Jurksztowicz on 2/20/18.
//  Copyright © 2018 eclectocrat. All rights reserved.
//

#ifndef modal_keys_select_h
#define modal_keys_select_h

#include "modal.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_keys_select: public Modal_keys {
public:
    void activate() override {
        Modal_keys::activate();
        _selected_step_note = CUR_SEQ->notes_map.end();
    }
    
    void did_step(Clock_t clock) override {
        Modal_keys::did_step(clock);
        _flash_step.tick_flash([&]{
            if(_selected_step_note != CUR_SEQ->notes_map.end()) {
                auto& pad = pad_for_note(*_selected_step_note);
                pad.draw_color(white_color);
            }
        }, [&]{
            if(_selected_step_note != CUR_SEQ->notes_map.end()) {
                auto& pad = pad_for_note(*_selected_step_note);
                pad.draw();
            }
        });
    }

    void exec_prog(Random_prog const& prog) override {
        for(Byte i=0; i<CUR_SEQ->notes_map.size(); ++i) {
            const Byte low = base_note() + (prog.param1-1)*8;
            Byte val = 0;
            do {
                val = random<Byte>(low, low + (prog.param2+1)*8);
            } while (algo::find(CUR_SEQ->notes_map, [=](auto n) {
                         return n == val;
                     }) != CUR_SEQ->notes_map.end());
            CUR_SEQ->notes_map[i] = val;
        }
        std::sort(CUR_SEQ->notes_map.begin(), CUR_SEQ->notes_map.end());
        display();
    }
    
    void generate_key_pads(const Color_code col) override {
        Modal_keys::generate_key_pads(col);
        for(auto note: CUR_SEQ->notes_map) {
            if (note >= base_note() and note < base_note() + PADS->size()) {
                auto &pad = pad_for_note(note);
                pad.set_color(dark_color_for_note(note) ? green_color
                                                        : green_light_color);
            }
        }
    }
    
    void did_touch_note(Byte note, Pad& pad) override {
        Modal_keys::did_touch_note(note, pad);
        if(_selected_step_note != CUR_SEQ->notes_map.end()) {
            // clear prev note
            auto& prev_pad = pad_for_note(*_selected_step_note);
            prev_pad.draw();
            auto j = algo::find(CUR_SEQ->notes_map,
                                [=](auto n) { return n == note; });
            if(j != CUR_SEQ->notes_map.end()) {
                // selected another green note
                _selected_step_note = j;
                return;
            }
            // set new note value, sort
            *_selected_step_note = note;
            std::sort(CUR_SEQ->notes_map.begin(), CUR_SEQ->notes_map.end());
            _selected_step_note = CUR_SEQ->notes_map.end();
            display();
        } else {
            // select note if green
            _selected_step_note = algo::find(CUR_SEQ->notes_map,
                                             [=](auto n) { return note == n; });
        }
    }
    
private:
    Sequence::Step_notes::iterator _selected_step_note;
    Flash_action _flash_step;
};

} // END namespace
#endif /* modal_keys_select_h */
