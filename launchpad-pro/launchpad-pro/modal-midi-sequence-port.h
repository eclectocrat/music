#ifndef modal_midi_sequence_port_h
#define modal_midi_sequence_port_h

#include "modal-midi-port.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_sequence_port: public Modal_midi_port {
public:
    void activate() override {
        Modal_midi_port::activate();
        _io_dir = Midi_port_dir::midi_in;
        _port = CUR_SEQ->out_port;
        APP->midi_pad.draw_color(green_color);
    }
    
    void deactivate() override {
        Modal_midi_port::deactivate();
        APP->midi_pad.draw_clear_color();
    }

    void handle_top_button(Pad_raw_index event, Byte value) override {
        Modal_midi_port::handle_top_button(event, value);
        if(value) {
            if(event == Pad_raw_index::arrow_up
            or event == Pad_raw_index::arrow_down) {
                // changed the io dir
                _port = _io_dir == Midi_port_dir::midi_in ? CUR_SEQ->in_port
                                                          : CUR_SEQ->out_port;
                display();
            } else if (event == Pad_raw_index::arrow_left or
                       event == Pad_raw_index::arrow_right) {
                // changed the port
                if(_io_dir == Midi_port_dir::midi_in) {
                    CUR_SEQ->flush_note_mono_off();
                    CUR_SEQ->in_port = _port;
                } else {
                    CUR_SEQ->flush_note_mono_off();
                    CUR_SEQ->out_port = _port;
                }
                display();
            }
        }
    }
};

} // END namespace

#endif /* modal_midi_sequence_port_h */
