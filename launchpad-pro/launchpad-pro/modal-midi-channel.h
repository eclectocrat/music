#ifndef modal_midi_channel_h
#define modal_midi_channel_h

#include "modal-edit-number.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_midi_channel: public Modal_edit_number {
public:
    void activate() override {
        Modal_edit_number::activate();
        APP->midi_pad.draw_color(green_color);
    }
    
    void deactivate() override {
        Modal_edit_number::deactivate();
        APP->midi_pad.draw_color(no_color);
    }

    void display() override {
        display_number(CUR_SEQ->channel + 1);
    }
    
    void handle_top_button(Pad_raw_index event, u8 value) override {
        switch(event) {
        case Pad_raw_index::arrow_up:
            if (value and CUR_SEQ->channel < 15) {
                CUR_SEQ->flush_note_mono_off();
                CUR_SEQ->channel++;
                APP->arrow_up_pad.draw_color(green_color);
                display();
            } else {
                APP->arrow_up_pad.draw_clear_color();
            }
            break;
        case Pad_raw_index::arrow_down:
            if (value and CUR_SEQ->channel > 0) {
                CUR_SEQ->flush_note_mono_off();
                CUR_SEQ->channel--;
                APP->arrow_down_pad.draw_color(green_color);
                display();
            } else {
                APP->arrow_down_pad.draw_clear_color();
            }
            break;
        default: break;
        }
    }
};

} // END namespace
#endif /* modal_midi_channel_h */

