#ifndef modal_clock_h
#define modal_clock_h

#include "modal.h"

namespace ec {
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Modal_clock: public Modal {
public:
    void activate() override {
        Modal::activate();
        APP->clock_pad.draw_color(white_color);
        display_io();
    }
    
    void deactivate() override {
        Modal::deactivate();
        APP->clock_pad.draw_clear_color();
        APP->arrow_up_pad.draw_clear_color();
        APP->arrow_down_pad.draw_clear_color();
    }
    
    // Change MIDI input/output port's for current sequence.
    void handle_top_button(Pad_raw_index event, Byte value) override {
        if(value) {
            switch(event) {
            case Pad_raw_index::arrow_up:
                APP->clock_mode = Application::internal;
                display_io();
                break;
            case Pad_raw_index::arrow_down:
                APP->clock_mode = Application::external;
                display_io();
                break;
            case Pad_raw_index::arrow_left:
                if(APP->clock_ms < 40) {
                    APP->clock_ms += 1;
                }
                display_bpm();
                break;
            case Pad_raw_index::arrow_right:
                if(APP->clock_ms > 10) {
                    APP->clock_ms -= 1;
                }
                display_bpm();
                break;
            default:
                break;
            }
        }
    }
    
    void display() override {
        display_io();
        display_bpm();
    }
    
    void display_bpm() {
        algo::for_each(*PADS, [](auto &p) { p.set_color(no_color); });
        algo::for_each(*PADS, [](auto &p) { p.draw(); });
        unsigned int bpm = 60000/(APP->clock_ms * Sequence::clocks_per_quarter);
        if(bpm >= 200) {
            PADS[Pad_index(0)].draw_color(white_color);
            PADS[Pad_index(1)].draw_color(white_color);
            bpm -= 200;
        } else if(bpm >= 100) {
            PADS[Pad_index(0)].draw_color(white_color);
            bpm -= 100;
        }
        plot_number(bpm, 2, yellow_color);
    }
    
    void display_io() {
        if(APP->clock_mode == Application::internal) {
            APP->arrow_up_pad.draw_color(green_color);
            APP->arrow_down_pad.draw_clear_color();
        } else {
            APP->arrow_down_pad.draw_color(green_color);
            APP->arrow_up_pad.draw_clear_color();
        }
    }
};

} // END namespace

#endif /* modal_clock_h */
