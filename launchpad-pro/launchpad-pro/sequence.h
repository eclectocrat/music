#ifndef sequencev2_h
#define sequencev2_h

#include <algorithm>
#include "app_defs.h"
#include "step.h"
#include "step_sequence.h"
#include "random-util.h"
#include "rprog.h"
#include "constexpr_algo.h"

namespace ec {

enum class Midi_port: Byte {
    standalone_port = USBSTANDALONE,
    usb_port = USBMIDI,
    din_port = DINMIDI,
    no_port = DINMIDI + 1
};

enum class Midi_port_dir: Byte {
    midi_in, midi_out
};

using Clock_t = Clock<unsigned int>;

struct Sequence: public std::array<Step, 64> {
    typedef std::array<Step, 64> Super;

    static constexpr Byte clocks_per_96th     = 1;
    static constexpr Byte clocks_per_32nd     = 3;
    static constexpr Byte clocks_per_16th     = 6;
    static constexpr Byte clocks_per_8th      = 12;
    static constexpr Byte clocks_per_quarter  = 24;
    static constexpr Byte clocks_per_half     = 48;
    static constexpr Byte clocks_per_whole    = 96;
    
    typedef std::array<Byte, 7> Step_notes;
    typedef Range<Super::iterator> Step_range;
    
    Step_range  view;
    Step_notes  notes_map;
    Sbyte       swing               = 0;
    Byte        cur_swing           = 0;
    Byte        channel             = 1;
    Byte        mute                = false;
    Byte        playing_note        = 0;
    Byte        playing_note_gate_counter = -1;
    Byte        default_gate_ticks  = 6;
    Byte        default_velocity    = 127;
    Color_code  base_color          = blue_color;
    Midi_port   out_port            = Midi_port::din_port;
    Midi_port   in_port             = Midi_port::no_port;
    Byte        selected_drum_index = 0;
    Byte        clocks_per_step     = clocks_per_16th;

    Sequence(): view(begin(), end()) {
        algo::for_each(begin(), end(), [](auto &s) { s.clear(); });
        algo::for_each(notes_map, [&](auto &byte) {
            byte = 22 + ec::random<Byte>(0, this->size());
        });
        std::sort(notes_map.begin(), notes_map.end());
        select_range(0, size());
    }
    
    inline Range<Byte> view_range() const noexcept {
        return Range<Byte>(view.begin() - begin(), view.end() - begin());
    }
    
    inline void flush_note_mono_off() {
        if(playing_note) {
            hal_send_midi(Byte(out_port), NOTEOFF | channel, playing_note, 127);
            playing_note = 0;
            playing_note_gate_counter = 0;
        }
    }
    
    inline void flush_notes_poly_offs() {
        if(auto b = playing_note) {
            if(b & 0x01) hal_send_midi(Byte(out_port), NOTEOFF|channel, notes_map[0], 127);
            if(b & 0x02) hal_send_midi(Byte(out_port), NOTEOFF|channel, notes_map[1], 127);
            if(b & 0x04) hal_send_midi(Byte(out_port), NOTEOFF|channel, notes_map[2], 127);
            if(b & 0x08) hal_send_midi(Byte(out_port), NOTEOFF|channel, notes_map[3], 127);
            if(b & 0x10) hal_send_midi(Byte(out_port), NOTEOFF|channel, notes_map[4], 127);
            if(b & 0x20) hal_send_midi(Byte(out_port), NOTEOFF|channel, notes_map[5], 127);
            if(b & 0x40) hal_send_midi(Byte(out_port), NOTEOFF|channel, notes_map[6], 127);
            playing_note = 0;
            playing_note_gate_counter = 0;
        }
    }
    inline void send_note_mono_on(Step const& step) {
        if(auto b = step.note()) {
            playing_note = notes_map[b-1];
            playing_note_gate_counter = step.gate() ?: default_gate_ticks;
            hal_send_midi(Byte(out_port), NOTEON | channel, playing_note, step.velocity());
        }
    }
    
    inline void send_notes_poly_on(Step const& step) {
        if(auto b = step.note()) {
            if(b & 0x01) hal_send_midi(Byte(out_port), NOTEON|channel, notes_map[0], 127);
            if(b & 0x02) hal_send_midi(Byte(out_port), NOTEON|channel, notes_map[1], 127);
            if(b & 0x04) hal_send_midi(Byte(out_port), NOTEON|channel, notes_map[2], 127);
            if(b & 0x08) hal_send_midi(Byte(out_port), NOTEON|channel, notes_map[3], 127);
            if(b & 0x10) hal_send_midi(Byte(out_port), NOTEON|channel, notes_map[4], 127);
            if(b & 0x20) hal_send_midi(Byte(out_port), NOTEON|channel, notes_map[5], 127);
            if(b & 0x40) hal_send_midi(Byte(out_port), NOTEON|channel, notes_map[6], 127);
            playing_note = b;
            playing_note_gate_counter = 1; // TODO: For now poly mode is for drums only.
        }
    }
    
    inline void select_range(Byte low, Byte high) {
        view = Step_range(begin() + low, begin() + high);
    }
    
    inline void select_all() {
        view = Step_range(begin(), end());
    }
    
    inline Byte calculate_step(Clock_t clock) const {
        return ec::calculate_step<Byte>(clock, view_range(), clocks_per_step,
                                        cur_swing);
    }
    
    void generate_prog(Random_prog const& prog) {
        for (auto& step: view) {
            step.clear();
            if(random<Byte>(1, 3 + 9 - prog.param1) < 4) {
                const u8 min = std::min(u8(7), std::min(prog.param2, prog.param3));
                const u8 max = std::min(u8(7), std::max(prog.param2, prog.param3));
                step[0] = random<Byte>(min, max+1);
                step[1] = random<Byte>(64, 96);
                step[2] = random<Byte>(6, 33);
            }
        }
    }
};
    
}
#endif
