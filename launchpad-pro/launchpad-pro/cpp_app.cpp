#include "app_defs.h"
#include "log.h"
#include "application.h"
#include "application-impl.h"
#include "plot.h"
#include "random-util.h"

using namespace std;
using namespace ec;

enum Pad_group {
    top_group = 0,
    left_group,
    right_group,
    bottom_group,
    grid_group,
    group_count
};

Pad_group pad_group_for_raw_index(u8 index) {
    if(index > 89) {
        return top_group;
    } else if(index < 10) {
        return bottom_group;
    } else if(index % 10 == 0) {
        return left_group;
    } else if(index % 10 == 9) {
        return right_group;
    } else {
        return grid_group;
    }
}

extern "C" void cpp_app_surface_event(u8 type, u8 index, u8 value) {
    switch (type) {
    case TYPEPAD: {
        const auto group = pad_group_for_raw_index(index);
        if(group == grid_group) {
            app.dispatch_pad(index, value);
        } else if(group == top_group) {
            app.dispatch_top_group_button(index, value);
        } else if(group == right_group) {
            app.dispatch_right_group_button(index, value);
        } else if(group == left_group) {
            app.dispatch_left_group_button(index, value);
        } else if(group == bottom_group) {
            app.dispatch_bottom_group_button(index, value);
        }
    } break;
    case TYPESETUP: {
        if(value) {

        }
    } break;
    }
}

extern "C" void cpp_app_midi_event(u8 port, u8 status, u8 d1, u8 d2) {
    if(app.clock_mode == Application::external) {
        if(status == MIDITIMINGCLOCK) {
            app.handle_clock_tick(port);
        } else if(status == MIDISTOP) {
            app.clock_pad.draw();
            app.clock_pad_flash.state = 0;
        } else if(status == MIDISTART) {
            app.sequencer.reset_clock();
        }
    }
    if (app.midi_clock_thru and
        (status == MIDITIMINGCLOCK or status == MIDISTOP or
         status == MIDISTART or status == MIDICONTINUE)) {
        hal_send_midi(DINMIDI, status, d1, d2);
        hal_send_midi(USBMIDI, status, d1, d2);
        hal_send_midi(USBSTANDALONE, status, d1, d2);
    }
    // example - MIDI interface functionality for USB "MIDI" port -> DIN port
    /* if (port == USBMIDI) {
        hal_send_midi(DINMIDI, status, d1, d2);
    }
    // example - MIDI interface functionality for DIN -> USB "MIDI" port port
    if (port == DINMIDI) {
        hal_send_midi(USBMIDI, status, d1, d2);
    } */
}

extern "C" void cpp_app_sysex_event(u8 port, u8 *data, u16 count) {}
extern "C" void cpp_app_aftertouch_event(u8 index, u8 value) {}
extern "C" void cpp_app_cable_event(u8 type, u8 value) {}
extern "C" void cpp_app_timer_event() {
    if(app.clock_mode == Application::internal) {
        if (++app.clock_internal >= u32(app.clock_ms)) {
            app.clock_internal = 0;
            app.handle_clock_tick(app.midi_clock_in_port);
            // send a clock pulse up the USB
            // hal_send_midi(USBSTANDALONE, MIDITIMINGCLOCK, 0, 0);
        }
    }
}

extern "C" void cpp_app_init(const u16 *adc_raw) {
    seed_random(static_cast<u32>(*adc_raw));
    new (&modals) Modals;
    new (&app) Application(adc_raw);
    app.sequencer.post_step_callback = update_sequence_step_display;
    app.set_display_mode(Application::display_mode_steps);
}
